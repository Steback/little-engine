cmake_minimum_required(VERSION 3.18)
project(LittleVulkanEngine)

set(CMAKE_CXX_STANDARD 20)

### Binary output ###
IF (WIN32)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${PROJECT_SOURCE_DIR}/bin)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${PROJECT_SOURCE_DIR}/bin)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${PROJECT_SOURCE_DIR}/lib)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${PROJECT_SOURCE_DIR}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${PROJECT_SOURCE_DIR}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${PROJECT_SOURCE_DIR}/lib)
ENDIF()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

### Conan ###
if (WIN32)
    if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
        message("Using Debug profile")
        set(CONAN_PROFILE default_debug)
    else()
        message("Using Release profile")
        set(CONAN_PROFILE default)
    endif()
else()
    set(CONAN_PROFILE default)
endif()

execute_process(COMMAND conan install -pr=${CONAN_PROFILE} ${CMAKE_SOURCE_DIR}/conanfile.txt -if=${CMAKE_BINARY_DIR} --build=missing)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(NO_OUTPUT_DIRS)

### Python venv ###
if (EXISTS ${PROJECT_SOURCE_DIR}/venv)
    if (WIN32)
        set(PYTHON_PATH ${PROJECT_SOURCE_DIR}/venv/Scripts/python.exe)
    else()
        set(PYTHON_PATH ${PROJECT_SOURCE_DIR}/venv/bin/python)
    endif()
else()
    set(PYTHON_PATH python)
endif()

message("Python path: ${PYTHON_PATH}")

### Shaders Compile ###
if (WIN32)
    set(SHADERS_COMPILER ${PROJECT_SOURCE_DIR}/bin/glslangValidator.exe)
else()
    set(SHADERS_COMPILER ${PROJECT_SOURCE_DIR}/bin/glslangValidator)
endif()

message("[Shaders] Compile shaders script running!")
execute_process(COMMAND python ../tools/ShadersCompiler/ShadersCompiler.py ${SHADERS_COMPILER} ${PROJECT_SOURCE_DIR}/data/shaders)

# Source code and link
include_directories(${PROJECT_SOURCE_DIR}/source)
add_subdirectory(source)

# Samples
add_subdirectory(${PROJECT_SOURCE_DIR}/samples)