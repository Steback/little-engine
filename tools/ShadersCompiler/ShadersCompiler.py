import sys
import os
import subprocess
from pathlib import Path


def compileShader(path, compiler):
    print(f"\tCompile shader: {path}")
    outPath = Path(str(path) + ".spv")
    cmd = f"\"{compiler}\" -V \"{path}\" -o \"{outPath}\""
    print(cmd)
    subprocess.run(cmd, shell=True)


def main(argv):
    shadersDir = Path(argv[2])
    print(f"\tShaders path: {shadersDir}")
    for root, dirs, files in os.walk(shadersDir):
        for file in files:
            filePath = Path(root) / Path(file)
            if filePath.suffix != ".spv":
                compileShader(filePath, Path(argv[1]))


if __name__ == '__main__':
    main(sys.argv)
