import sys
import chevron
from pathlib import Path


def createHeaderFile(projectName, projectPath):
    fileContent = ""
    with open('templates/HeaderFile.mustache', 'r') as file:
        fileContent = chevron.render(file, {
            "projectName": projectName.replace(' ', ''),
            "projectNameMacro": projectName.upper().replace(' ', '_')
        })

    with open(projectPath / f"{projectName.replace(' ', '')}.hpp", 'w') as file:
        file.write(fileContent)


def createSourceFile(projectName, projectPath):
    fileContent = ""
    with open('templates/SourceFile.mustache', 'r') as file:
        fileContent = chevron.render(file, {
            "projectName": projectName.replace(' ', ''),
            "projectRealName": projectName,
        })

    with open(projectPath / f"{projectName.replace(' ', '')}.cpp", 'w') as file:
        file.write(fileContent)


def createMainFile(projectName, projectPath):
    fileContent = ""
    with open('templates/Main.mustache', 'r') as file:
        fileContent = chevron.render(file, {
            "projectName": projectName.replace(' ', ''),
        })

    with open(projectPath / "main.cpp", 'w') as file:
        file.write(fileContent)


def createCMakeFile(projectName, projectPath):
    fileContent = ""
    with open('templates/CMakeLists.mustache', 'r') as file:
        fileContent = chevron.render(file, {
            "projectName": projectName.replace(' ', ''),
        })

    with open(projectPath / "CMakeLists.txt", 'w') as file:
        file.write(fileContent)



def main(argv):
    projectName = str(argv[1])
    print(f"Project Name: {projectName}")

    projectPath = Path(argv[2]).absolute() / projectName
    if not projectPath.exists():
        projectPath.mkdir()

    print(f"Project Directory: {projectPath}")

    createHeaderFile(projectName, projectPath)
    createSourceFile(projectName, projectPath)
    createMainFile(projectName, projectPath)
    createCMakeFile(projectName, projectPath)


if __name__ == '__main__':
    main(sys.argv)
