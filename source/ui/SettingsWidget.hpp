#ifndef LITTLEVULKANENGINE_SETTINGSWIDGET_HPP
#define LITTLEVULKANENGINE_SETTINGSWIDGET_HPP


#include "Widget.hpp"
#include "config/Settings.hpp"


namespace lve {

    class SettingsWidget : public Widget {
    public:
        explicit SettingsWidget(std::shared_ptr<Settings> settings);

        void draw() override;

        void setSettings(std::shared_ptr<Settings> settings);

    private:
        float footerHeight = 0.0f;
        float buttonsWidth = 0.0f;
        std::shared_ptr<SettingsInterface> currentField;
        std::shared_ptr<Settings> settings;
        std::string currentFieldName;
        nlohmann::json cache;
    };

}


#endif //LITTLEVULKANENGINE_SETTINGSWIDGET_HPP
