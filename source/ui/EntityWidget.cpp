#include "EntityWidget.hpp"

#include "SceneWidget.hpp"
#include "scene/ScenesManager.hpp"
#include "components/DataComponent.hpp"
#include "components/RenderComponent.hpp"
#include "components/TransformComponent.hpp"


#define DRAW_COMPONENT(type, entity) { \
    if (auto* component = scene->getComponent<type>(entity)) { \
        ImGui::PushID(#type); \
        if (ImGui::BeginTable("##Table", 2)) { \
            ImGui::TableSetupColumn("", ImGuiTableColumnFlags_WidthFixed); \
            ImGui::TableSetupColumn("", 0); \
            imSerialize(*component); \
            ImGui::EndTable(); \
        } \
        ImGui::PopID(); \
    } \
}

#define DRAW_COMPONENT_COLLAPSING(type, title, entity) { \
    if (auto* component = scene->getComponent<type>(entity)) { \
        ImGui::PushID(#type); \
        ImGui::Checkbox("##Enable", &component->enable); \
        ImGui::SameLine(); \
        const bool isOpen = ImGui::CollapsingHeader(title); \
        if (ImGui::BeginPopupContextItem()) { \
            popup(); \
            ImGui::EndPopup(); \
        } \
        if (isOpen) { \
            if (ImGui::BeginTable("##Table", 2)) { \
                ImGui::TableSetupColumn("", ImGuiTableColumnFlags_WidthFixed); \
                ImGui::TableSetupColumn("", 0); \
                imSerialize(*component); \
                ImGui::EndTable(); \
            } \
        } \
        ImGui::PopID(); \
    } \
}


namespace lve {

    EntityWidget::EntityWidget(const std::shared_ptr<SceneWidget>& sceneWidget) : Widget("Entity") {
        flags |= WidgetFlags::AllowClose;

        sceneWidget->onSelectEntity.add([this](Entity entity) { onSelectEntity(entity); });
    }

    void EntityWidget::draw() {
        if (auto scene = ScenesManager::get()->getCurrent()) {
            if (currentEntity != entt::null && scene->isEntityValid(currentEntity)) {
                DRAW_COMPONENT(DataComponent, currentEntity)

                ImGui::Separator();
                if (ImGui::Button("Add Component")) {
                    // TODO: Implement Add Component button
                }

                DRAW_COMPONENT_COLLAPSING(TransformComponent, "Transform", currentEntity)
                DRAW_COMPONENT_COLLAPSING(RenderComponent, "Render", currentEntity)
            }
        }
    }

    void EntityWidget::onSelectEntity(Entity entity) {
        if (entity != entt::null) {
            currentEntity = entity;
        }
    }

    void EntityWidget::popup() {
        if (currentEntity != entt::null) {
            if (ImGui::Selectable("Remove")) {
                // TODO: Implement Remove component
            }
        }
    }

} // lve