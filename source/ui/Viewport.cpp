#include "Viewport.hpp"

#include <algorithm>


namespace lve {

    void Viewport::update() {
        ImGuiIO& io = ImGui::GetIO();
        auto displaySize = io.DisplaySize;

        while (!widgetsToRemove.empty()) {
            auto* widget = widgetsToRemove.top();
            const auto it = std::find(widgets.begin(), widgets.end(), widget);
            if (it != widgets.end()) {
                widgets.erase(it);
            }

            widget->flags &= ~WidgetFlags::Enable;
            widgetsToRemove.pop();
        }

        for (const auto& widget : widgets) {
            if (widget->isOpen()) {
                widget->update(displaySize);
            }
        }

        while (!widgetsToAdd.empty()) {
            auto* widget = widgetsToAdd.top();
            if (std::find(widgets.begin(), widgets.end(), widget) == widgets.end()) {
                widgets.push_back(widget);
            }

            widget->flags |= WidgetFlags::Enable;
            widgetsToAdd.pop();
        }
    }

    void Viewport::addWidget(Widget* widget) {
        if (!(widget->flags & WidgetFlags::Enable)) {
            widgetsToAdd.push(widget);
        }
    }

    void Viewport::removeWidget(Widget* widget) {
        if (widget->flags & WidgetFlags::Enable) {
            widgetsToRemove.push(widget);
        }
    }

    bool Viewport::hasWidget(const Widget* widget) {
        return std::find(widgets.begin(), widgets.end(), widget) != widgets.end();
    }

}
