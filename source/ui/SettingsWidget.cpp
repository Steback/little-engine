#include "SettingsWidget.hpp"


namespace lve {

    SettingsWidget::SettingsWidget(std::shared_ptr<Settings> settings)
            : Widget("Settings"), settings(std::move(settings)) {
        flags |= WidgetFlags::AllowClose;

        this->settings->save(cache);

        onShow.add([this] { this->settings->save(cache); });
        onHide.add([this] { cache = {}; });
    }

    void SettingsWidget::draw() {
        if (settings) {
            auto fields = settings->getFields();

            if (ImGui::BeginTable("##Settings", 2, ImGuiTableFlags_BordersInnerV | ImGuiTableFlags_ScrollY, {0.0f, ImGui::GetContentRegionAvail().y - footerHeight})) {
                ImGui::TableSetupColumn("Field", ImGuiTableColumnFlags_WidthFixed, 100.0f);
                ImGui::TableSetupColumn(currentField ? currentFieldName.c_str() : "");
                ImGui::TableHeadersRow();

                ImGui::TableNextColumn();
                for (const auto& [name, field] : fields) {
                    if (ImGui::Selectable(name.c_str(), currentField == field)) {
                        currentFieldName = name;
                        currentField = field;
                    }
                }

                ImGui::TableNextColumn();
                if (currentField) {
                    if (ImGui::BeginTable("##Data", 2)) {
                        ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed);
                        ImGui::TableSetupColumn("Info");
                        currentField->draw();
                        ImGui::EndTable();
                    }
                }

                ImGui::EndTable();
            }

            float PosY = ImGui::GetCursorPosY();
            ImGui::Separator();

            ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (ImGui::GetContentRegionAvail().x - buttonsWidth));
            ImGui::BeginGroup();
            {
                if (ImGui::Button("Cancel")) {
                    if (!cache.empty()) {
                        settings->load(cache);
                    }

                    invalidate();
                }

                ImGui::SameLine();

                if (ImGui::Button("Save")) {
                    settings->save();
                    invalidate();
                }
            }
            ImGui::EndGroup();

            auto rectSize = ImGui::GetItemRectSize();
            buttonsWidth = rectSize.x;
            footerHeight = ImGui::GetCursorPosY() - PosY;
        }
    }

    void SettingsWidget::setSettings(std::shared_ptr<Settings> inSettings) {
        settings = std::move(inSettings);
    }

}
