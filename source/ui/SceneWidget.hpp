#ifndef LITTLEVULKANENGINE_SCENEWIDGET_HPP
#define LITTLEVULKANENGINE_SCENEWIDGET_HPP


#include "ui/Widget.hpp"
#include "scene/Scene.hpp"
#include "utils/Delegate.hpp"


namespace lve {

    class SceneWidget : public Widget {
    public:
        SceneWidget();

        void draw() override;

    private:
        static void popup(Entity entity = entt::null);

        void drawEntity(const Entity& entity, entt::registry& registry);

    public:
        Delegate<void, Entity> onSelectEntity;
    };

}


#endif //LITTLEVULKANENGINE_SCENEWIDGET_HPP
