#include "SceneWidget.hpp"

#include <fmt/format.h>

#include "scene/ScenesManager.hpp"
#include "components/DataComponent.hpp"
#include "components/RelationshipComponent.hpp"


namespace lve {

    SceneWidget::SceneWidget() : Widget("Scene Inspector") {
        flags |= WidgetFlags::AllowClose;
    }

    void SceneWidget::draw() {
        if (auto scene = ScenesManager::get()->getCurrent()) {
            if (ImGui::BeginPopupContextWindow()) {
                popup();
                ImGui::EndPopup();
            }

            auto root = scene->getRoot();
            if (auto* relationshipComponent = scene->getComponent<RelationshipComponent>(root)) {
                for (auto& entity : relationshipComponent->children) {
                    drawEntity(entity, scene->getRegistry());
                }
            }
        }
    }

    void SceneWidget::popup(Entity entity) {
        if (ImGui::Selectable("Add Entity")) {
            ScenesManager::get()->getCurrent()->addEntity("Entity", entt::null, entity);
        }
    }

    void SceneWidget::drawEntity(const Entity& entity, entt::registry& registry) {
        auto& data = registry.get<DataComponent>(entity);
        bool nodeOpen = ImGui::TreeNodeEx(fmt::format("{}##{}", data.name, (uint32_t)entity).c_str(), ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick);

        if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen()) {
            if (onSelectEntity.isBound()) {
                onSelectEntity.broadcast(entity);
            }
        }

        if (ImGui::BeginPopupContextItem()) {
            popup(entity);
            ImGui::EndPopup();
        }

        if (nodeOpen) {
            auto& relationship = registry.get<RelationshipComponent>(entity);
            for (const auto& child : relationship.children) {
                drawEntity(child, registry);
            }

            ImGui::TreePop();
        }
    }

}
