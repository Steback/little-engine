#include "ConsoleWidget.hpp"

#include "logs/Logs.hpp"


namespace lve {

    ConsoleWidget::ConsoleWidget() : Widget("Console"), app("Little Vulkan Engine Console") {
        flags |= WidgetFlags::AllowClose;

        app.set_help_flag("", "");
        addCommand("help", [this]{ printHelpMessage(); });
        addCommand("clear", [this]{ clear(); clearHistory(); });
    }

    void ConsoleWidget::draw() {
        const float height = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing();
        if (ImGui::BeginChild("ScrollingRegion", ImVec2(0, -height), false, ImGuiWindowFlags_HorizontalScrollbar)) {
            if (ImGui::BeginPopupContextWindow()) {
                if (ImGui::Selectable("Clear")) {
                    history.clear();
                }
                ImGui::EndPopup();
            }

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
            for (const auto& item : history) {
                ImGui::TextUnformatted(item.c_str());
            }

            ImGui::PopStyleVar();
        }

        ImGui::EndChild();
        ImGui::Separator();

        const ImGuiInputTextFlags inputFlags = ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_EscapeClearsAll;
        ImGui::PushItemWidth(-FLT_MIN);
        if (ImGui::InputText("##Input", buffer, LVE_CONSOLE_BUFFER_SIZE, inputFlags)) {
            try {
                history.emplace_back(buffer);
                LVE_LOG_INFO("[Console] Command: {}", buffer);
                app.parse(buffer);
            } catch (const CLI::ParseError &e) {
                history.emplace_back(e.what());
            }

            strcpy(buffer, "");

            ImGui::SetItemDefaultFocus();
            ImGui::SetKeyboardFocusHere(-1);
        }
    }

    CLI::App* ConsoleWidget::addCommand(const std::string &name, const std::function<void()> &callback, CLI::App *parent, bool addHelpCommand) {
        auto* command = parent ? parent->add_subcommand(name) : app.add_subcommand(name);
        if (command) {
            command->callback(callback);

            if (addHelpCommand) {
                command->add_subcommand("help")->callback([this, command]{ history.push_back(command->help()); });
            }

            return command;
        }

        return nullptr;
    }

    void ConsoleWidget::printHelpMessage() {
        clear();
        history.emplace_back(app.help());
    }

    void ConsoleWidget::clearHistory() {
        clear();
        history.clear();
    }

    void ConsoleWidget::clear() {
        app.clear();
    }

    void ConsoleWidget::addMessage(const std::string &message) {
        history.push_back(message);
    }


} // lve