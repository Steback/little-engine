#include "Widget.hpp"

#include "Viewport.hpp"


namespace lve {

    Widget::Widget(std::string name, const ImVec2 &position, const ImVec2 &size, uint32_t flags)
            : name(std::move(name)), position(position), size(size), flags(flags) {

    }

    void Widget::update(const ImVec2 &winSize) {
        ImGui::PushID(name.c_str());

        int imFlags{ImGuiWindowFlags_NoCollapse};
        if (flags & WidgetFlags::BlockMove) {
            imFlags |= ImGuiWindowFlags_NoMove;
            ImGui::SetNextWindowPos(position);
        }

        if (flags & WidgetFlags::BlockResize) {
            imFlags |= ImGuiWindowFlags_NoResize;
            ImGui::SetNextWindowSize(size);
        }

        ImGui::Begin(name.c_str(), flags & WidgetFlags::AllowClose ? &open : nullptr, imFlags);
        {
            draw();

            position = ImGui::GetWindowPos();
            size = ImGui::GetWindowSize();
        }
        ImGui::End();

        ImGui::PopID();
    }

    void Widget::addToViewport() {
        if (isEnable()) {
            return;
        }

        Viewport::get()->addWidget(this);

        show();
    }

    void Widget::invalidate() {
        if (!isEnable()) {
            return;
        }

        hide();

        Viewport::get()->removeWidget(this);
    }

    void Widget::show() {
        if (open) {
            return;
        }

        open = true;

        if (onShow.isBound()) {
            onShow.broadcast();
        }
    }

    void Widget::hide() {
        if (!open) {
            return;
        }

        open = false;

        if (onHide.isBound()) {
            onHide.broadcast();
        }
    }

} // lve