#ifndef LITTLEVULKANENGINE_VIEWPORT_HPP
#define LITTLEVULKANENGINE_VIEWPORT_HPP


#include <stack>
#include <vector>
#include <memory>

#include "Widget.hpp"
#include "utils/Singleton.hpp"


namespace lve {

    class Viewport : public Singleton<Viewport> {
        friend Singleton<Viewport>;

        Viewport() = default;

    public:
        void update();

        void addWidget(Widget* widget);

        void removeWidget(Widget* widget);

        bool hasWidget(const Widget* widget);

    private:
        std::vector<Widget*> widgets;
        std::stack<Widget*> widgetsToAdd;
        std::stack<Widget*> widgetsToRemove;
    };

}


#endif //LITTLEVULKANENGINE_VIEWPORT_HPP
