#ifndef LITTLEVULKANENGINE_ENTITYWIDGET_HPP
#define LITTLEVULKANENGINE_ENTITYWIDGET_HPP


#include "Widget.hpp"
#include "scene/Scene.hpp"


namespace lve {

    class SceneWidget;

    class EntityWidget : public Widget {
    public:
        explicit EntityWidget(const std::shared_ptr<SceneWidget>& sceneWidget);

        void draw() override;

    private:
        void onSelectEntity(Entity entity);

        void popup();

        Entity currentEntity{entt::null};
    };

} // lve


#endif //LITTLEVULKANENGINE_ENTITYWIDGET_HPP
