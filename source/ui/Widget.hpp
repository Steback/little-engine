#ifndef LITTLEVULKANENGINE_WIDGET_HPP
#define LITTLEVULKANENGINE_WIDGET_HPP


#include <string>

#include <imgui.h>

#include "utils/Delegate.hpp"


namespace lve {

    namespace WidgetFlags {
        enum {
            None = 0x0,
            BlockResize = 0x1,
            BlockMove = 0x2,
            AllowClose = 0x4,

            // Don't use this, just for internal use
            Enable = 0x8,
        };
    }

    class Widget {
        friend class Viewport;

    public:
        Widget() = default;

        explicit Widget(std::string name, const ImVec2& position = {0, 0}, const ImVec2& size = {0, 0}, uint32_t flags = WidgetFlags::None);

        virtual void update(const ImVec2& winSize);

        virtual void draw() = 0;

        void addToViewport();

        void invalidate();

        void show();

        void hide();

        [[nodiscard]] inline bool isOpen() const { return open; }

        [[nodiscard]] inline bool isEnable() const { return flags & WidgetFlags::Enable; }

    public:
        ImVec2 size{0, 0};
        ImVec2 position{0, 0};
        std::string name{"Unnamed"};
        Delegate<void> onShow;
        Delegate<void> onHide;

    protected:
        bool open{false};
        uint32_t flags{WidgetFlags::None};
    };

} // lve


#endif //LITTLEVULKANENGINE_WIDGET_HPP
