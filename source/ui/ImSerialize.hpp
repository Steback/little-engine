#ifndef LITTLEVULKANENGINE_IMSERIALIZE_HPP
#define LITTLEVULKANENGINE_IMSERIALIZE_HPP


#include <vector>

#include <imgui.h>
#include <glm/glm.hpp>
#include <fmt/format.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include "external/imgui_stdlib.h"


#define LVE_LEFT_NAME_SERIALIZE(label, function, ...) \
        ImGui::PushID(label); \
        ImGui::TableNextRow(); \
        ImGui::TableNextColumn(); \
        ImGui::TextUnformatted(label); \
        ImGui::TableNextColumn(); \
        function("##Input", __VA_ARGS__); \
        ImGui::PopID()


namespace lve {

    inline void imSerialize(std::string& str, ImGuiInputTextFlags flags = 0) {
        ImGui::InputText("", &str, flags);
    }

    inline void imSerialize(const char* label, bool& check) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::Checkbox, &check);
    }

    inline void imSerialize(const char* label, uint32_t& number, ImGuiInputTextFlags flags = 0) {
        int temp = static_cast<int>(number);
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputInt, &temp);
        number = static_cast<uint32_t>(temp);
    }

    inline void imSerialize(const char* label, int& number, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputInt, &number, 1, 100, flags);
    }

    inline void imSerialize(const char* label, float& number, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputFloat, &number, 0.0f, 0.0f, "%.3f", flags);
    }

    inline void imSerialize(const char* label, std::string& str, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputText, &str);
    }

    inline void imSerialize(const char* label, glm::vec2& vector, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputFloat2, glm::value_ptr(vector), "%.3f", flags);
    }

    inline void imSerialize(const char* label, glm::vec3& vector, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputFloat3, glm::value_ptr(vector), "%.3f", flags);
    }

    inline void imSerialize(const char* label, glm::vec4& vector, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputFloat4, glm::value_ptr(vector), "%.3f", flags);
    }

    inline void imSerialize(const char* label, glm::quat& quat, ImGuiInputTextFlags flags = 0) {
        LVE_LEFT_NAME_SERIALIZE(label, ImGui::InputFloat4, glm::value_ptr(quat), "%.3f", flags);
    }

    template <typename T>
    inline void imSerialize(const char* label, std::vector<T>& vector, ImGuiTableFlags flags = 0) {
        ImGui::PushID(label);

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted(label);

        ImGui::TableNextColumn();
        if (ImGui::Button("Add")) {
            vector.push_back({});
        }

        ImGui::SameLine();

        if (ImGui::Button("Clear")) {
            vector.clear();
        }

        ImGui::Separator();

        if (ImGui::BeginTable("##Vector", 3, flags | ImGuiTableFlags_BordersInnerV)) {
            ImGui::TableSetupColumn("Index", ImGuiTableColumnFlags_WidthFixed, 20.0f);
            ImGui::TableSetupColumn("Data");
            ImGui::TableSetupColumn("Options", ImGuiTableColumnFlags_WidthFixed);
            for (int i = 0; i < vector.size(); ++i) {
                ImGui::PushID(i);

                if (i > 0) {
                    ImGui::Separator();
                }

                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                ImGui::Text("%d", i);
                ImGui::TableNextColumn();
                imSerialize(vector[i]);
                ImGui::TableNextColumn();
                if (ImGui::Button("Remove")) {
                    vector.erase(vector.begin() + i);
                    ImGui::PopID();
                    break;
                }

                ImGui::PopID();
            }
            ImGui::EndTable();
        }

        ImGui::PopID();
    }

    inline void imSerialize(const char* label, const std::vector<std::string>& values, int& current, ImGuiComboFlags flags = 0) {
        ImGui::PushID(label);

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted(label);

        ImGui::TableNextColumn();
        if (ImGui::BeginCombo("##Combo", values.empty() ? "" : values[current].c_str(), flags)) {
            for (int i = 0; i < values.size(); ++i) {
                if (ImGui::Selectable(values[i].c_str())) {
                    current = i;
                }

                if (i == current) {
                    ImGui::SetItemDefaultFocus();
                }
            }

            ImGui::EndCombo();
        }

        ImGui::PopID();
    }

}


#endif //LITTLEVULKANENGINE_IMSERIALIZE_HPP
