#ifndef LITTLEVULKANENGINE_CONSOLEWIDGET_HPP
#define LITTLEVULKANENGINE_CONSOLEWIDGET_HPP


#include <vector>
#include <functional>

#include <CLI/App.hpp>
#include <CLI/Formatter.hpp>
#include <CLI/Config.hpp>

#include "Widget.hpp"


#define LVE_CONSOLE_BUFFER_SIZE 256


namespace lve {

    class ConsoleWidget : public Widget {
    public:
        ConsoleWidget();

        void draw() override;

        /**
         * @brief Add subcommand to CLI App
         * @param name Name of subcommand, must be unique
         * @param callback Function callback that will be executed when subcommand its parse
         * @param parent Subcommand parent when the new one its a nested subcommand
         * @return Valid pointer to subcommand CLI APP, nullptr if something failed
         */
        CLI::App* addCommand(const std::string& name, const std::function<void()>& callback, CLI::App* parent = nullptr, bool addHelpCommand = false);

        /**
         * @brief Print main help message, not relative to command
         */
        void printHelpMessage();

        /**
         * @brief clear the console history
         */
        void clearHistory();

        /**
         * @brief Clear CLI app parse data
         */
        void clear();

        void addMessage(const std::string& message);

    private:
        char buffer[LVE_CONSOLE_BUFFER_SIZE]{};
        CLI::App app;
        std::vector<std::string> history;
    };

} // lve


#endif //LITTLEVULKANENGINE_CONSOLEWIDGET_HPP
