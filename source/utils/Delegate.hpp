#ifndef LITTLEVULKANENGINE_DELEGATE_HPP
#define LITTLEVULKANENGINE_DELEGATE_HPP


#include <vector>
#include <functional>


namespace lve {

    template <typename ReturnType, typename ...Params>
    class Delegate {
    public:
        using FunctionType = std::function<ReturnType(Params...params)>;

        Delegate() = default;

        inline void broadcast(Params...params) {
            for (auto& callback : callbacks) {
                callback(std::forward<Params>(params)...);
            }
        }

        inline void add(FunctionType delegate) {
            callbacks.push_back(std::move(delegate));
        }

        [[nodiscard]] inline bool isBound() const { return !callbacks.empty(); }

    private:
        std::vector<FunctionType> callbacks{};
    };

} // lve


#endif //LITTLEVULKANENGINE_DELEGATE_HPP
