#ifndef LITTLEVULKANENGINE_SINGLETON_HPP
#define LITTLEVULKANENGINE_SINGLETON_HPP


#include <memory>


namespace lve {

    /**
     * @brief Singleton Interface
     * @tparam T: Class type
     */
    template <typename T>
    class Singleton {
    public:
        /**
         * @brief Get the T singleton instance, create a new one if doesn't exists. The constructor shouldn't accept parameters
         * @return Instance reference
         */
        static std::shared_ptr<T> get();

        /**
         * @brief Create singleton instance
         * @param args Needed parameters for create instance of type T, if theres any
         */
        template<typename ...Args>
        static void setup(Args&&...args);

        /**
         * @brief Destroy instance of T if exists
         */
        static void destroy();

    private:
        static std::shared_ptr<T> singleton;
    };

    template <typename T>
    std::shared_ptr<T> Singleton<T>::singleton = nullptr;

    template <typename T>
    std::shared_ptr<T> Singleton<T>::get() {
        return singleton;
    }

    template<typename T>
    template<typename... Args>
    void Singleton<T>::setup(Args &&... args) {
        if (!singleton) {
            singleton = std::shared_ptr<T>(new T(std::forward<Args>(args)...));
        }
    }

    template <typename T>
    void Singleton<T>::destroy() {
        if (singleton) {
            singleton.reset();
        }
    }

}


#endif //LITTLEVULKANENGINE_SINGLETON_HPP
