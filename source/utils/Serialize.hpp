#ifndef LITTLEVULKANENGINE_SERIALIZE_HPP
#define LITTLEVULKANENGINE_SERIALIZE_HPP

#ifdef WIN32
#include <filesystem>
#endif


//#include <uuid.h>
#include <glm/glm.hpp>
#include <nlohmann/json.hpp>
#include <glm/gtc/type_ptr.hpp>


#define LVE_SERIALIZE_EXPAND(x) x

#define LVE_SERIALIZE2(func, v1) func(v1)
#define LVE_SERIALIZE3(func, v1, v2) func(v1) LVE_SERIALIZE2(func, v2)
#define LVE_SERIALIZE4(func, v1, v2, v3) func(v1) LVE_SERIALIZE3(func, v2, v3)
#define LVE_SERIALIZE5(func, v1, v2, v3, v4) func(v1) LVE_SERIALIZE4(func, v2, v3, v4)
#define LVE_SERIALIZE6(func, v1, v2, v3, v4, v5) func(v1) LVE_SERIALIZE5(func, v2, v3, v4, v5)
#define LVE_SERIALIZE7(func, v1, v2, v3, v4, v5, v6) func(v1) LVE_SERIALIZE6(func, v2, v3, v4, v5, v6)
#define LVE_SERIALIZE8(func, v1, v2, v3, v4, v5, v6, v7) func(v1) LVE_SERIALIZE7(func, v2, v3, v4, v5, v6, v7)
#define LVE_SERIALIZE9(func, v1, v2, v3, v4, v5, v6, v7, v8) func(v1) LVE_SERIALIZE8(func, v2, v3, v4, v5, v6, v7, v8)
#define LVE_SERIALIZE10(func, v1, v2, v3, v4, v5, v6, v7, v8, v9) func(v1) LVE_SERIALIZE9(func, v2, v3, v4, v5, v6, v7, v8)
#define LVE_SERIALIZE11(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10) func(v1) LVE_SERIALIZE10(func, v2, v3, v4, v5, v6, v7, v8, v9)

#define LVE_SERIALIZE_GET_MACRO(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, NAME, ...) NAME
#define LVE_SERIALIZE(...) LVE_SERIALIZE_EXPAND(LVE_SERIALIZE_GET_MACRO(__VA_ARGS__ \
    LVE_SERIALIZE11, \
    LVE_SERIALIZE10, \
    LVE_SERIALIZE9, \
    LVE_SERIALIZE8, \
    LVE_SERIALIZE7, \
    LVE_SERIALIZE6, \
    LVE_SERIALIZE5, \
    LVE_SERIALIZE4, \
    LVE_SERIALIZE3, \
    LVE_SERIALIZE2)(__VA_ARGS__))

#define LVE_SERIALIZE_TO(v1) archive[#v1] = obj.v1;
#define LVE_SERIALIZE_FROM(v1) archive.at(#v1).get_to(obj.v1);

#define LVE_SERIALIZE_TYPE_TO(Type, ...) inline void to_json(nlohmann::json& archive, const Type& obj) { LVE_SERIALIZE_EXPAND(LVE_SERIALIZE(LVE_SERIALIZE_TO, __VA_ARGS__)) }
#define LVE_SERIALIZE_TYPE_FROM(Type, ...) inline void from_json(const nlohmann::json& archive, Type& obj) { LVE_SERIALIZE_EXPAND(LVE_SERIALIZE(LVE_SERIALIZE_FROM, __VA_ARGS__)) }

#define LVE_SERIALIZE_TYPE_INTRUSIVE(Type, ...)  \
    friend LVE_SERIALIZE_TYPE_TO(Type, __VA_ARGS__) \
    friend LVE_SERIALIZE_TYPE_FROM(Type, __VA_ARGS__)

#define LVE_SERIALIZE_TYPE_NON_INTRUSIVE(Type, ...)  \
    LVE_SERIALIZE_TYPE_TO(Type, __VA_ARGS__) \
    LVE_SERIALIZE_TYPE_FROM(Type, __VA_ARGS__)


namespace glm {

    inline void to_json(nlohmann::json& data, const vec3& vec) {
        data.push_back(vec.x);
        data.push_back(vec.y);
        data.push_back(vec.z);
    }

    inline void from_json(const nlohmann::json& data, vec3& vec) {
        vec.x = data[0];
        vec.y = data[1];
        vec.z = data[2];
    }

    inline void to_json(nlohmann::json& data, const vec4& vec) {
        data.push_back(vec.x);
        data.push_back(vec.y);
        data.push_back(vec.z);
        data.push_back(vec.w);
    }

    inline void from_json(const nlohmann::json& data, vec4& vec) {
        vec.x = data[0];
        vec.y = data[1];
        vec.z = data[2];
        vec.w = data[3];
    }

    inline void to_json(nlohmann::json& data, const quat& quat) {
        data.push_back(quat.w);
        data.push_back(quat.x);
        data.push_back(quat.y);
        data.push_back(quat.z);
    }

    inline void from_json(const nlohmann::json& data, quat& quat) {
        quat.w = data[0];
        quat.x = data[1];
        quat.y = data[2];
        quat.z = data[3];
    }

    inline void to_json(nlohmann::json& data, const mat4& mat) {
        float row1[4] = {mat[0][0], mat[0][1], mat[0][2], mat[0][3]};
        data.push_back(row1);
        float row2[4] = {mat[1][0], mat[1][1], mat[1][2], mat[1][3]};
        data.push_back(row2);
        float row3[4] = {mat[2][0], mat[2][1], mat[2][2], mat[2][3]};
        data.push_back(row3);
        float row4[4] = {mat[3][0], mat[3][1], mat[3][2], mat[3][3]};
        data.push_back(row4);
    }

    inline void from_json(const nlohmann::json& data, mat4& mat) {
        mat[0][0] = data[0][0]; mat[0][1] = data[0][1]; mat[0][2] = data[0][2]; mat[0][3] = data[0][3];
        mat[1][0] = data[1][0]; mat[1][1] = data[1][1]; mat[1][2] = data[1][2]; mat[1][3] = data[1][3];
        mat[2][0] = data[2][0]; mat[2][1] = data[2][1]; mat[2][2] = data[2][2]; mat[2][3] = data[2][3];
        mat[3][0] = data[3][0]; mat[3][1] = data[3][1]; mat[3][2] = data[3][2]; mat[3][3] = data[3][3];
    }

}

//namespace uuids {
//
//    inline void to_json(nlohmann::json& data, const uuid& uuid) {
//        data = to_string(uuid);
//    }
//
//    inline void from_json(const nlohmann::json& data, uuid& uuid) {
//        uuid = uuids::uuid::from_string(data.get<std::string>()).value();
//    }
//
//}

#ifdef WIN32
namespace std {

    inline void to_json(nlohmann::json& data, const filesystem::path& path) {
        data = path.string();
    }

}
#endif

#endif //LITTLEVULKANENGINE_SERIALIZE_HPP
