#ifndef LITTLEVULKANENGINE_WRAPPER_HPP
#define LITTLEVULKANENGINE_WRAPPER_HPP


namespace lve {

    template <class T>
    class Wrapper {
    public:
        Wrapper() = default;

        explicit Wrapper(T handle) : handle(handle) {  }

        virtual ~Wrapper() = default;

        inline T& get() { return handle; }

        inline T get() const { return handle; }

        inline operator T() { return handle; }

        inline operator T() const { return handle; }

    protected:
        T handle{};
    };

} // lve


#endif //LITTLEVULKANENGINE_WRAPPER_HPP
