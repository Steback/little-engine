#include "Settings.hpp"

#include "logs/Logs.hpp"
#include "assets/File.hpp"


namespace lve {

    Settings::Settings(std::filesystem::path path) : path(std::move(path)) {

    }

    void Settings::load() {
        nlohmann::json archive;
        File(path).read(archive);
        load(archive);

        LVE_LOG_INFO("[Settings] Load file: {}", path.string().c_str());
    }

    void Settings::save() {
        nlohmann::json archive;
        save(archive);
        File(path).write(archive);

        LVE_LOG_INFO("[Settings] Save file: {}", path.string().c_str());
    }

    void Settings::load(const nlohmann::json &archive) {
        for (auto& [key, field] : fields) {
            field->load(archive[key]);

            if (field->onLoad.isBound()) {
                field->onLoad.broadcast(field.get());
            }
        }
    }

    void Settings::save(nlohmann::json &archive) {
        for (const auto& [key, field] : fields) {
            if (field->onSave.isBound()) {
                field->onSave.broadcast(field.get());
            }

            field->save(archive[key]);
        }
    }

} // namespace lv