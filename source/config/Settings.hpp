#ifndef LITTLEVULKANENGINE_SETTINGS_HPP
#define LITTLEVULKANENGINE_SETTINGS_HPP


#include <string>
#include <memory>
#include <filesystem>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "logs/Logs.hpp"
#include "utils/Delegate.hpp"
#include "ui/ImSerialize.hpp"


#define LVE_REGISTRY_SETTINGS_FIELD(type) \
    static inline std::string getName() { return #type; }

#define LVE_SAVE_FIELD_ATTRIBUTE(archive, attribute) archive[#attribute] = attribute
#define LVE_LOAD_FIELD_ATTRIBUTE(archive, attribute) archive.at(#attribute).get_to(attribute)


namespace lve {

    /**
     * @brief Setting interface, for dynamic add/remove fields in settings file
     * @note Don't forget to use the LVE_REGISTRY_SETTINGS_FIELD macro for avoid errors.
     */
    class SettingsInterface {
    public:
        SettingsInterface() = default;

        virtual ~SettingsInterface() = default;

        virtual void save(nlohmann::json& archive) = 0;

        virtual void load(const nlohmann::json& archive) = 0;

        /**
         * @brief Used for UI draw
         */
        virtual void draw() = 0;

    public:
        // Called before save function
        Delegate<void, SettingsInterface*> onSave;
        // Called after load function
        Delegate<void, SettingsInterface*> onLoad;
    };

    class Settings {
    public:
        explicit Settings(std::filesystem::path path);

        void load();

        void save();

        void load(const nlohmann::json& archive);

        void save(nlohmann::json& archive);

        [[nodiscard]] inline std::filesystem::path getPath() const { return path; }

        [[nodiscard]] inline const auto& getFields() const { return fields; }

        template<typename T>
        std::shared_ptr<T> addField();

        template<typename T>
        std::shared_ptr<T> getField();

        template<typename T>
        [[nodiscard]] bool hasField() const;

    public:
        // Application name;
        std::string name;

    private:
        std::filesystem::path path;
        std::unordered_map<std::string, std::shared_ptr<SettingsInterface>> fields;
    };

    template<typename T>
    std::shared_ptr<T> Settings::addField() {
        if (auto field = std::make_shared<T>()) {
            const auto fieldName = T::getName();
            if (hasField<T>()) {
                LVE_LOG_WARN("[Settings] Field already exists will be overwrite: {}", name);
            }

            fields[fieldName] = field;

            return field;
        }

        return nullptr;
    }

    template<typename T>
    std::shared_ptr<T> Settings::getField() {
        if (hasField<T>()) {
            return std::dynamic_pointer_cast<T>(fields[T::getName()]);
        }

        return nullptr;
    }

    template<typename T>
    bool Settings::hasField() const {
        return fields.contains(T::getName());
    }

} // namespace lve


#endif //LITTLEVULKANENGINE_SETTINGS_HPP
