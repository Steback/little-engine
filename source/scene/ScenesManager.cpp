#include "ScenesManager.hpp"

#include "logs/Logs.hpp"
#include "assets/AssetsManager.hpp"
#include "components/RenderComponent.hpp"


namespace lve {


    ScenesManager::ScenesManager(const std::shared_ptr<SceneSettings> &settings)
            : settings(settings) {
        scenes.resize(settings->scenes.size());
        for (int i = 0; i < scenes.size(); ++i) {
            scenes[i] = std::make_shared<Scene>(settings->scenes[i]);
        }

        currentIndex = settings->currentScene;
        if (currentIndex > -1 && currentIndex < scenes.size()) {
            current = scenes[currentIndex];
            loadScene(currentIndex);
        }
    }

    void ScenesManager::loadScene(uint32_t index) {
        if (index > scenes.size()) {
            LVE_LOG_ERROR("[Scene] Scene index to load isn't valid: {}", index);
        }

        if (scenes[index]->flags & SceneFlagLoaded) {
            LVE_LOG_ERROR("[Scene] Scene already was loaded, skip load: {}", index);
            return;
        }

        scenes[index]->load();

        auto view = scenes[index]->getRegistry().view<RenderComponent>();
        for (const auto& entity : view) {
            auto& renderComponent = view.get<RenderComponent>(entity);
            renderComponent.mesh = AssetsManager::get()->addMesh(renderComponent.meshName).second;
            renderComponent.material = AssetsManager::get()->addMaterial(renderComponent.materialName).second;
        }
    }

    void ScenesManager::saveScene(uint32_t index) {
        if (index > scenes.size()) {
            LVE_LOG_ERROR("[Scene] Scene index to save isn't valid: {}", index);
        }

        scenes[index]->save();
    }

    void ScenesManager::saveCurrent() {
        if (current && currentIndex > -1 && currentIndex < scenes.size()) {
            saveScene(currentIndex);
        }
    }

    void ScenesManager::setCurrentScene(uint32_t index) {
        if (index > scenes.size()) {
            LVE_LOG_ERROR("[Scene] Scene index to set isn't valid: {}", index);
        }

        current = scenes[index];
        currentIndex = static_cast<int32_t>(index);

        loadScene(currentIndex);
    }

    void ScenesManager::newScene(const std::string &name) {
        currentIndex = -1;
        current = std::make_shared<Scene>("", name == "Unnamed" ? SceneFlagUnnamed : 0);
    }

    void SceneSettings::save(nlohmann::json &archive) {
        LVE_SAVE_FIELD_ATTRIBUTE(archive, scenes);
        LVE_SAVE_FIELD_ATTRIBUTE(archive, currentScene);
    }

    void SceneSettings::load(const nlohmann::json &archive) {
        LVE_LOAD_FIELD_ATTRIBUTE(archive, scenes);
        LVE_LOAD_FIELD_ATTRIBUTE(archive, currentScene);
    }

    void SceneSettings::draw() {
        imSerialize("Main", currentScene);
        imSerialize("Scenes", scenes);
    }

}
