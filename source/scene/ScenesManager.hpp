#ifndef LITTLEVULKANENGINE_SCENESMANAGER_HPP
#define LITTLEVULKANENGINE_SCENESMANAGER_HPP


#include <memory>
#include <vector>

#include "Scene.hpp"
#include "config/Settings.hpp"
#include "utils/Singleton.hpp"


namespace lve {

    class SceneSettings;

    class ScenesManager : public Singleton<ScenesManager> {
        friend Singleton<ScenesManager>;

        explicit ScenesManager(const std::shared_ptr<SceneSettings>& settings);

    public:
        void loadScene(uint32_t index);

        void saveScene(uint32_t index);

        void saveCurrent();

        void setCurrentScene(uint32_t index);

        void newScene(const std::string& name = "Unnamed");

        [[nodiscard]] inline std::shared_ptr<Scene> getCurrent() const { return current; }

        [[nodiscard]] inline uint32_t getCurrentIndex() const { return currentIndex; }

    private:
        int32_t currentIndex{-1};
        std::shared_ptr<Scene> current;
        std::shared_ptr<SceneSettings> settings;
        std::vector<std::shared_ptr<Scene>> scenes;
    };

    struct SceneSettings : SettingsInterface {
        LVE_REGISTRY_SETTINGS_FIELD(Scene)

        SceneSettings() = default;

        void save(nlohmann::json &archive) override;

        void load(const nlohmann::json &archive) override;

        void draw() override;

        int32_t currentScene{-1};
        std::vector<std::string> scenes;
    };

}


#endif //LITTLEVULKANENGINE_SCENESMANAGER_HPP
