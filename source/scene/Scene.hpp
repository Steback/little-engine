#ifndef LITTLEVULKANENGINE_SCENE_HPP
#define LITTLEVULKANENGINE_SCENE_HPP


#include <memory>
#include <string>
#include <unordered_map>

#include <entt/entt.hpp>
#include <nlohmann/json.hpp>

#include "utils/NonCopyable.hpp"


#define LVE_SCENE_EXT ".scene"


namespace lve {

    using Entity = entt::entity;

    enum SceneFlag {
        SceneFlagNone = 0x0,
        SceneFlagUnnamed = 0x1,
        SceneFlagLoaded = 0x2
    };

    class Scene : NonCopyable {
        friend class ScenesManager;

    public:
        explicit Scene(std::filesystem::path path, uint32_t flags = SceneFlagNone);

        ~Scene() override;

        entt::registry& getRegistry();

        Entity addEntity(Entity id = entt::null, Entity parent = entt::null);

        Entity addEntity(const std::string& name, Entity id = entt::null, Entity parent = entt::null);

        Entity getEntity(const std::string& name);

        void removeEntity(Entity id);

        void load();

        void save();

        void clear();

        void setPath(std::filesystem::path inPath);

        [[nodiscard]] inline std::filesystem::path getPath() const { return path; }

        void setName(const std::string& name);

        [[nodiscard]] inline std::string getName() const  { return path.filename().string(); }

        [[nodiscard]] inline uint32_t getFlags() const { return flags; }

        [[nodiscard]] inline Entity getRoot() const { return root; }

        [[nodiscard]] inline bool isEntityValid(Entity entity) const { return registry.valid(entity); }

        template<typename T>
        T* addComponent(const Entity& entity);

        template<typename T>
        T* getComponent(const Entity& entity);

        template<typename T>
        bool hasComponent(const Entity& entity);

    private:
        uint32_t flags{};
        Entity root{entt::null};
        entt::registry registry;
        std::filesystem::path path;
    };

    template<typename T>
    T* Scene::addComponent(const Entity &entity) {
        if (!hasComponent<T>(entity)) {
            return &registry.emplace<T>(entity, entity, this);
        } else {
            return &registry.get<T>(entity);
        }
    }

    template<typename T>
    T *Scene::getComponent(const Entity &entity) {
        if (hasComponent<T>(entity)) {
            return &registry.get<T>(entity);
        } else {
            return nullptr;
        }
    }

    template<typename T>
    bool Scene::hasComponent(const Entity &entity) {
        return registry.all_of<T>(entity);
    }

} // namespace lve


#endif //LITTLEVULKANENGINE_SCENE_HPP
