#include "Scene.hpp"

#include <utility>

#include <fmt/format.h>

#include "assets/File.hpp"
#include "assets/AssetsManager.hpp"
#include "components/DataComponent.hpp"
#include "components/RenderComponent.hpp"
#include "components/TransformComponent.hpp"
#include "components/RelationshipComponent.hpp"


#define LVE_LOAD_COMPONENT(Component) { \
    if (archive.contains(#Component)) { \
        for (const auto& compData : archive[#Component]) { \
            Entity owner = compData["owner"]; \
            auto* component = addComponent<Component>(owner); \
            compData.get_to(*component); \
        } \
    } \
}

#define LVE_SAVE_COMPONENT(Component) { \
    archive[#Component]; \
    auto view = registry.view<Component>(); \
    for (const auto& entity : view) { \
        archive[#Component].push_back(view.get<Component>(entity)); \
    } \
}


namespace lve {

    Scene::Scene(std::filesystem::path path, uint32_t flags)
            : path(std::move(path)), flags(flags) {
        clear();
    }

    Scene::~Scene() = default;

    entt::registry &Scene::getRegistry() {
        return registry;
    }

    Entity Scene::addEntity(Entity id, Entity parent) {
        if (registry.valid(id)) {
            return id;
        }

        Entity entity = registry.create(id);
        registry.emplace<DataComponent>(entity, entity, this);
        registry.emplace<RelationshipComponent>(entity, entity, this);
        registry.emplace<TransformComponent>(entity, entity, this);

        if (parent != entt::null) {
            getComponent<RelationshipComponent>(parent)->addChild(entity);
        } else {
            getComponent<RelationshipComponent>(root)->children.push_back(entity);
        }

        return entity;
    }

    Entity Scene::addEntity(const std::string &name, Entity id, Entity parent) {
        Entity entity = addEntity(id, parent);
        auto* dataComponent = getComponent<DataComponent>(entity);
        if (dataComponent) {
            dataComponent->name = name;
        }

        return entity;
    }

    Entity Scene::getEntity(const std::string &name) {
        auto view = registry.view<DataComponent>();
        for (const auto& entity : view) {
            const auto& data = view.get<DataComponent>(entity);
            if (data.name == name) {
                return entity;
            }
        }

        return entt::null;
    }

    void Scene::removeEntity(Entity id) {
        if (registry.valid(id)) {
            registry.destroy(id);
        }
    }

    void Scene::load() {
        clear();

        auto file = File(AssetsManager::get()->getData() / path);

        nlohmann::json archive;
        file.read(archive);

        for (const auto& entity : archive["Entities"]) {
            addEntity(entity.get<Entity>());
        }

        LVE_LOAD_COMPONENT(DataComponent)
        LVE_LOAD_COMPONENT(RelationshipComponent)
        LVE_LOAD_COMPONENT(TransformComponent)
        LVE_LOAD_COMPONENT(RenderComponent)

        flags |= SceneFlagLoaded;
    }

    void Scene::save() {
        auto file = File(AssetsManager::get()->getData() / path);

        nlohmann::json archive;
        archive["Entities"] = {};

        auto dataView = registry.view<DataComponent>();
        for (const auto& entity : dataView) {
            archive["Entities"].push_back(entity);
        }

        LVE_SAVE_COMPONENT(DataComponent)
        LVE_SAVE_COMPONENT(RelationshipComponent)
        LVE_SAVE_COMPONENT(TransformComponent)
        LVE_SAVE_COMPONENT(RenderComponent)

        file.write(archive);
    }

    void Scene::clear() {
        registry.clear();
        registry = {};

        root = registry.create();
        registry.emplace<RelationshipComponent>(root, root, this);
    }

    void Scene::setPath(std::filesystem::path inPath) {
        path = std::move(inPath);
        if (path.stem() == "Unnamed") {
            flags &= ~SceneFlagUnnamed;
        } else {
            flags |= SceneFlagUnnamed;
        }
    }

    void Scene::setName(const std::string &name) {
        path = path / fmt::format("{}{}", name, LVE_SCENE_EXT);
        if (name == "Unnamed") {
            flags &= ~SceneFlagUnnamed;
        } else {
            flags |= SceneFlagUnnamed;
        }
    }

} // namespace lv