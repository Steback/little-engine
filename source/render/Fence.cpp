#include "Fence.hpp"


namespace lve {

    Fence::Fence(const std::shared_ptr<Device>& device, bool inSignaledState) : device(*device) {
        VkFenceCreateInfo createInfo{VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
        createInfo.flags = inSignaledState ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

        LVE_VK_CHECK(vkCreateFence(this->device, &createInfo, nullptr, &handle));
    }

    Fence::~Fence() {
        if (handle) {
            vkDestroyFence(device, handle, nullptr);
        }
    }

    void Fence::block(uint64_t timeoutLimit) const {
        vkWaitForFences(device, 1, &handle, VK_TRUE, timeoutLimit);
    }

    void Fence::reset() const {
        vkResetFences(device, 1, &handle);
    }


}
