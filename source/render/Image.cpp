#include "Image.hpp"

#include "Command.hpp"
#include "logs/Logs.hpp"


namespace lve {

    Image::Image(VkImage handle, const std::shared_ptr<Device>& device, VkImageView view, VmaAllocation allocation, ImageFlags flags)
            : Wrapper<VkImage>(handle), device(device), view(view), allocator(device->getAllocator()), allocation(allocation), flags(flags) {

    }

    Image::~Image() {
        if (view) {
            vkDestroyImageView(*device, view, nullptr);
        }

        if (handle && allocation) {
            vmaDestroyImage(allocator, handle, allocation);
        }
    }

    void Image::transitionLayout(VkImageLayout oldLayout, VkImageLayout newLayout) {
        VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = handle;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        VkPipelineStageFlags srcStage;
        VkPipelineStageFlags dstStage;

        if (VK_IMAGE_LAYOUT_UNDEFINED == oldLayout && VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL == newLayout) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        } else if (VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL == oldLayout && VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL == newLayout) {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        } else {
            LVE_THROW_EX("[Vulkan] Unsupported image layout transition: from {} - to {}", toString(oldLayout), toString(newLayout));
        }

        auto queue = device->getQueue(VK_QUEUE_GRAPHICS_BIT);
        OnceCommandBuffer imageTransitionChange(device, queue.index, queue);
        vkCmdPipelineBarrier(imageTransitionChange, srcStage, dstStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
        imageTransitionChange.end();
    }

    ImageBuilder::ImageBuilder(std::shared_ptr<Device> device) : device(std::move(device)) {

    }

    ImageBuilder &ImageBuilder::setHandle(VkImage inHandle) {
        handle = inHandle;
        return *this;
    }

    ImageBuilder &ImageBuilder::setFlags(ImageFlags inFlags) {
        flags = inFlags;
        return *this;
    }

    ImageBuilder &ImageBuilder::setType(VkImageType inType) {
        type = inType;
        return *this;
    }

    ImageBuilder &ImageBuilder::setFormat(VkFormat inFormat) {
        format = inFormat;
        return *this;
    }

    ImageBuilder &ImageBuilder::setExtent(VkExtent3D inExtent) {
        extent = inExtent;
        return *this;
    }

    ImageBuilder &ImageBuilder::setMipLevel(uint32_t inMipLevels) {
        mipLevels = inMipLevels;
        return *this;
    }

    ImageBuilder &ImageBuilder::setArrayLayer(uint32_t inArrayLayers) {
        arrayLayers = inArrayLayers;
        return *this;
    }

    ImageBuilder &ImageBuilder::setSamplerCount(VkSampleCountFlagBits inSampleCount) {
        sampleCount = inSampleCount;
        return *this;
    }

    ImageBuilder &ImageBuilder::setTiling(VkImageTiling inTiling) {
        tiling = inTiling;
        return *this;
    }

    ImageBuilder &ImageBuilder::setUsage(VkImageUsageFlags inUsage) {
        usage = inUsage;
        return *this;
    }

    ImageBuilder &ImageBuilder::setSharingMode(VkSharingMode inSharingMode) {
        sharingMode = inSharingMode;
        return *this;
    }

    ImageBuilder &ImageBuilder::setImageLayout(VkImageLayout inImageLayout) {
        imageLayout = inImageLayout;
        return *this;
    }

    ImageBuilder &ImageBuilder::setAspect(VkImageAspectFlagBits inAspect) {
        aspect = inAspect;
        return *this;
    }

    ImageBuilder &ImageBuilder::setMemoryUsage(VmaMemoryUsage inMemoryUsage) {
        memoryUsage = inMemoryUsage;
        return *this;
    }

    ImageBuilder &ImageBuilder::setComponent(VkComponentMapping inComponents) {
        components = inComponents;
        return *this;
    }

    std::unique_ptr<Image> ImageBuilder::build() {
        VmaAllocation allocation = nullptr;
        if (!(flags & IsSwapChainImage)) {
            VkImageCreateInfo createInfo{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
            createInfo.imageType = type;
            createInfo.format = format;
            createInfo.extent = extent;
            createInfo.mipLevels = mipLevels;
            createInfo.arrayLayers = arrayLayers;
            createInfo.samples = sampleCount;
            createInfo.tiling = tiling;
            createInfo.usage = usage;
            createInfo.sharingMode = sharingMode;
            createInfo.initialLayout = imageLayout;

            VmaAllocationCreateInfo allocationCreateInfo{};
            allocationCreateInfo.usage = memoryUsage;

            LVE_VK_CHECK(vmaCreateImage(device->getAllocator(), &createInfo, &allocationCreateInfo, &handle, &allocation, nullptr))
        }

        VkImageViewType viewType;
        switch (type) {
            case VK_IMAGE_TYPE_1D:
                viewType = VK_IMAGE_VIEW_TYPE_1D;
                break;
            case VK_IMAGE_TYPE_2D:
                viewType = VK_IMAGE_VIEW_TYPE_2D;
                break;
            case VK_IMAGE_TYPE_3D:
                viewType = VK_IMAGE_VIEW_TYPE_3D;
                break;
            case VK_IMAGE_TYPE_MAX_ENUM:
                viewType = VK_IMAGE_VIEW_TYPE_MAX_ENUM;
                break;
        }

        VkImageViewCreateInfo viewCreateInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
        viewCreateInfo.image = handle;
        viewCreateInfo.format = format;
        viewCreateInfo.viewType = viewType;
        viewCreateInfo.components = components;
        viewCreateInfo.subresourceRange = subresourceRange;
        viewCreateInfo.subresourceRange.aspectMask = aspect;
        viewCreateInfo.subresourceRange.baseMipLevel = 0;
        viewCreateInfo.subresourceRange.levelCount = mipLevels;
        viewCreateInfo.subresourceRange.baseArrayLayer = 0;
        viewCreateInfo.subresourceRange.layerCount = arrayLayers;

        VkImageView view;
        LVE_VK_CHECK(vkCreateImageView(*device, &viewCreateInfo, nullptr, &view))

        return std::make_unique<Image>(handle, device, view, allocation, flags);
    }

} // namespace lv