#ifndef LITTLEVULKANENGINE_SEMAPHORE_HPP
#define LITTLEVULKANENGINE_SEMAPHORE_HPP


#include <volk.h>

#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class Semaphore : public Wrapper<VkSemaphore> {
    public:
        explicit Semaphore(const std::shared_ptr<Device>& device);

        ~Semaphore() override;

    private:
        VkDevice device;
    };

}


#endif //LITTLEVULKANENGINE_SEMAPHORE_HPP
