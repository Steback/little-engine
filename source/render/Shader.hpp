#ifndef LITTLEVULKANENGINE_SHADER_HPP
#define LITTLEVULKANENGINE_SHADER_HPP


#include <volk.h>

#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class Shader : public Wrapper<VkShaderModule> {
    public:
        Shader(const std::shared_ptr<Device>& device, const std::filesystem::path& path, VkShaderStageFlagBits stage, uint32_t flags = 0);

        ~Shader() override;

        [[nodiscard]] VkPipelineShaderStageCreateInfo getPipelineStageCreateInfo() const;

    private:
        uint32_t flags;
        VkShaderStageFlagBits stage{};
        VkDevice device;
    };

}


#endif //LITTLEVULKANENGINE_SHADER_HPP
