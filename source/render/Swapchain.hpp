#ifndef LITTLEVULKANENGINE_SWAPCHAIN_HPP
#define LITTLEVULKANENGINE_SWAPCHAIN_HPP


#include <memory>
#include "vector"

#include <volk.h>

#include "Image.hpp"
#include "Device.hpp"
#include "Window.hpp"
#include "Surface.hpp"
#include "Semaphore.hpp"
#include "utils/Wrapper.hpp"
#include "utils/NonCopyable.hpp"


namespace lve {

    class Device;
    class Image;

    struct SwapchainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;

        static SwapchainSupportDetails querySwachainSupport(VkPhysicalDevice gpu, VkSurfaceKHR surface);

        static std::optional<VkSurfaceFormatKHR> chooseFormat(const std::vector<VkSurfaceFormatKHR>& foramts);

        static std::optional<VkPresentModeKHR> choosePresentMode(const std::vector<VkPresentModeKHR>& presentModes, bool vsyncEnable = true);

        static VkExtent2D chooseExtent(const VkSurfaceCapabilitiesKHR& capabilities, const VkExtent2D& windowSize);
    };

    class Swapchain : public Wrapper<VkSwapchainKHR> {
    public:
        Swapchain(VkSwapchainKHR handle, const std::shared_ptr<Device>& device, uint32_t imagesCount, VkFormat format, VkExtent2D extent);

        ~Swapchain() override;

        VkResult acquireNextImage(const std::shared_ptr<Semaphore>& semaphore, uint32_t& imageIndex);

        void clearImages();

        [[nodiscard]] inline VkExtent2D getExtent() const { return extent; }

        [[nodiscard]] inline VkFormat getFormat() const { return format; }

        [[nodiscard]] inline VkFormat getDepthFormat() const { return depthFormat; }

        [[nodiscard]] inline uint32_t getImagesCount() const { return images.size(); }

        [[nodiscard]] inline VkImageView getImageView(uint32_t index) const { return images[index]->getView(); }

        [[nodiscard]] inline VkImageView getDepthView(uint32_t index) const { return depthImages[index]->getView(); }

    public:
        static constexpr uint32_t maxFramesInFlight = 2;

    private:
        VkDevice device;
        VkFormat format;
        VkFormat depthFormat;
        VkExtent2D extent;
        std::vector<std::unique_ptr<Image>> images;
        std::vector<std::unique_ptr<Image>> depthImages;
    };

    struct SwapchainBuilder {
        SwapchainBuilder(std::shared_ptr<Device> device, std::shared_ptr<Window> window, std::shared_ptr<Surface> surface);

        SwapchainBuilder& setImageArrayLayer(uint32_t imageArrayLayers);

        SwapchainBuilder& setImageUsage(VkImageUsageFlags imageUsage);

        SwapchainBuilder& setImageSharingMode(VkSharingMode sharingMode);

        SwapchainBuilder& setQueueFamilyIndex(uint32_t queueFamilyIndex);

        SwapchainBuilder& setCompositeAlpha(VkCompositeAlphaFlagBitsKHR compositeAlpha);

        SwapchainBuilder& setClipped(VkBool32 clipped);

        SwapchainBuilder& setOldSwapchain(VkSwapchainKHR oldSwapchain);

        SwapchainBuilder& setVSyncStatus(bool enableVSync);

        std::unique_ptr<Swapchain> build();

        bool enableVSync{true};
        VkBool32 clipped{VK_TRUE};
        VkSharingMode sharingMode{VK_SHARING_MODE_EXCLUSIVE};
        VkImageUsageFlags imageUsage{VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT};
        VkCompositeAlphaFlagBitsKHR compositeAlpha{VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR};
        VkPresentModeKHR presentMode{};
        uint32_t imagesCount{};
        uint32_t imageArrayLayers{1};
        uint32_t queueFamilyIndex{0};
        VkFormat format{};
        VkColorSpaceKHR colorSpace{};
        VkExtent2D extent{};
        std::shared_ptr<Device> device;
        VkSwapchainKHR oldSwapchain{};
        std::shared_ptr<Window> window;
        std::shared_ptr<Surface> surface;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_SWAPCHAIN_HPP
