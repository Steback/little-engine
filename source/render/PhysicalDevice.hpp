#ifndef LITTLEVULKANENGINE_PHYSICALDEVICE_HPP
#define LITTLEVULKANENGINE_PHYSICALDEVICE_HPP


#include <memory>
#include <vector>
#include <optional>

#include <volk.h>

#include "Instance.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    struct Queue : Wrapper<VkQueue> {
        Queue() = default;

        Queue(VkQueue handle, uint32_t index, VkQueueFlagBits type);

        Queue(const Queue&) = default;

        /**
         * @brief Check if queue is valid for Swapchain presentation support
         * @param gpu: Valid vulkan physical device
         * @param queue: Valid queue family index(Must be VK_QUEUE_GRAPHICS_BIT)
         * @param surface: Valid vulkan surface that
         * @return True if queue index is valid for Swapchain presentation
         */
        static bool isPresentationSupported(VkPhysicalDevice gpu, uint32_t queue, VkSurfaceKHR surface);

        /**
         * @brief Find and get requested queue family index
         * @param gpu: Valid vulkan physical device
         * @param queue: Requested queue family index. By default is VK_QUEUE_GRAPHICS_BIT
         * @param surface: Valid vulkan surface for check presentation support, for this queue must be VK_QUEUE_GRAPHICS_BIT
         * @return Optional struct with the queue index if was found
         */
        static std::optional<uint32_t> find(VkPhysicalDevice gpu, VkQueueFlagBits queue = VK_QUEUE_GRAPHICS_BIT, VkSurfaceKHR surface = VK_NULL_HANDLE);

        VkQueueFlagBits type{};
        uint32_t index{(uint32_t)-1};
    };

    class PhysicalDevice : public Wrapper<VkPhysicalDevice> {
    public:
        PhysicalDevice(VkPhysicalDevice handle, std::shared_ptr<Instance> instance);

        [[nodiscard]] std::optional<VkFormat> findSupportFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) const;

        [[nodiscard]] inline std::shared_ptr<Instance> getInstance() const { return instance; }

        [[nodiscard]] inline VkPhysicalDeviceFeatures getFeatures() const { return getFeatures(handle); }

        [[nodiscard]] inline VkPhysicalDeviceProperties getProperties() const { return getProperties(handle); }

        [[nodiscard]] std::optional<uint32_t> getQueueIndex(VkQueueFlagBits queue, VkSurfaceKHR surface = VK_NULL_HANDLE) const;

        [[nodiscard]] inline std::vector<VkExtensionProperties> getExtensionsProperties() const { return getExtensionsProperties(handle); }

        /**
         *
         * @param gpu: Valid vulkan physical device
         * @return Physical device features
         */
        static VkPhysicalDeviceFeatures getFeatures(VkPhysicalDevice gpu);

        /**
         *
         * @param gpu: Valid vulkan physical device
         * @return Physical device properties
         */
        static VkPhysicalDeviceProperties getProperties(VkPhysicalDevice gpu);

        /**
         *
         * @param gpu: Valid vulkan physical device
         * @return List of available extensions properties if there is any
         */
        static std::vector<VkExtensionProperties> getExtensionsProperties(VkPhysicalDevice gpu);

        /**
         * @brief Check if physical device is suitable for use, according to the requirements
         * @param gpu: Valid vulkan physical device
         * @param queues: Required queue flags to support
         * @param extensions: List of required extensions names to support
         * @param surface: Valid vulkan surface for check Swapchain presentation
         * @return True if physical device meets all requirements
         */
        static bool isSuitable(VkPhysicalDevice gpu, VkQueueFlags queues, const std::vector<const char*>& extensions, VkSurfaceKHR surface = VK_NULL_HANDLE);

    private:
        std::shared_ptr<Instance> instance;
    };

    struct PhysicalDeviceBuilder {
        explicit PhysicalDeviceBuilder(std::shared_ptr<Instance> instance);

        PhysicalDeviceBuilder& setQueues(VkQueueFlags queues);

        PhysicalDeviceBuilder& setSurface(VkSurfaceKHR surface);

        PhysicalDeviceBuilder& addExtension(const char* name);

        std::unique_ptr<PhysicalDevice> build();

        VkQueueFlags queues{};
        VkSurfaceKHR surface{};
        std::shared_ptr<Instance> instance;
        std::vector<const char*> extensions;
    };

}


#endif //LITTLEVULKANENGINE_PHYSICALDEVICE_HPP
