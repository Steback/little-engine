#include "Swapchain.hpp"


namespace lve {

    SwapchainSupportDetails SwapchainSupportDetails::querySwachainSupport(VkPhysicalDevice gpu, VkSurfaceKHR surface) {
        SwapchainSupportDetails details;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu, surface, &details.capabilities);

        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, nullptr);

        if (formatCount != 0) {
            details.formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, details.formats.data());
        }

        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(gpu, surface, &presentModeCount, nullptr);

        if (presentModeCount != 0) {
            details.presentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(gpu, surface, &presentModeCount, details.presentModes.data());
        }

        return details;
    }

    std::optional<VkSurfaceFormatKHR> SwapchainSupportDetails::chooseFormat(const std::vector<VkSurfaceFormatKHR> &foramts) {
        for (const auto &availableFormat : foramts) {
            if (availableFormat.format == VK_FORMAT_R8G8B8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                return availableFormat;
        }

        if (!foramts.empty())
            return foramts[0];

        return std::nullopt;
    }

    std::optional<VkPresentModeKHR> SwapchainSupportDetails::choosePresentMode(const std::vector<VkPresentModeKHR> &presentModes, bool vsyncEnable) {
        if (vsyncEnable) {
            return VK_PRESENT_MODE_FIFO_KHR;
        }

        for (const auto &availablePresentMode : presentModes) {
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return availablePresentMode;
            }
        }

        for (auto presentMode : presentModes) {
            if (presentMode == VK_PRESENT_MODE_FIFO_KHR) {
                return VK_PRESENT_MODE_FIFO_KHR;
            }
        }

        if (!presentModes.empty()) {
            return presentModes[0];
        }

        return std::nullopt;
    }

    VkExtent2D SwapchainSupportDetails::chooseExtent(const VkSurfaceCapabilitiesKHR &capabilities, const VkExtent2D &windowSize) {
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
            return capabilities.currentExtent;
        } else {
            VkExtent2D actualExtent = windowSize;
            actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

            return actualExtent;
        }
    }

    Swapchain::Swapchain(VkSwapchainKHR handle, const std::shared_ptr<Device>& device, uint32_t imagesCount, VkFormat format, VkExtent2D extent)
            : Wrapper<VkSwapchainKHR>(handle), device(*device), format(format), extent(extent) {
        images.resize(imagesCount);
        depthImages.resize(imagesCount);

        std::vector<VkImage> rawImages;
        vkGetSwapchainImagesKHR(this->device, handle, &imagesCount, nullptr);
        rawImages.resize(imagesCount);
        vkGetSwapchainImagesKHR(this->device, handle, &imagesCount, rawImages.data());

        const std::vector<VkFormat> candidates = {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT};
        if (const auto depthFormatOpt = device->getPhysicalDevice()->findSupportFormat(candidates, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)) {
            depthFormat = depthFormatOpt.value();
            LVE_LOG_INFO("\t - Depth format: {}", toString(depthFormat));
        } else {
            LVE_THROW_EX("[Vulkan] Failed to find valid Swapchain depth format");
        }

        for (int i = 0; i < imagesCount; ++i) {
            images[i] = ImageBuilder(device)
                    .setFlags(IsSwapChainImage)
                    .setHandle(rawImages[i])
                    .setFormat(format)
                    .setType(VK_IMAGE_TYPE_2D)
                    .setAspect(VK_IMAGE_ASPECT_COLOR_BIT)
                    .setMipLevel(1)
                    .setArrayLayer(1)
                    .setComponent({ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A })
                    .build();

            depthImages[i] = ImageBuilder(device)
                    .setType(VK_IMAGE_TYPE_2D)
                    .setExtent({extent.width, extent.height, 1})
                    .setFormat(depthFormat)
                    .setMipLevel(1)
                    .setArrayLayer(1)
                    .setTiling(VK_IMAGE_TILING_OPTIMAL)
                    .setImageLayout(VK_IMAGE_LAYOUT_UNDEFINED)
                    .setUsage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
                    .setSamplerCount(VK_SAMPLE_COUNT_1_BIT)
                    .setSharingMode(VK_SHARING_MODE_EXCLUSIVE)
                    .setAspect(VK_IMAGE_ASPECT_DEPTH_BIT)
                    .build();
        }
    }

    Swapchain::~Swapchain() {
        clearImages();

        if (handle) {
            vkDestroySwapchainKHR(device, handle, nullptr);
        }
    }

    VkResult Swapchain::acquireNextImage(const std::shared_ptr<Semaphore>& semaphore, uint32_t &imageIndex) {
        return vkAcquireNextImageKHR(device, handle, std::numeric_limits<std::uint64_t>::max(), *semaphore, VK_NULL_HANDLE, &imageIndex);
    }

    void Swapchain::clearImages() {
        depthImages.clear();
        images.clear();
    }

    SwapchainBuilder::SwapchainBuilder(std::shared_ptr<Device> device, std::shared_ptr<Window> window, std::shared_ptr<Surface> surface)
            : device(std::move(device)), window(std::move(window)), surface(std::move(surface)) {

    }

    SwapchainBuilder &SwapchainBuilder::setImageArrayLayer(uint32_t inImageArrayLayers) {
        imageArrayLayers = inImageArrayLayers;
        return *this;
}

SwapchainBuilder &SwapchainBuilder::setImageUsage(VkImageUsageFlags inImageUsage) {
    imageUsage = inImageUsage;
    return *this;
}

    SwapchainBuilder &SwapchainBuilder::setImageSharingMode(VkSharingMode inSharingMode) {
        sharingMode = inSharingMode;
        return *this;
    }

    SwapchainBuilder &SwapchainBuilder::setQueueFamilyIndex(uint32_t inQueueFamilyIndex) {
        queueFamilyIndex = inQueueFamilyIndex;
        return *this;
    }

    SwapchainBuilder &SwapchainBuilder::setCompositeAlpha(VkCompositeAlphaFlagBitsKHR inCompositeAlpha) {
        compositeAlpha = inCompositeAlpha;
        return *this;
    }

    SwapchainBuilder &SwapchainBuilder::setClipped(VkBool32 inClipped) {
        clipped = inClipped;
        return *this;
    }

    SwapchainBuilder &SwapchainBuilder::setOldSwapchain(VkSwapchainKHR inOldSwapchain) {
        oldSwapchain = inOldSwapchain;
        return *this;
    }

    SwapchainBuilder &SwapchainBuilder::setVSyncStatus(bool inEnableVSync) {
        enableVSync = inEnableVSync;
        return *this;
    }

    std::unique_ptr<Swapchain> SwapchainBuilder::build() {
        SwapchainSupportDetails supportDetails = SwapchainSupportDetails::querySwachainSupport(*device->getPhysicalDevice(), *surface);
        VkSurfaceFormatKHR surfaceFormat;
        if (const auto formatOpt = SwapchainSupportDetails::chooseFormat(supportDetails.formats)) {
            surfaceFormat = formatOpt.value();
        } else {
            LVE_LOG_ERROR("[Vulkan] Failed to find a valid swapchain surface format!");
            return nullptr;
        }

        LVE_LOG_INFO("[Vulkan] Swapchain creation:");

        format = surfaceFormat.format;
        LVE_LOG_INFO("\t - Format: {}", toString(format));

        colorSpace = surfaceFormat.colorSpace;
        LVE_LOG_INFO("\t - Color space: {}", toString(colorSpace));

        LVE_LOG_INFO("\t - VSync enable: {}", enableVSync ? "True" : "False");
        if (const auto presentModeOpt = SwapchainSupportDetails::choosePresentMode(supportDetails.presentModes, enableVSync)) {
            presentMode = presentModeOpt.value();
            LVE_LOG_INFO("\t - Present mode: {}", toString(presentMode));
        } else {
            LVE_LOG_ERROR("[Vulkan] Failed to find a valid swapchain present mode!");
        }

        extent = SwapchainSupportDetails::chooseExtent(supportDetails.capabilities, window->getFramebufferExtent());
        LVE_LOG_INFO("\t - Extent: {}x{}", extent.width, extent.height);

        imagesCount = supportDetails.capabilities.minImageCount + 1;
        if (supportDetails.capabilities.maxImageCount > 0 && imagesCount > supportDetails.capabilities.maxImageCount) {
            imagesCount = supportDetails.capabilities.maxImageCount;
        }

        LVE_LOG_INFO("\t - Image Count: {}", imagesCount);

        VkSwapchainCreateInfoKHR createInfo{VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
        createInfo.surface = *surface;
        createInfo.minImageCount = imagesCount;
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = imageArrayLayers;
        createInfo.imageUsage = imageUsage;
        createInfo.imageSharingMode = sharingMode;
        createInfo.queueFamilyIndexCount = queueFamilyIndex;
        createInfo.pQueueFamilyIndices = nullptr;
        createInfo.preTransform = supportDetails.capabilities.currentTransform;
        createInfo.compositeAlpha = compositeAlpha;
        createInfo.presentMode = presentMode;
        createInfo.clipped = clipped;
        createInfo.oldSwapchain = oldSwapchain;

        VkSwapchainKHR handle;
        LVE_VK_CHECK(vkCreateSwapchainKHR(*device, &createInfo, nullptr, &handle))

        return std::make_unique<Swapchain>(handle, device, imagesCount, format, extent);
    }

} // namespace lve