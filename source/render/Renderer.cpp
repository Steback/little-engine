#include "Renderer.hpp"

#include "Device.hpp"
#include "Window.hpp"
#include "Surface.hpp"
#include "Swapchain.hpp"


namespace lve {

    Renderer::Renderer(std::shared_ptr<Window> window, const std::shared_ptr<Settings>& settings)
            : window(std::move(window)), settings(settings->getField<RendererSettings>()) {
        LVE_VK_CHECK(volkInitialize())

        InstanceBuilder instanceBuilder;
        instanceBuilder.setApplicationName(settings->name);
        instanceBuilder.setApplicationVersion(VK_MAKE_VERSION(0, 0 , 0));
        instanceBuilder.setVulkanVersion(VK_API_VERSION_1_3);
        instanceBuilder.addExtension(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        instanceBuilder.addExtension(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

        uint32_t glfwExtensionCount = 0;
        auto *glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
        if (glfwExtensionCount == 0) {
            LVE_THROW_EX("glfwGetRequiredInstanceExtensions results 0 as number of required instance extensions!");
        }

        for (std::size_t i = 0; i < glfwExtensionCount; i++) {
            instanceBuilder.addExtension(glfwExtensions[i]);
        }

#ifdef LVE_DEBUG
        instanceBuilder.addValidationLayer("VK_LAYER_KHRONOS_validation");
#endif

        instance = instanceBuilder.build();
        std::shared_ptr<Surface> surface = std::make_unique<Surface>(instance, this->window);

        physicalDevice = PhysicalDeviceBuilder(instance)
                .addExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
                .setQueues(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)
                .setSurface(*surface)
                .build();

        device = DeviceBuilder(physicalDevice)
                .setSurface(surface)
                .addExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
                .addQueueType(VK_QUEUE_GRAPHICS_BIT)
                .addQueueType(VK_QUEUE_COMPUTE_BIT)
                .addQueueType(VK_QUEUE_TRANSFER_BIT)
                .build();

        swapchain = SwapchainBuilder(device, this->window, device->getSurface())
                .setQueueFamilyIndex(device->getQueue(VK_QUEUE_GRAPHICS_BIT).index)
                .setVSyncStatus(this->settings->vsync)
                .build();

        commandPool = std::make_shared<CommandPool>(device, device->getQueue(VK_QUEUE_GRAPHICS_BIT).index);
        commandBuffers.resize(Swapchain::maxFramesInFlight);
        for (auto& buffer : commandBuffers) {
            buffer = commandPool->createCmdBuffer();
        }

        imageAvailableSemaphores.resize(Swapchain::maxFramesInFlight);
        renderFinishedSemaphores.resize(Swapchain::maxFramesInFlight);
        inFlightFences.resize(Swapchain::maxFramesInFlight);
        imagesInFlight.resize(swapchain->getImagesCount(), nullptr);
        for (size_t i = 0; i < Swapchain::maxFramesInFlight; i++) {
            imageAvailableSemaphores[i] = std::make_unique<Semaphore>(device);
            renderFinishedSemaphores[i] = std::make_unique<Semaphore>(device);
            inFlightFences[i] = std::make_shared<Fence>(device, true);
        }

        createRenderPass();
    }

    Renderer::~Renderer() {
        vkDeviceWaitIdle(*device);

        imagesInFlight.clear();
        inFlightFences.clear();
        imageAvailableSemaphores.clear();
        renderFinishedSemaphores.clear();
        commandBuffers.clear();
        commandPool.reset();
        renderPass.reset();
        swapchain.reset();
        device.reset();
        instance.reset();
        window.reset();
        settings.reset();
    }

    void Renderer::render(const Camera& camera) {
        inFlightFences[currentFrame]->block();

        if (swapchain->acquireNextImage(imageAvailableSemaphores[currentFrame], imageIndex) == VK_ERROR_OUT_OF_DATE_KHR) {
            recreate();
        }

        auto& cmdBuffer = commandBuffers[currentFrame];
        cmdBuffer->begin();

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = {0.01f, 0.01f, 0.01f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};

        renderPass->begin(*cmdBuffer, clearValues, imageIndex);

        for (auto& layer : layers) {
            layer->render(cmdBuffer->get(), swapchain->getExtent(), imageIndex, camera);
        }

        renderPass->end(*cmdBuffer);
        cmdBuffer->end();

        if (imagesInFlight[imageIndex] != nullptr)
            imagesInFlight[imageIndex]->block();

        imagesInFlight[imageIndex] = inFlightFences[currentFrame];

        VkSubmitInfo submitInfo{VK_STRUCTURE_TYPE_SUBMIT_INFO};
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &(cmdBuffer->get());

        VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submitInfo.pWaitSemaphores = &imageAvailableSemaphores[currentFrame]->get();
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &renderFinishedSemaphores[currentFrame]->get();

        inFlightFences[currentFrame]->reset();
        LVE_VK_CHECK(vkQueueSubmit(device->getQueue(VK_QUEUE_GRAPHICS_BIT), 1, &submitInfo, *inFlightFences[currentFrame]))

        VkPresentInfoKHR presentInfo{VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &renderFinishedSemaphores[currentFrame]->get();
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &swapchain->get();
        presentInfo.pImageIndices = &imageIndex;

        if (vkQueuePresentKHR(device->getQueue(VK_QUEUE_GRAPHICS_BIT), &presentInfo) == VK_ERROR_OUT_OF_DATE_KHR) {
            recreate();
        }

        currentFrame = (currentFrame + 1) % Swapchain::maxFramesInFlight;

        device->waitIdle();
    }

    void Renderer::recreate() {
        window->waitForFocus();
        device->waitIdle();
        renderPass.reset();

        auto oldSwapchain = std::move(swapchain);
        oldSwapchain->clearImages();

        SwapchainBuilder swapchainBuilder(device, window, device->getSurface());
        swapchainBuilder.setQueueFamilyIndex(device->getQueue(VK_QUEUE_GRAPHICS_BIT).index);
        swapchainBuilder.setVSyncStatus(settings->vsync);
        swapchainBuilder.setOldSwapchain(*oldSwapchain);

        swapchain = swapchainBuilder.build();
        createRenderPass();

        for (auto& layer : layers) {
            layer->resize(renderPass, swapchain);
        }

        oldSwapchain.reset();
    }

    void Renderer::addLayer(const std::shared_ptr<RenderLayer> &renderLayer) {
        if (renderLayer) {
            renderLayer->setup(device, renderPass, swapchain);
            layers.push_back(renderLayer);
        }
    }

    void Renderer::clearLayer() {
        layers.clear();
    }

    void Renderer::waitForDevice() {
        device->waitIdle();
    }

    void Renderer::createRenderPass() {
        std::array<VkAttachmentDescription, 2> attachments = {};
        // Color attachment
        attachments[0].format = swapchain->getFormat();
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        // Depth attachment
        attachments[1].format = swapchain->getDepthFormat();
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference colorReference = {};
        colorReference.attachment = 0;
        colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depthReference = {};
        depthReference.attachment = 1;
        depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::array<VkSubpassDependency, 2> dependencies{};
        dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[0].dstSubpass = 0;
        dependencies[0].srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        dependencies[0].dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        dependencies[0].srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        dependencies[0].dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
        dependencies[0].dependencyFlags = 0;

        dependencies[1].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[1].dstSubpass = 0;
        dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].srcAccessMask = 0;
        dependencies[1].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
        dependencies[1].dependencyFlags = 0;

        renderPass = RenderPassBuilder(device, swapchain)
                .addSubpassDependency(dependencies[0])
                .addSubpassDependency(dependencies[1])
                .addAttachment(attachments[0])
                .addAttachment(attachments[1])
                .addColorAttachmentReference(colorReference)
                .setDepthAttachmentReference(depthReference)
                .build();
    }

    void RendererSettings::load(const nlohmann::json &archive) {
        LVE_LOAD_FIELD_ATTRIBUTE(archive, vsync);
    }

    void RendererSettings::save(nlohmann::json &archive) {
        LVE_SAVE_FIELD_ATTRIBUTE(archive, vsync);
    }

    void RendererSettings::draw() {
        imSerialize("VSync", vsync);
    }

} // namespace lve