#ifndef LITTLEVULKANENGINE_CAMERA_HPP
#define LITTLEVULKANENGINE_CAMERA_HPP


#include "glm/glm.hpp"


namespace lve {

    class Camera {
    public:
        Camera();

        void setOrthographicProjection(float left, float right, float top, float bottom, float near, float far);

        void setPerspectiveProjection(float fovy, float aspect, float near, float far);

        void setViewDirection(const glm::vec3& position, const glm::vec3& direction, const glm::vec3& up = {0.0f, -1.0f, 0.0f});

        void setViewTarget(const glm::vec3& position, const glm::vec3& target, const glm::vec3& up = {0.f, -1.f, 0.f});

        void setViewYXZ(const glm::vec3& position, const glm::vec3& rotation);

        [[nodiscard]] const glm::mat4& getProjection() const;

        [[nodiscard]] const glm::mat4& getView() const;

    private:
        glm::mat4 projection{1.0f};
        glm::mat4 view{1.0f};
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_CAMERA_HPP
