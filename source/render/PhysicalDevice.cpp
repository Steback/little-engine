#include "PhysicalDevice.hpp"

#include "logs/Logs.hpp"


namespace lve {

    Queue::Queue(VkQueue handle, uint32_t index, VkQueueFlagBits type) : Wrapper<VkQueue>(handle), index(index), type(type) {

    }

    bool Queue::isPresentationSupported(VkPhysicalDevice gpu, uint32_t queue, VkSurfaceKHR surface) {
        VkBool32 presentationAvailable = 0;
        LVE_VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(gpu, queue, surface, &presentationAvailable))
        return presentationAvailable;
    }

    std::optional<uint32_t> Queue::find(VkPhysicalDevice gpu, VkQueueFlagBits queue, VkSurfaceKHR surface) {
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);

        std::vector<VkQueueFamilyProperties> properties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, properties.data());

        if (queue & VK_QUEUE_COMPUTE_BIT) {
            for (uint32_t i = 0; i < properties.size(); ++i) {
                if ((properties[i].queueFlags & queue) && (!(properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))) {
                    return i;
                }
            }

            return std::nullopt;
        }

        if (queue & VK_QUEUE_TRANSFER_BIT) {
            for (uint32_t i = 0; i < properties.size(); ++i) {
                if ((properties[i].queueFlags & queue) && (!(properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))) {
                    return i;
                }
            }

            return std::nullopt;
        }

        for (uint32_t i = 0; i < properties.size(); ++i) {
            if (properties[i].queueFlags & queue) {
                if (surface && queue == VK_QUEUE_GRAPHICS_BIT) {
                    if (isPresentationSupported(gpu, i, surface)) {
                        return i;
                    }
                } else {
                    return i;
                }
            }
        }

        return std::nullopt;
    }

    PhysicalDevice::PhysicalDevice(VkPhysicalDevice handle, std::shared_ptr<Instance> instance)
            : Wrapper<VkPhysicalDevice>(handle), instance(std::move(instance)) {

    }

    std::optional<VkFormat> PhysicalDevice::findSupportFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features) const {
        for (const auto& format : candidates) {
            VkFormatProperties properties;
            vkGetPhysicalDeviceFormatProperties(handle, format, &properties);

            if ((tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features) == features)
                || (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features) == features)) {
                return format;
            }
        }

        return std::nullopt;
    }

    std::optional<uint32_t> PhysicalDevice::getQueueIndex(VkQueueFlagBits queue, VkSurfaceKHR surface) const {
        return Queue::find(handle, queue, surface);
    }

    VkPhysicalDeviceFeatures PhysicalDevice::getFeatures(VkPhysicalDevice gpu) {
        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(gpu, &features);
        return features;
    }

    VkPhysicalDeviceProperties PhysicalDevice::getProperties(VkPhysicalDevice gpu) {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(gpu, &properties);
        return properties;
    }

    std::vector<VkExtensionProperties> PhysicalDevice::getExtensionsProperties(VkPhysicalDevice gpu) {
        std::uint32_t extensionsCount = 0;
        vkEnumerateDeviceExtensionProperties(gpu, nullptr, &extensionsCount, nullptr);

        if (extensionsCount == 0) {
            return {};
        } else {
            std::vector<VkExtensionProperties> extensions(extensionsCount);
            vkEnumerateDeviceExtensionProperties(gpu, nullptr, &extensionsCount, extensions.data());
            return extensions;
        }
    }

    bool PhysicalDevice::isSuitable(VkPhysicalDevice gpu, VkQueueFlags queues, const std::vector<const char *>& extensions, VkSurfaceKHR surface) {
        const auto supportedExtensions = getExtensionsProperties(gpu);
        const bool supportReqExtensions = std::all_of(extensions.begin(), extensions.end(), [supportedExtensions](const char* name){
            return std::any_of(supportedExtensions.begin(), supportedExtensions.end(), [name](VkExtensionProperties props) {
                return strcmp(props.extensionName, name) == 0;
            });
        });

        if (!supportReqExtensions)
            return false;

        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);

        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, queueFamilies.data());

        // TODO: Check how to find queue family indices
        const bool supportReqQueues = std::any_of(queueFamilies.begin(), queueFamilies.end(), [queues](VkQueueFamilyProperties queueProps){
            return queueProps.queueFlags & queues;
        });

        if (!supportReqQueues)
            return false;

        if (surface) {
            bool supportPresentation = false;
            for (uint32_t index = 0; index < queueFamilyCount; ++index) {
                if (queueFamilies[index].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                    supportPresentation = Queue::isPresentationSupported(gpu, index, surface);
                }
            }

            if (!supportPresentation)
                return false;
        }

        return true;
    }

    PhysicalDeviceBuilder::PhysicalDeviceBuilder(std::shared_ptr<Instance> instance) : instance(std::move(instance)) {

    }

    PhysicalDeviceBuilder& PhysicalDeviceBuilder::setQueues(VkQueueFlags inQueues) {
        queues = inQueues;
        return *this;
    }

    PhysicalDeviceBuilder& PhysicalDeviceBuilder::setSurface(VkSurfaceKHR inSurface) {
        surface = inSurface;
        return *this;
    }

    PhysicalDeviceBuilder& PhysicalDeviceBuilder::addExtension(const char *name) {
        extensions.push_back(name);
        return *this;
    }

    std::unique_ptr<PhysicalDevice> PhysicalDeviceBuilder::build() {
        VkPhysicalDevice handle{VK_NULL_HANDLE};

        LVE_LOG_INFO("[Vulkan] Physical device extensions required: ");
        for (const auto& extension : extensions) {
            LVE_LOG_INFO("\t - {}", extension);
        }

        const auto gpus = instance->getPhysicalDevices();
        LVE_LOG_INFO("[Vulkan]: Available GPUs:");
        for (auto& gpu : gpus) {
            auto properties = PhysicalDevice::getProperties(gpu);
            LVE_LOG_INFO("\t - {}", properties.deviceName);
            if (PhysicalDevice::isSuitable(gpu, queues, extensions, surface)) {
                handle = gpu;
            }
        }

        if (handle == VK_NULL_HANDLE) {
            return nullptr;
        }

        auto properties = PhysicalDevice::getProperties(handle);
        LVE_LOG_INFO("[Vulkan] Selected GPU: {}", properties.deviceName);

        return std::make_unique<PhysicalDevice>(handle, instance);
    }

}
