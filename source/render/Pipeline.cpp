#include "Pipeline.hpp"


namespace lve {

    Pipeline::Pipeline(VkPipeline handle, std::shared_ptr<Device> device, VkPipelineLayout layout, VkPipelineBindPoint bindPoint, VkPipelineCache cache)
            : Wrapper<VkPipeline>(handle), device(std::move(device)), layout(layout), bindPoint(bindPoint), cache(cache) {

    }

    Pipeline::~Pipeline() {
        vkDestroyPipeline(*device, handle, nullptr);
        vkDestroyPipelineLayout(*device, layout, nullptr);
        if (cache) {
            vkDestroyPipelineCache(*device, cache, nullptr);
        }
    }

    void Pipeline::bind(const VkCommandBuffer &cmdBuffer) {
        vkCmdBindPipeline(cmdBuffer, bindPoint, handle);
    }

    PipelineBuilder::PipelineBuilder(std::shared_ptr<Device> device, std::shared_ptr<RenderPass> renderPass)
            : device(std::move(device)), renderPass(std::move(renderPass)) {
        inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

        viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportInfo.viewportCount = 1;
        viewportInfo.pViewports = nullptr;
        viewportInfo.scissorCount = 1;
        viewportInfo.pScissors = nullptr;

        rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationInfo.depthClampEnable = VK_FALSE;
        rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationInfo.lineWidth = 1.0f;
        rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
        rasterizationInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationInfo.depthBiasEnable = VK_FALSE;

        multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleInfo.sampleShadingEnable = VK_FALSE;
        multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                                              VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;

        colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendInfo.attachmentCount = 1;
        colorBlendInfo.pAttachments = &colorBlendAttachment;

        depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilInfo.depthTestEnable = VK_FALSE;
        depthStencilInfo.depthWriteEnable = VK_FALSE;
        depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
        depthStencilInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;

        dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;

        bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    }

    PipelineBuilder &PipelineBuilder::setBindPoint(VkPipelineBindPoint bindPoint_) {
        bindPoint = bindPoint_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setSubpass(uint32_t subpass_) {
        subpass = subpass_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setViewportInfo(VkPipelineViewportStateCreateInfo viewportInfo_) {
        viewportInfo = viewportInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setInputAssemblyInfo(VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo_) {
        inputAssemblyInfo = inputAssemblyInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setRasterizationInfo(VkPipelineRasterizationStateCreateInfo rasterizationInfo_) {
        rasterizationInfo = rasterizationInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setMultisampleInfo(VkPipelineMultisampleStateCreateInfo multisampleInfo_) {
        multisampleInfo = multisampleInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setPipelineColorBlend(VkPipelineColorBlendAttachmentState colorBlendAttachment_) {
        colorBlendAttachment = colorBlendAttachment_;
        colorBlendInfo.attachmentCount = 1;
        colorBlendInfo.pAttachments = &colorBlendAttachment;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setColorBlendInfo(VkPipelineColorBlendStateCreateInfo colorBlendInfo_) {
        colorBlendInfo = colorBlendInfo_;
        colorBlendInfo.attachmentCount = 1;
        colorBlendInfo.pAttachments = &colorBlendAttachment;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setDepthStencilInfo(VkPipelineDepthStencilStateCreateInfo depthStencilInfo_) {
        depthStencilInfo = depthStencilInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::setDynamicStateInfo(VkPipelineDynamicStateCreateInfo dynamicStateInfo_) {
        dynamicStateInfo = dynamicStateInfo_;
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addDynamicState(VkDynamicState dynamicState) {
        dynamicStateEnables.push_back(dynamicState);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addSetLayout(VkDescriptorSetLayout setLayout) {
        setLayouts.push_back(setLayout);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addPushConstantRange(VkPushConstantRange pushConstantRange) {
        pushConstantRanges.push_back(pushConstantRange);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addShaderStage(VkPipelineShaderStageCreateInfo shaderStageInfo) {
        shaderStages.push_back(shaderStageInfo);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addBindingDescription(VkVertexInputBindingDescription bindingDescription) {
        bindingsDescription.push_back(bindingDescription);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::addAttributeDescription(VkVertexInputAttributeDescription attributeDescription) {
        attributesDescription.push_back(attributeDescription);
        return *this;
    }

    PipelineBuilder &PipelineBuilder::useCache(bool state) {
        initCache = state;
        return *this;
    }

    std::unique_ptr<Pipeline> PipelineBuilder::build() {
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
        pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(setLayouts.size());
        pipelineLayoutInfo.pSetLayouts = setLayouts.data();
        pipelineLayoutInfo.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRanges.size());
        pipelineLayoutInfo.pPushConstantRanges = pushConstantRanges.data();

        VkPipelineLayout pipelineLayout;
        LVE_VK_CHECK(vkCreatePipelineLayout(*device, &pipelineLayoutInfo, nullptr, &pipelineLayout));

        VkPipelineVertexInputStateCreateInfo vertexInputInfo{VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributesDescription.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributesDescription.data();
        vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingsDescription.size());
        vertexInputInfo.pVertexBindingDescriptions = bindingsDescription.data();

        dynamicStateInfo.pDynamicStates = dynamicStateEnables.data();
        dynamicStateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());
        dynamicStateInfo.flags = 0;

        VkGraphicsPipelineCreateInfo pipelineInfo{VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO};
        pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
        pipelineInfo.pStages = shaderStages.data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssemblyInfo;
        pipelineInfo.pViewportState = &viewportInfo;
        pipelineInfo.pRasterizationState = &rasterizationInfo;
        pipelineInfo.pMultisampleState = &multisampleInfo;
        pipelineInfo.pColorBlendState = &colorBlendInfo;
        pipelineInfo.pDepthStencilState = &depthStencilInfo;
        pipelineInfo.pDynamicState = &dynamicStateInfo;
        pipelineInfo.layout = pipelineLayout;
        pipelineInfo.renderPass = *renderPass;
        pipelineInfo.subpass = subpass;
        pipelineInfo.basePipelineIndex = -1;
        pipelineInfo.pTessellationState = VK_NULL_HANDLE;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

        VkPipelineCache pipelineCache{VK_NULL_HANDLE};
        if (initCache) {
            VkPipelineCacheCreateInfo pipelineCacheCreateInfo{VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO};
            LVE_VK_CHECK(vkCreatePipelineCache(*device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));
        }

        VkPipeline pipeline;
        LVE_VK_CHECK(vkCreateGraphicsPipelines(*device, pipelineCache, 1, &pipelineInfo, nullptr, &pipeline))

        return std::make_unique<Pipeline>(pipeline, device, pipelineLayout, bindPoint, pipelineCache);
    }

}
