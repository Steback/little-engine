#ifndef LITTLEVULKANENGINE_FRAMEBUFFER_HPP
#define LITTLEVULKANENGINE_FRAMEBUFFER_HPP


#include <memory>
#include <vector>

#include <volk.h>

#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class Framebuffer : public Wrapper<VkFramebuffer> {
    public:
        Framebuffer(const std::shared_ptr<Device>& device, VkRenderPass renderPass, VkExtent2D extent, const std::vector<VkImageView>& attachments);

        ~Framebuffer() override;

        [[nodiscard]] inline VkExtent2D getExtent() const { return extent; }

    private:
        VkExtent2D extent;
        VkDevice device;
    };

}


#endif //LITTLEVULKANENGINE_FRAMEBUFFER_HPP
