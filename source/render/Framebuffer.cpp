#include "Framebuffer.hpp"


namespace lve {

    Framebuffer::Framebuffer(const std::shared_ptr<Device>& device, VkRenderPass renderPass, VkExtent2D extent, const std::vector<VkImageView>& attachments)
            : device(*device), extent(extent) {
        VkFramebufferCreateInfo createInfo{VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
        createInfo.renderPass = renderPass;
        createInfo.width = extent.width;
        createInfo.height = extent.height;
        createInfo.layers = 1;
        createInfo.attachmentCount = (uint32_t)attachments.size();
        createInfo.pAttachments = attachments.data();

        LVE_VK_CHECK(vkCreateFramebuffer(this->device, &createInfo, nullptr, &handle))
    }

    Framebuffer::~Framebuffer() {
        if (handle) {
            vkDestroyFramebuffer(device, handle, nullptr);
        }
    }

}
