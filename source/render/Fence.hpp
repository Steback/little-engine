#ifndef LITTLEVULKANENGINE_FENCE_HPP
#define LITTLEVULKANENGINE_FENCE_HPP


#include <volk.h>

#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class Fence : public Wrapper<VkFence> {
    public:
        explicit Fence(const std::shared_ptr<Device>& device, bool inSignaledState = false);

        ~Fence() override;

        void block(std::uint64_t timeoutLimit = std::numeric_limits<std::uint64_t>::max()) const;

        void reset() const;

    private:
        VkDevice device;
    };

}


#endif //LITTLEVULKANENGINE_FENCE_HPP
