#include "Shader.hpp"

#include <fstream>


namespace lve {

    Shader::Shader(const std::shared_ptr<Device>& device, const std::filesystem::path &path, VkShaderStageFlagBits stage, uint32_t flags)
            : device(*device), stage(stage), flags(flags) {
        if (path.extension().string() != ".spv") {
            LVE_THROW_EX("[Vulkan] Invalid shader file: {}", path.string());
        }

        std::ifstream file{path, std::ios::ate | std::ios::binary};
        if (!file.is_open()) {
            LVE_THROW_EX("[Vulkan] failed to open shader file: {}", path.string());
        }

        size_t fileSize = static_cast<size_t>(file.tellg());
        file.seekg(0);

        std::vector<char> code(fileSize);
        file.read(code.data(), static_cast<std::streamsize>(fileSize));

        file.close();

        VkShaderModuleCreateInfo createInfo{VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO};
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        LVE_VK_CHECK(vkCreateShaderModule(this->device, &createInfo, nullptr, &handle));
    }

    Shader::~Shader() {
        if (handle) {
            vkDestroyShaderModule(device, handle, nullptr);
        }
    }

    VkPipelineShaderStageCreateInfo Shader::getPipelineStageCreateInfo() const {
        return {
                VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                nullptr,
                flags,
                stage,
                handle,
                "main",
                nullptr
        };
    }

}
