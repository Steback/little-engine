#include "StagingBuffer.hpp"


namespace lve {

    StagingBuffer::StagingBuffer(const std::shared_ptr<Device> &device, VkDeviceSize size, VkBufferUsageFlags usage)
            : Buffer(device, size, usage | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU) {
        map();
    }

    StagingBuffer::~StagingBuffer() {
        unmap();
    }

    void StagingBuffer::copyTo(const VkCommandBuffer &commandBuffer, Buffer &dst, VkDeviceSize srcOffset, VkDeviceSize dstOffset) {
        VkBufferCopy copyRegion{};
        copyRegion.srcOffset = srcOffset;
        copyRegion.dstOffset = dstOffset;
        copyRegion.size = size;

        vkCmdCopyBuffer(commandBuffer, handle, dst, 1, &copyRegion);
    }

} // lve