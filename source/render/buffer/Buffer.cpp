#include "Buffer.hpp"


namespace lve {

    Buffer::Buffer(const std::shared_ptr<Device>& device, VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage memoryUsage, uint32_t flags)
            : allocator(device->getAllocator()), device(*device), size(size) {
        VkBufferCreateInfo createInfo{VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
        createInfo.usage = usage;
        createInfo.size = size;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.flags = flags;

        VmaAllocationCreateInfo allocationCreateInfo{};
        allocationCreateInfo.usage = memoryUsage;

        LVE_VK_CHECK(vmaCreateBuffer(allocator, &createInfo, &allocationCreateInfo, &handle, &allocation, nullptr))
    }

    Buffer::~Buffer() {
        if (handle && allocation) {
            vmaDestroyBuffer(allocator, handle, allocation);
        }
    }

    VkResult Buffer::map() {
        return vmaMapMemory(allocator, allocation, &mapped);
    }

    void Buffer::unmap() {
        if (mapped) {
            vmaUnmapMemory(allocator, allocation);
            mapped = nullptr;
        }
    }

    VkResult Buffer::bind() {
        return vmaBindBufferMemory(allocator, allocation, handle);
    }

    VkResult Buffer::flush(VkDeviceSize inSize, VkDeviceSize offset) {
        return vmaFlushAllocation(allocator, allocation, offset, inSize);
    }

    VkResult Buffer::invalidate(VkDeviceSize inSize, VkDeviceSize offset) {
        return vmaInvalidateAllocation(allocator, allocation, offset, inSize);
    }

}
