#ifndef LITTLEVULKANENGINE_STAGINGBUFFER_HPP
#define LITTLEVULKANENGINE_STAGINGBUFFER_HPP


#include "Buffer.hpp"


namespace lve {

    class StagingBuffer : public Buffer {
    public:
        StagingBuffer(const std::shared_ptr<Device>& device, VkDeviceSize size, VkBufferUsageFlags usage);

        template<typename T>
        StagingBuffer(const std::shared_ptr<Device>& device, VkDeviceSize size, VkBufferUsageFlags usage, T* data);

        ~StagingBuffer() override;

        void copyTo(const VkCommandBuffer& commandBuffer, Buffer& dst, VkDeviceSize srcOffset = 0, VkDeviceSize dstOffset = 0);
    };

    template<typename T>
    StagingBuffer::StagingBuffer(const std::shared_ptr<Device> &device, VkDeviceSize size, VkBufferUsageFlags usage, T *data)
            : Buffer(device, size, usage | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU) {
        map();
        Buffer::copyTo(data);
    }

} // lve


#endif //LITTLEVULKANENGINE_STAGINGBUFFER_HPP
