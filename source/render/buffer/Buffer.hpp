#ifndef LITTLEVULKANENGINE_BUFFER_HPP
#define LITTLEVULKANENGINE_BUFFER_HPP


#include <volk.h>

#include "utils/Wrapper.hpp"
#include "render/Device.hpp"


namespace lve {

    class Buffer : public Wrapper<VkBuffer> {
    public:
        Buffer(const std::shared_ptr<Device>& device, VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage memoryUsage, uint32_t flags = 0);

        ~Buffer() override;

        VkResult map();

        void unmap();

        VkResult bind();

        VkResult flush(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);

        VkResult invalidate(VkDeviceSize size = VK_WHOLE_SIZE, VkDeviceSize offset = 0);

        template<typename T>
        void copyTo(T* data);

    public:
        void* mapped{nullptr};

    protected:
        VkDevice device;
        VkDeviceSize size;
        VmaAllocator allocator{};
        VmaAllocation allocation{};
    };

    template<typename T>
    void Buffer::copyTo(T *data) {
        std::memcpy(mapped, data, size);
    }

}


#endif //LITTLEVULKANENGINE_BUFFER_HPP
