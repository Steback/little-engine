#ifndef LITTLEVULKANENGINE_RENDERPASS_HPP
#define LITTLEVULKANENGINE_RENDERPASS_HPP


#include <vector>

#include <volk.h>

#include "Device.hpp"
#include "Swapchain.hpp"
#include "Framebuffer.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class RenderPass : public Wrapper<VkRenderPass> {
    public:
        RenderPass(VkRenderPass handle, std::shared_ptr<Device> device, const std::shared_ptr<Swapchain>& swapchain);

        ~RenderPass() override;

        void begin(const VkCommandBuffer &cmdBuffer, const std::vector<VkClearValue> &clearValues, uint32_t index);

        void end(const VkCommandBuffer &cmdBuffer);

    private:
        std::shared_ptr<Device> device;
        std::vector<std::unique_ptr<Framebuffer>> framebuffers;
    };

    struct RenderPassBuilder {
        explicit RenderPassBuilder(std::shared_ptr<Device> device, std::shared_ptr<Swapchain> swapchain);

        RenderPassBuilder& addAttachment(VkAttachmentDescription attachmentDescription);

        RenderPassBuilder& addColorAttachmentReference(VkAttachmentReference reference);

        RenderPassBuilder& setDepthAttachmentReference(VkAttachmentReference reference);

        RenderPassBuilder& addSubpassDependency(VkSubpassDependency subpassDependency);

        std::unique_ptr<RenderPass> build();

        VkPipelineBindPoint bindPoint{};
        VkAttachmentReference depthAttachmentReference{};
        std::shared_ptr<Device> device;
        std::shared_ptr<Swapchain> swapchain;
        std::vector<VkAttachmentDescription> attachmentsDescriptions;
        std::vector<VkAttachmentReference> colorAttachmentsReferences;
        std::vector<VkSubpassDependency> dependencies;
    };

}


#endif //LITTLEVULKANENGINE_RENDERPASS_HPP
