#ifndef LITTLEVULKANENGINE_INSTANCE_HPP
#define LITTLEVULKANENGINE_INSTANCE_HPP


#include <vector>

#include <volk.h>

#include "utils/Utils.hpp"
#include "utils/Wrapper.hpp"
#include "utils/NonCopyable.hpp"


namespace lve {

    class Instance : public Wrapper<VkInstance>, public NonCopyable {
        friend class InstanceBuilder;

    public:
        explicit Instance(VkInstance handle);

        ~Instance() override;

        [[nodiscard]] std::vector<VkPhysicalDevice> getPhysicalDevices() const;

#ifdef LVE_DEBUG
    private:
        VkDebugReportCallbackEXT debugReportCallback{VK_NULL_HANDLE};
#endif
    };

    struct InstanceBuilder {
        InstanceBuilder() = default;

        InstanceBuilder& setApplicationName(std::string name);

        InstanceBuilder& setApplicationVersion(uint32_t version);

        InstanceBuilder& setVulkanVersion(uint32_t version);

        InstanceBuilder& addExtension(const char* name);

#ifdef LVE_DEBUG
        InstanceBuilder& addValidationLayer(const char* name);
#endif

        std::unique_ptr<Instance> build();

        std::string appName{"Unnamed"};
        uint32_t appVersion{VK_MAKE_VERSION(0, 0 , 0)};
        uint32_t vulkanVersion{VK_VERSION_1_3};
        std::vector<const char*> extensions;
#ifdef LVE_DEBUG
        std::vector<const char*> validationLayers;
#endif
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_INSTANCE_HPP
