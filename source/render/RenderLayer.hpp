#ifndef LITTLEVULKANENGINE_RENDERLAYER_HPP
#define LITTLEVULKANENGINE_RENDERLAYER_HPP


#include "Camera.hpp"
#include "Device.hpp"
#include "Pipeline.hpp"
#include "Swapchain.hpp"
#include "RenderPass.hpp"
#include "scene/Scene.hpp"
#include "descriptor/DescriptorPool.hpp"
#include "descriptor/DescriptorLayout.hpp"


namespace lve {

    class RenderLayer {
    public:
        friend class Renderer;

        RenderLayer() = default;

        virtual ~RenderLayer();

        virtual void setup(std::shared_ptr<Device> device, std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain>& swapchain);

        virtual void resize(std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain>& swapchain);

        virtual void update(const std::shared_ptr<Scene>& scene, float dt) = 0;

        virtual void render(const VkCommandBuffer& commandBuffer, const VkExtent2D& extent, uint32_t imageIndex, const Camera& camera) = 0;

    private:
        virtual void createRenderPass(const std::shared_ptr<Swapchain>& swapchain) = 0;

        virtual void createPipeline() = 0;

        virtual void setupDescriptors() = 0;

    public:
        std::shared_ptr<Device> device;
        std::shared_ptr<Pipeline> pipeline;
        std::shared_ptr<DescriptorPool> pool;
        std::shared_ptr<RenderPass> renderPass;
        std::vector<std::shared_ptr<DescriptorLayout>> layouts;
    };


} // lve


#endif //LITTLEVULKANENGINE_RENDERLAYER_HPP
