#ifndef LITTLEVULKANENGINE_IMAGE_HPP
#define LITTLEVULKANENGINE_IMAGE_HPP


#include <memory>

#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    using ImageFlags = uint32_t;

    enum ImageFlag {
        None = 0,
        IsSwapChainImage = 1
    };

    class Image : public Wrapper<VkImage> {
    public:
        Image(VkImage handle, const std::shared_ptr<Device>& device, VkImageView view, VmaAllocation allocation, ImageFlags flags = None);

        ~Image() override;

        void transitionLayout(VkImageLayout oldLayout, VkImageLayout newLayout);

        [[nodiscard]] inline VkImageView getView() const { return view; }

    private:
        ImageFlags flags{};
        VkImageView view{};
        VmaAllocator allocator;
        VmaAllocation allocation{};
        std::shared_ptr<Device> device;
    };

    struct ImageBuilder {
        explicit ImageBuilder(std::shared_ptr<Device> device);

        ImageBuilder& setHandle(VkImage handle);

        ImageBuilder& setFlags(ImageFlags flags);

        ImageBuilder& setType(VkImageType type);

        ImageBuilder& setFormat(VkFormat format);

        ImageBuilder& setExtent(VkExtent3D extent);

        ImageBuilder& setMipLevel(uint32_t mipLevels);

        ImageBuilder& setArrayLayer(uint32_t arrayLayers);

        ImageBuilder& setSamplerCount(VkSampleCountFlagBits sampleCount);

        ImageBuilder& setTiling(VkImageTiling tiling);

        ImageBuilder& setUsage(VkImageUsageFlags usage);

        ImageBuilder& setSharingMode(VkSharingMode sharingMode);

        ImageBuilder& setImageLayout(VkImageLayout imageLayout);

        ImageBuilder& setAspect(VkImageAspectFlagBits aspect);

        ImageBuilder& setMemoryUsage(VmaMemoryUsage memoryUsage);

        ImageBuilder& setComponent(VkComponentMapping components);

        std::unique_ptr<Image> build();

        ImageFlags flags{};
        VkImageType type{};
        VkFormat format{};
        VkExtent3D extent{};
        uint32_t mipLevels{};
        uint32_t arrayLayers{};
        VkSampleCountFlagBits sampleCount{};
        VkImageTiling tiling{};
        VkImageUsageFlags usage{};
        VkSharingMode sharingMode{};
        VkImageLayout imageLayout{};
        VkImageAspectFlagBits aspect{};
        VkComponentMapping components{};
        VkImageSubresourceRange subresourceRange{};
        VmaMemoryUsage memoryUsage{};
        VkImage handle{};
        std::shared_ptr<Device> device;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_IMAGE_HPP
