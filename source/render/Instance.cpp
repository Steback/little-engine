#include "Instance.hpp"

#include "logs/Logs.hpp"


namespace lve {

    Instance::Instance(VkInstance handle) : Wrapper<VkInstance>(handle) {
#ifdef LVE_DEBUG
        LVE_LOG_INFO("[Vulkan] Debug report callback object created");

        VkDebugReportCallbackCreateInfoEXT createInfo{VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT};
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
        createInfo.pfnCallback = reinterpret_cast<PFN_vkDebugReportCallbackEXT>(+[](VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT, std::uint64_t, std::size_t, std::int32_t, const char *, const char *message, void *user_data) {
            if ((flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) != 0) {
                LVE_LOG_INFO(message);
            } else if ((flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) != 0) {
                LVE_LOG_ERROR(message);
            } else {
                // This also deals with VK_DEBUG_ENGINEPORT_PERFORMANCE_WARNING_BIT_EXT.
                LVE_LOG_WARN(message);
            }

            return VK_FALSE;
        });

        LVE_VK_CHECK(vkCreateDebugReportCallbackEXT(handle, &createInfo, nullptr, &debugReportCallback))
#endif
    }

    Instance::~Instance() {
#ifdef LVE_DEBUG
        if (debugReportCallback) vkDestroyDebugReportCallbackEXT(handle, debugReportCallback, nullptr);
#endif

        if (handle) vkDestroyInstance(handle, nullptr);
    }

    std::vector<VkPhysicalDevice> Instance::getPhysicalDevices() const {
        uint32_t gpuCount = 0;
        vkEnumeratePhysicalDevices(handle, &gpuCount, nullptr);

        if (gpuCount == 0) {
            LVE_LOG_ERROR("[Vulkan] Could not find any graphics cards!");
            return {};
        }

        std::vector<VkPhysicalDevice> availableGpus(gpuCount);
        vkEnumeratePhysicalDevices(handle, &gpuCount, availableGpus.data());

        return std::move(availableGpus);
    }

    InstanceBuilder& InstanceBuilder::setApplicationName(std::string name) {
        appName = std::move(name);
        return *this;
    }

    InstanceBuilder& InstanceBuilder::setApplicationVersion(uint32_t version) {
        appVersion = version;
        return *this;
    }

    InstanceBuilder& InstanceBuilder::setVulkanVersion(uint32_t version) {
        vulkanVersion = version;
        return *this;
    }

    InstanceBuilder& InstanceBuilder::addExtension(const char *name) {
        extensions.push_back(name);
        return *this;
    }

#ifdef LVE_DEBUG
    InstanceBuilder& InstanceBuilder::addValidationLayer(const char *name) {
        validationLayers.push_back(name);
        return *this;
    }
#endif

    std::unique_ptr<Instance> InstanceBuilder::build() {
        LVE_LOG_INFO("[Vulkan] Instance extensions required: ");
        for (const auto& extension : extensions) {
            LVE_LOG_INFO("\t- {}", extension);
        }

#ifdef LVE_DEBUG
        LVE_LOG_INFO("[Vulkan] Instance validation layers required: ");
        for (const auto& validationLayer : validationLayers) {
            LVE_LOG_INFO("\t - {}", validationLayer);
        }
#endif

        VkApplicationInfo applicationInfo{VK_STRUCTURE_TYPE_APPLICATION_INFO};
        applicationInfo.pApplicationName = appName.c_str();
        applicationInfo.applicationVersion = appVersion;
        applicationInfo.pEngineName = "Custom Engine";
        applicationInfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);
        applicationInfo.apiVersion = vulkanVersion;

        VkInstanceCreateInfo createInfo{VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
        createInfo.pApplicationInfo = &applicationInfo;
        createInfo.enabledExtensionCount = extensions.size();
        createInfo.ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data();
#ifdef LVE_DEBUG
        createInfo.enabledLayerCount = validationLayers.size();
        createInfo.ppEnabledLayerNames = validationLayers.empty() ? nullptr : validationLayers.data();
#else
        createInfo.enabledLayerCount = 0;
        createInfo.ppEnabledLayerNames = nullptr;
#endif

        VkInstance handle;
        LVE_VK_CHECK(vkCreateInstance(&createInfo, nullptr, &handle))

        volkLoadInstance(handle);

        return std::make_unique<Instance>(handle);
    }

} // namespace lv
