#ifndef LITTLEVULKANENGINE_SCENERENDER_HPP
#define LITTLEVULKANENGINE_SCENERENDER_HPP


#include "Camera.hpp"
#include "scene/Scene.hpp"
#include "RenderLayer.hpp"


namespace lve {

    class SceneRender : public RenderLayer {
    public:
        SceneRender() = default;

        ~SceneRender() override = default;

        void update(const std::shared_ptr<Scene>& scene, float dt) override;

        void render(const VkCommandBuffer &commandBuffer, const VkExtent2D& extent, uint32_t imageIndex, const Camera& camera) override;

    private:
        void setupDescriptors() override;

        void createPipeline() override;

        void createRenderPass(const std::shared_ptr<Swapchain> &swapchain) override;

    private:
        std::shared_ptr<Scene> scene;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_SCENERENDER_HPP
