#ifndef LITTLEVULKANENGINE_IMGUIRENDER_HPP
#define LITTLEVULKANENGINE_IMGUIRENDER_HPP


#include "RenderLayer.hpp"
#include "buffer/Buffer.hpp"
#include "assets/Texture.hpp"


namespace lve {

    class ImGuiSettings;

    class ImGuiRender : public RenderLayer {
    public:
        ImGuiRender(const std::shared_ptr<ImGuiSettings>& settings);

        ~ImGuiRender() override;

        void setup(std::shared_ptr<Device> device, std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain> &swapchain) override;

        void resize(std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain> &swapchain) override;

        void update(const std::shared_ptr<Scene>& scene, float dt) override;

        void render(const VkCommandBuffer &commandBuffer, const VkExtent2D &extent, uint32_t imageIndex, const Camera &camera) override;

    private:
        void setupDescriptors() override;

        void createRenderPass(const std::shared_ptr<Swapchain> &swapchain) override;

        void createPipeline() override;

        void updateBuffers();

        void draw(const VkCommandBuffer& commandBuffer);

    private:
        uint32_t indexCount{};
        uint32_t vertexCount{};
        VkDescriptorSet fontTextureDscriptorSet{};
        std::unique_ptr<Buffer> indexBuffer;
        std::unique_ptr<Buffer> vertexBuffer;
        std::unique_ptr<Texture> fontTexture;
        std::shared_ptr<ImGuiSettings> settings;
    };

    struct ImGuiSettings : public SettingsInterface {
        LVE_REGISTRY_SETTINGS_FIELD(ImGui)

        ImGuiSettings() = default;

        void save(nlohmann::json &archive) override;

        void load(const nlohmann::json &archive) override;

        void draw() override;

        uint32_t fontSize{16};
        std::string font{"fonts/Roboto-Medium.ttf"};
    };

}


#endif //LITTLEVULKANENGINE_IMGUIRENDER_HPP
