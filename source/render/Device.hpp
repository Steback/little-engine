#ifndef LITTLEVULKANENGINE_DEVICE_HPP
#define LITTLEVULKANENGINE_DEVICE_HPP


#include <unordered_map>

#include <volk.h>
#include <vk_mem_alloc.h>

#include "Surface.hpp"
#include "PhysicalDevice.hpp"


namespace lve {

    class Device : public Wrapper<VkDevice> {
    public:
        Device(VkDevice handle, std::shared_ptr<PhysicalDevice> physicalDevice, const std::unordered_map<VkQueueFlagBits, uint32_t>& queues, std::shared_ptr<Surface> surface = nullptr);

        ~Device() override;

        Queue getQueue(VkQueueFlagBits type);

        void waitIdle();

        [[nodiscard]] inline VmaAllocator getAllocator() const { return allocator; }

        [[nodiscard]] inline std::shared_ptr<Surface> getSurface() const { return surface; }

        [[nodiscard]] inline std::shared_ptr<PhysicalDevice> getPhysicalDevice() const { return physicalDevice; }

        [[nodiscard]] inline std::shared_ptr<Instance> getInstance() const { return physicalDevice->getInstance(); }

    private:
        VmaAllocator allocator{};
        std::shared_ptr<Surface> surface;
        std::shared_ptr<PhysicalDevice> physicalDevice;
        std::unordered_map<VkQueueFlagBits, Queue> queues;
    };

    struct DeviceBuilder {
        explicit DeviceBuilder(std::shared_ptr<PhysicalDevice> physicalDevice);

        DeviceBuilder& addQueueType(VkQueueFlagBits type);

        DeviceBuilder& setSurface(std::shared_ptr<Surface> surface);

        DeviceBuilder& addExtension(const char* name);

        std::unique_ptr<Device> build();

        std::shared_ptr<Surface> surface;
        std::shared_ptr<PhysicalDevice> physicalDevice;
        std::vector<VkQueueFlagBits> queuesTypes;
        std::vector<const char*> extensions;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_DEVICE_HPP
