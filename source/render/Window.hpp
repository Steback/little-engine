#ifndef LITTLEVULKANENGINE_WINDOW_HPP
#define LITTLEVULKANENGINE_WINDOW_HPP


#include <volk.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include "utils/Wrapper.hpp"
#include "config/Settings.hpp"
#include "utils/NonCopyable.hpp"


namespace lve {

    class WindowSettings;
    class InputManager;

    class Window : public Wrapper<GLFWwindow*> {
    public:
        explicit Window(const std::shared_ptr<WindowSettings>& settings);

        ~Window() override;

        void waitForFocus() const;

        float getAspectRatio() const;

        void setTitle(const std::string& title);

        [[nodiscard]] inline std::string getTitle() const { return title; }

        [[nodiscard]] inline bool isOpen() const { return !glfwWindowShouldClose(handle); }

        [[nodiscard]] inline VkExtent2D getExtent() const { return {(uint32_t)width, (uint32_t)height}; }

        [[nodiscard]] inline VkExtent2D getFramebufferExtent() const { return {(uint32_t)framebufferWidth, (uint32_t)framebufferHeight}; }

    private:
        static void resizeCallback(GLFWwindow* window, int width, int height);

        static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

        static void windowFocusCallback(GLFWwindow* window, int focused);

        static void cursorEnterCallback(GLFWwindow* window, int entered);

        static void cursorPosCallback(GLFWwindow* window, double x, double y);

        static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

        static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

        static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

        static void charCallback(GLFWwindow* window, unsigned int c);

    private:
        int width{};
        int height{};
        int framebufferWidth{};
        int framebufferHeight{};
        std::string title;
        std::shared_ptr<WindowSettings> settings;
        glm::vec<2, double> lastCursorPos{};
    };

    struct WindowSettings : public SettingsInterface {
        LVE_REGISTRY_SETTINGS_FIELD(Window)

        WindowSettings() = default;

        ~WindowSettings() override = default;

        void save(nlohmann::json &archive) override;

        void load(const nlohmann::json &archive) override;

        void draw() override;

        std::string title{"Unknown"};
        int width{1920};
        int height{1080};
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_WINDOW_HPP
