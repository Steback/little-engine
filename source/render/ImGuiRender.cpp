#include "ImGuiRender.hpp"

#include <imgui.h>
#include <glm/glm.hpp>

#include "scene/Scene.hpp"
#include "assets/AssetsManager.hpp"
#include "descriptor/DescriptorWriter.hpp"


namespace lve {

    struct PushConstBlock {
        glm::vec2 scale;
        glm::vec2 translate;
    } pushConstBlock;

    ImGuiRender::ImGuiRender(const std::shared_ptr<ImGuiSettings>& settings)
            : settings(settings) {
        ImGui::CreateContext();
        ImGui::StyleColorsDark();

        ImGuiIO& io = ImGui::GetIO();
        io.FontGlobalScale = 1.0f;
        io.Fonts->AddFontFromFileTTF(AssetsManager::get()->getFile(settings->font).getPath().string().c_str(), (float)settings->fontSize);
    }

    ImGuiRender::~ImGuiRender() {
        ImGui::DestroyContext();

        if (vertexBuffer) {
            vertexBuffer->unmap();
        }

        if (indexBuffer) {
            indexBuffer->unmap();
        }

        vertexBuffer.reset();
        indexBuffer.reset();
        fontTexture.reset();
    }

    void ImGuiRender::setup(std::shared_ptr<Device> device, std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain> &swapchain) {
        RenderLayer::setup(std::move(device), std::move(renderPass), swapchain);

        unsigned char* fontData;
        int texWidth, texHeight;
        ImGuiIO& io = ImGui::GetIO();
        io.Fonts->GetTexDataAsRGBA32(&fontData, &texWidth, &texHeight);

        const VkDeviceSize uploadSize = texWidth*texHeight * 4 * sizeof(char);
        fontTexture = std::make_unique<Texture>(this->device, VkExtent3D{(uint32_t)texWidth, (uint32_t)texHeight, 1}, fontData, uploadSize);

        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.sampler = fontTexture->getSampler();
        imageInfo.imageView = fontTexture->getImage()->getView();

        DescriptorWriter writer(layouts[0], pool);
        writer.writeImage(0, &imageInfo);
        writer.build(fontTextureDscriptorSet);
    }

    void ImGuiRender::resize(std::shared_ptr<RenderPass> renderPass, const std::shared_ptr<Swapchain> &swapchain) {
        RenderLayer::resize(std::move(renderPass), swapchain);

        ImGuiIO& io = ImGui::GetIO();
        auto extent = swapchain->getExtent();
        io.DisplaySize = ImVec2((float)extent.width, (float)extent.height);
    }

    void ImGuiRender::update(const std::shared_ptr<Scene>& scene, float dt) {
        ImGuiIO& io = ImGui::GetIO();
        io.DeltaTime = dt;
        updateBuffers();
    }

    void ImGuiRender::render(const VkCommandBuffer &commandBuffer, const VkExtent2D &extent, uint32_t imageIndex, const Camera &camera) {
        ImGuiIO& io = ImGui::GetIO();
        VkViewport viewport{};
        viewport.width = static_cast<float>(io.DisplaySize.x);
        viewport.height = static_cast<float>(io.DisplaySize.y);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

        VkRect2D scissor{};
        scissor.extent = {(uint32_t)io.DisplaySize.x, (uint32_t)io.DisplaySize.y};
        scissor.offset = {};
        vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getLayout(), 0, 1, &fontTextureDscriptorSet, 0, nullptr);
        pipeline->bind(commandBuffer);

        // UI scale and translate via push constants
        pushConstBlock.scale = glm::vec2(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y);
        pushConstBlock.translate = glm::vec2(-1.0f);
        vkCmdPushConstants(commandBuffer, pipeline->getLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(PushConstBlock), &pushConstBlock);

        draw(commandBuffer);
    }

    void ImGuiRender::setupDescriptors() {
        pool = DescriptorPoolBuilder(device)
                .setMaxSets(2)
                .addPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1)
                .build();

        DescriptorLayoutBuilder descriptorLayoutBuilder(device);
        descriptorLayoutBuilder.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT, 1);
        layouts.push_back(descriptorLayoutBuilder.build());
    }

    void ImGuiRender::createRenderPass(const std::shared_ptr<Swapchain> &swapchain) {

    }

    void ImGuiRender::createPipeline() {
        VkPipelineColorBlendAttachmentState blendAttachmentState{};
        blendAttachmentState.blendEnable = VK_TRUE;
        blendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        blendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        blendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
        blendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

        auto vertShader = std::make_unique<Shader>(device, AssetsManager::get()->getFile("shaders/ImGui.vert.spv").getPath(), VK_SHADER_STAGE_VERTEX_BIT);
        auto fragShader = std::make_unique<Shader>(device, AssetsManager::get()->getFile("shaders/ImGui.frag.spv").getPath(), VK_SHADER_STAGE_FRAGMENT_BIT);

        PipelineBuilder pipelineBuilder(device, renderPass);
        pipelineBuilder.useCache(true);
        pipelineBuilder.setPipelineColorBlend(blendAttachmentState);
        pipelineBuilder.addDynamicState(VK_DYNAMIC_STATE_VIEWPORT);
        pipelineBuilder.addDynamicState(VK_DYNAMIC_STATE_SCISSOR);
        pipelineBuilder.addPushConstantRange({VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(PushConstBlock)});
        pipelineBuilder.addBindingDescription({0, sizeof(ImDrawVert), VK_VERTEX_INPUT_RATE_VERTEX});
        pipelineBuilder.addAttributeDescription({0, 0, VK_FORMAT_R32G32_SFLOAT, offsetof(ImDrawVert, pos)});
        pipelineBuilder.addAttributeDescription({1, 0, VK_FORMAT_R32G32_SFLOAT, offsetof(ImDrawVert, uv)});
        pipelineBuilder.addAttributeDescription({2, 0, VK_FORMAT_R8G8B8A8_UNORM, offsetof(ImDrawVert, col)});
        pipelineBuilder.addShaderStage(vertShader->getPipelineStageCreateInfo());
        pipelineBuilder.addShaderStage(fragShader->getPipelineStageCreateInfo());

        for (const auto& layout : layouts) {
            pipelineBuilder.addSetLayout(*layout);
        }

        pipeline = pipelineBuilder.build();
    }

    void ImGuiRender::updateBuffers() {
        ImDrawData* imDrawData = ImGui::GetDrawData();

        // Note: Alignment is done inside buffer creation
        VkDeviceSize vertexBufferSize = imDrawData->TotalVtxCount * sizeof(ImDrawVert);
        VkDeviceSize indexBufferSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);

        if ((vertexBufferSize == 0) || (indexBufferSize == 0)) {
            return;
        }

        // Update buffers only if vertex or index count has been changed compared to current buffer size

        // Vertex buffer
        if ((vertexBuffer == nullptr) || (vertexCount != imDrawData->TotalVtxCount)) {
            if (vertexBuffer) {
                vertexBuffer->unmap();
                vertexBuffer.reset();
            }

            vertexBuffer = std::make_unique<Buffer>(device, vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
            vertexCount = imDrawData->TotalVtxCount;
            vertexBuffer->map();
        }

        // Index buffer
        if ((indexBuffer == nullptr) || (indexCount != imDrawData->TotalIdxCount)) {
            if (indexBuffer) {
                indexBuffer->unmap();
                indexBuffer.reset();
            }

            indexBuffer = std::make_unique<Buffer>(device, indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
            indexCount = imDrawData->TotalIdxCount;
            indexBuffer->map();
        }

        // Upload data
        auto* vtxDst = (ImDrawVert*)vertexBuffer->mapped;
        auto* idxDst = (ImDrawIdx*)indexBuffer->mapped;

        for (int n = 0; n < imDrawData->CmdListsCount; n++) {
            const ImDrawList* cmdList = imDrawData->CmdLists[n];
            memcpy(vtxDst, cmdList->VtxBuffer.Data, cmdList->VtxBuffer.Size * sizeof(ImDrawVert));
            memcpy(idxDst, cmdList->IdxBuffer.Data, cmdList->IdxBuffer.Size * sizeof(ImDrawIdx));
            vtxDst += cmdList->VtxBuffer.Size;
            idxDst += cmdList->IdxBuffer.Size;
        }

        // Flush to make writes visible to GPU
        vertexBuffer->flush();
        indexBuffer->flush();
    }

    void ImGuiRender::draw(const VkCommandBuffer& commandBuffer) {
        ImDrawData* imDrawData = ImGui::GetDrawData();
        int32_t vertexOffset = 0;
        int32_t indexOffset = 0;

        if (imDrawData->CmdListsCount > 0) {
            VkDeviceSize offsets[1] = { 0 };
            vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffer->get(), offsets);
            vkCmdBindIndexBuffer(commandBuffer, indexBuffer->get(), 0, VK_INDEX_TYPE_UINT16);

            for (int32_t i = 0; i < imDrawData->CmdListsCount; i++) {
                const ImDrawList *cmdList = imDrawData->CmdLists[i];
                for (int32_t j = 0; j < cmdList->CmdBuffer.Size; j++) {
                    const ImDrawCmd *pcmd = &cmdList->CmdBuffer[j];
                    VkRect2D scissorRect;
                    scissorRect.offset.x = std::max((int32_t)(pcmd->ClipRect.x), 0);
                    scissorRect.offset.y = std::max((int32_t)(pcmd->ClipRect.y), 0);
                    scissorRect.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
                    scissorRect.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y);
                    vkCmdSetScissor(commandBuffer, 0, 1, &scissorRect);
                    vkCmdDrawIndexed(commandBuffer, pcmd->ElemCount, 1, pcmd->IdxOffset + indexOffset, pcmd->VtxOffset + vertexOffset, 0);
                }
                indexOffset += cmdList->IdxBuffer.Size;
                vertexOffset += cmdList->VtxBuffer.Size;
            }
        }
    }

    void ImGuiSettings::save(nlohmann::json &archive) {
        LVE_SAVE_FIELD_ATTRIBUTE(archive, font);
        LVE_SAVE_FIELD_ATTRIBUTE(archive, fontSize);
    }

    void ImGuiSettings::load(const nlohmann::json &archive) {
        LVE_LOAD_FIELD_ATTRIBUTE(archive, font);
        LVE_LOAD_FIELD_ATTRIBUTE(archive, fontSize);
    }

    void ImGuiSettings::draw() {
        imSerialize("Font", font);
        imSerialize("Font Size", fontSize);
    }

}
