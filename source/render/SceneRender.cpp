#include "SceneRender.hpp"

#include <glm/glm.hpp>

#include "assets/Mesh.hpp"
#include "render/Camera.hpp"
#include "assets/AssetsManager.hpp"
#include "components/RenderComponent.hpp"
#include "components/TransformComponent.hpp"


namespace lve {

    struct SimplePushConstantData {
        glm::mat4 transform{1.f};
    };

    void SceneRender::update(const std::shared_ptr<Scene>& inScene, float dt) {
        scene = inScene;
    }

    void SceneRender::render(const VkCommandBuffer &commandBuffer, const VkExtent2D& extent, uint32_t imageIndex, const Camera& camera) {
        if (!scene) {
            return;
        }

        pipeline->bind(commandBuffer);

        VkViewport viewport{};
        viewport.width = static_cast<float>(extent.width);
        viewport.height = static_cast<float>(extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

        VkRect2D scissor{};
        scissor.offset = {0, 0};
        scissor.extent = extent;
        vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

        auto view = scene->getRegistry().view<RenderComponent>();
        for (const auto entity : view) {
            auto& transform = scene->getRegistry().get<TransformComponent>(entity);
            auto viewProjection = camera.getProjection() * camera.getView();

            SimplePushConstantData push{};
            auto modelMatrix = transform.worldTransform();
            push.transform = viewProjection * modelMatrix;

            vkCmdPushConstants(commandBuffer, pipeline->getLayout(),
                    VK_SHADER_STAGE_VERTEX_BIT,
                    0,
                    sizeof(SimplePushConstantData),
                    &push);

            auto& renderComponent = view.get<RenderComponent>(entity);
            renderComponent.mesh->bind(commandBuffer);
            renderComponent.material->bind(commandBuffer, pipeline->getLayout());
            renderComponent.mesh->draw(commandBuffer);
        }
    }

    void SceneRender::setupDescriptors() {
        pool = DescriptorPoolBuilder(device)
                .setMaxSets(Swapchain::maxFramesInFlight)
                .addPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, AssetsManager::get()->getMaterialsCount())
                .build();

        DescriptorLayoutBuilder descriptorLayoutBuilder(device);
        descriptorLayoutBuilder.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT, AssetsManager::get()->getMaterialsCount());
        layouts.push_back(descriptorLayoutBuilder.build());

        AssetsManager::get()->setupDescriptorsSets(pool, layouts[0]);
    }

    void SceneRender::createPipeline() {
        VkPipelineColorBlendAttachmentState blendAttachmentState{};
        blendAttachmentState.blendEnable = VK_TRUE;
        blendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        blendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        blendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
        blendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

        auto vertShader = std::make_shared<Shader>(device, AssetsManager::get()->getFile("shaders/Model.vert.spv").getPath(), VK_SHADER_STAGE_VERTEX_BIT);
        auto fragShader = std::make_shared<Shader>(device, AssetsManager::get()->getFile("shaders/Model.frag.spv").getPath(), VK_SHADER_STAGE_FRAGMENT_BIT);

        PipelineBuilder pipelineBuilder(device, renderPass);
        pipelineBuilder.useCache(true);
        pipelineBuilder.setPipelineColorBlend(blendAttachmentState);
        pipelineBuilder.addDynamicState(VK_DYNAMIC_STATE_VIEWPORT);
        pipelineBuilder.addDynamicState(VK_DYNAMIC_STATE_SCISSOR);
        pipelineBuilder.addShaderStage(vertShader->getPipelineStageCreateInfo());
        pipelineBuilder.addShaderStage(fragShader->getPipelineStageCreateInfo());
        pipelineBuilder.addPushConstantRange({VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(SimplePushConstantData)});

        auto vertexBindings = Vertex::getBindingDescriptions();
        for (const auto& binding : vertexBindings) {
            pipelineBuilder.addBindingDescription(binding);
        }

        auto vertexAttributes = Vertex::getAttributeDescriptions();
        for (const auto& attribute : vertexAttributes) {
            pipelineBuilder.addAttributeDescription(attribute);
        }

        for (const auto& layout : layouts) {
            pipelineBuilder.addSetLayout(*layout);
        }

        pipeline = pipelineBuilder.build();
    }

    void SceneRender::createRenderPass(const std::shared_ptr<Swapchain> &swapchain) {

    }


} // namespace lve