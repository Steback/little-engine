#include "Command.hpp"


namespace lve {

    CommandBuffer::CommandBuffer(const VkDevice& device, VkCommandPool pool, VkCommandBufferLevel level)
            : device(device), pool(pool) {
        VkCommandBufferAllocateInfo allocateBufferInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
        allocateBufferInfo.commandBufferCount = 1;
        allocateBufferInfo.commandPool = pool;
        allocateBufferInfo.level = level;

        LVE_VK_CHECK(vkAllocateCommandBuffers(this->device, &allocateBufferInfo, &handle))
    }

    CommandBuffer::~CommandBuffer() {
        vkFreeCommandBuffers(device, pool, 1, &handle);
    }

    void CommandBuffer::begin(uint32_t flags) {
        VkCommandBufferBeginInfo beginInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
        beginInfo.flags = flags;

        LVE_VK_CHECK(vkBeginCommandBuffer(handle, &beginInfo))
    }

    void CommandBuffer::end(VkQueue queue) {
        LVE_VK_CHECK(vkEndCommandBuffer(handle))

        if (queue) {
            VkSubmitInfo submitInfo{VK_STRUCTURE_TYPE_SUBMIT_INFO};
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &handle;

            LVE_VK_CHECK(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE))
            vkQueueWaitIdle(queue);
        }
    }

    CommandPool::CommandPool(const std::shared_ptr<Device>& device, uint32_t queueFamilyIndex, uint32_t flags)
            : device(*device) {
        VkCommandPoolCreateInfo createInfo{VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
        createInfo.flags = flags;
        createInfo.queueFamilyIndex = queueFamilyIndex;

        LVE_VK_CHECK(vkCreateCommandPool(this->device, &createInfo, nullptr, &handle))
    }

    CommandPool::~CommandPool() {
        vkDestroyCommandPool(device, handle, nullptr);
    }

    std::unique_ptr<CommandBuffer> CommandPool::createCmdBuffer(bool oneTimeSubmit) {
        auto commandBuffer = std::make_unique<CommandBuffer>(device, handle);
        if (oneTimeSubmit) {
            commandBuffer->begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        }
        return std::move(commandBuffer);
    }

    OnceCommandBuffer::OnceCommandBuffer(const std::shared_ptr<Device>& device, uint32_t queueFamilyIndex, VkQueue queue, bool startRecording)
            : pool(device, queueFamilyIndex), buffer(*device, pool), queue(queue) {
        handle = buffer;
        if (startRecording) {
            start();
        }
    }

    OnceCommandBuffer::~OnceCommandBuffer() {
        if (isRecording) {
            end();
        }
    }

    void OnceCommandBuffer::start() {
        buffer.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        isRecording = true;
    }

    void OnceCommandBuffer::end() {
        buffer.end(queue);
        isRecording = false;
    }

}
