#ifndef LITTLEVULKANENGINE_RENDERER_HPP
#define LITTLEVULKANENGINE_RENDERER_HPP


#include <memory>

#include "Fence.hpp"
#include "Window.hpp"
#include "Camera.hpp"
#include "Device.hpp"
#include "Command.hpp"
#include "Instance.hpp"
#include "Semaphore.hpp"
#include "Swapchain.hpp"
#include "RenderPass.hpp"
#include "RenderLayer.hpp"
#include "PhysicalDevice.hpp"
#include "utils/Singleton.hpp"
#include "config/Settings.hpp"
#include "utils/NonCopyable.hpp"


namespace lve {

    class RendererSettings;

    class Renderer : public Singleton<Renderer> {
        friend Singleton<Renderer>;

        Renderer(std::shared_ptr<Window> window, const std::shared_ptr<Settings>& settings);

    public:
        ~Renderer();

        void render(const Camera& camera);

        void recreate();

        void addLayer(const std::shared_ptr<RenderLayer>& renderLayer);

        void clearLayer();

        void waitForDevice();

        [[nodiscard]] std::shared_ptr<RenderLayer> getRenderLayer(uint32_t index) const { return layers[index]; }

        [[nodiscard]] inline std::shared_ptr<Window> getWindow() const { return window; }

        [[nodiscard]] inline std::shared_ptr<Device> getDevice() const { return device; }

        [[nodiscard]] inline std::shared_ptr<Swapchain> getSwapchain() const { return swapchain; }

        [[nodiscard]] inline std::shared_ptr<Instance> getInstance() const { return instance; }

        [[nodiscard]] inline std::shared_ptr<PhysicalDevice> getPhysicalDevice() const { return physicalDevice; }

    private:
        void createRenderPass();

    private:
        uint32_t imageIndex = 0;
        uint32_t currentFrame = 0;
        std::shared_ptr<Device> device;
        std::shared_ptr<Window> window;
        std::shared_ptr<Instance> instance;
        std::shared_ptr<Swapchain> swapchain;
        std::shared_ptr<RendererSettings> settings;
        std::shared_ptr<PhysicalDevice> physicalDevice;
        std::shared_ptr<CommandPool> commandPool;
        std::shared_ptr<RenderPass> renderPass;
        std::vector<std::shared_ptr<CommandBuffer>> commandBuffers;
        std::vector<std::shared_ptr<Fence>> inFlightFences;
        std::vector<std::shared_ptr<Fence>> imagesInFlight;
        std::vector<std::shared_ptr<Semaphore>> imageAvailableSemaphores;
        std::vector<std::shared_ptr<Semaphore>> renderFinishedSemaphores;
        std::vector<std::shared_ptr<RenderLayer>> layers;
    };

    struct RendererSettings : public SettingsInterface {
        LVE_REGISTRY_SETTINGS_FIELD(Renderer)

        RendererSettings() = default;

        ~RendererSettings() override = default;

        void load(const nlohmann::json &archive) override;

        void save(nlohmann::json &archive) override;

        void draw() override;

        bool vsync{true};
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_RENDERER_HPP
