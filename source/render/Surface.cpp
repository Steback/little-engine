#include "Surface.hpp"


namespace lve {

    Surface::Surface(const std::shared_ptr<Instance>& instance, const std::shared_ptr<Window>& window)
            : instance(*instance) {
        LVE_VK_CHECK(glfwCreateWindowSurface(*instance, *window, nullptr, &handle));
    }

    Surface::~Surface() {
        if (handle) {
            vkDestroySurfaceKHR(instance, handle, nullptr);
        }
    }

}
