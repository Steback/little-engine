#ifndef LITTLEVULKANENGINE_RENDER_COMMAND_HPP
#define LITTLEVULKANENGINE_RENDER_COMMAND_HPP


#include "Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class CommandBuffer : public Wrapper<VkCommandBuffer> {
    public:
        CommandBuffer(const VkDevice& device, VkCommandPool pool, VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);

        ~CommandBuffer() override;

        void begin(uint32_t flags = 0);

        void end(VkQueue queue = VK_NULL_HANDLE);

    private:
        VkDevice device;
        VkCommandPool pool;
    };

    class CommandPool : public Wrapper<VkCommandPool> {
    public:
        CommandPool(const std::shared_ptr<Device>& device, uint32_t queueFamilyIndex, uint32_t flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

        ~CommandPool() override;

        std::unique_ptr<CommandBuffer> createCmdBuffer(bool oneTimeSubmit = false);

    private:
        VkDevice device;
    };

    class OnceCommandBuffer : public Wrapper<VkCommandBuffer> {
    public:
        OnceCommandBuffer(const std::shared_ptr<Device>& device, uint32_t queueFamilyIndex, VkQueue queue, bool startRecording = true);

        ~OnceCommandBuffer() override;

        void start();

        void end();

    private:
        bool isRecording{false};
        VkQueue queue{};
        CommandPool pool;
        CommandBuffer buffer;
    };

}


#endif //LITTLEVULKANENGINE_RENDER_COMMAND_HPP
