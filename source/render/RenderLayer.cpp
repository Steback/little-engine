#include "RenderLayer.hpp"


namespace lve {

    RenderLayer::~RenderLayer() {
        pipeline.reset();
        renderPass.reset();

        layouts.clear();
        pool.reset();
    }

    void RenderLayer::setup(std::shared_ptr<Device> inDevice, std::shared_ptr<RenderPass> inRenderPass, const std::shared_ptr<Swapchain> &swapchain) {
        device = std::move(inDevice);

        resize(std::move(inRenderPass), swapchain);
        setupDescriptors();
        createPipeline();
    }

    void RenderLayer::resize(std::shared_ptr<RenderPass> inRenderPass, const std::shared_ptr<Swapchain> &swapchain) {
        renderPass = std::move(inRenderPass);
        if (!renderPass) {
            assert(swapchain);
            createRenderPass(swapchain);
        }
    }

}