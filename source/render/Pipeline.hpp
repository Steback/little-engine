#ifndef LITTLEVULKANENGINE_PIPELINE_HPP
#define LITTLEVULKANENGINE_PIPELINE_HPP


#include <memory>

#include <volk.h>

#include "Shader.hpp"
#include "Device.hpp"
#include "RenderPass.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class Pipeline : public Wrapper<VkPipeline> {
    public:
        Pipeline(VkPipeline handle, std::shared_ptr<Device> device, VkPipelineLayout layout, VkPipelineBindPoint bindPoint, VkPipelineCache cache = VK_NULL_HANDLE);

        ~Pipeline() override;

        void bind(const VkCommandBuffer& cmdBuffer);

        [[nodiscard]] inline const VkPipelineLayout& getLayout() const { return layout; }

        [[nodiscard]] inline VkPipelineBindPoint getBindPoint() const { return bindPoint; }

    private:
        VkPipelineLayout layout{VK_NULL_HANDLE};
        VkPipelineCache cache{VK_NULL_HANDLE};
        VkPipelineBindPoint bindPoint{};
        std::shared_ptr<Device> device{VK_NULL_HANDLE};
    };

    struct PipelineBuilder {
        PipelineBuilder(std::shared_ptr<Device> device, std::shared_ptr<RenderPass> renderPass);

        PipelineBuilder& setBindPoint(VkPipelineBindPoint bindPoint);

        PipelineBuilder& setSubpass(uint32_t subpass);

        PipelineBuilder& setViewportInfo(VkPipelineViewportStateCreateInfo viewportInfo);

        PipelineBuilder& setInputAssemblyInfo(VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo);

        PipelineBuilder& setRasterizationInfo(VkPipelineRasterizationStateCreateInfo rasterizationInfo);

        PipelineBuilder& setMultisampleInfo(VkPipelineMultisampleStateCreateInfo multisampleInfo);

        PipelineBuilder& setPipelineColorBlend(VkPipelineColorBlendAttachmentState colorBlendAttachment);

        PipelineBuilder& setColorBlendInfo(VkPipelineColorBlendStateCreateInfo colorBlendInfo);

        PipelineBuilder& setDepthStencilInfo(VkPipelineDepthStencilStateCreateInfo depthStencilInfo);

        PipelineBuilder& setDynamicStateInfo(VkPipelineDynamicStateCreateInfo dynamicStateInfo);

        PipelineBuilder& addDynamicState(VkDynamicState dynamicState);

        PipelineBuilder& addSetLayout(VkDescriptorSetLayout setLayout);

        PipelineBuilder& addPushConstantRange(VkPushConstantRange pushConstantRange);

        PipelineBuilder& addShaderStage(VkPipelineShaderStageCreateInfo shaderStageInfo);

        PipelineBuilder& addBindingDescription(VkVertexInputBindingDescription bindingDescription);

        PipelineBuilder& addAttributeDescription(VkVertexInputAttributeDescription attributeDescription);

        PipelineBuilder& useCache(bool state);

        std::unique_ptr<Pipeline> build();

    private:
        bool initCache{false};
        uint32_t subpass{0};
        std::shared_ptr<Device> device;
        std::shared_ptr<RenderPass> renderPass;
        VkPipelineBindPoint bindPoint;
        VkPipelineViewportStateCreateInfo viewportInfo{};
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
        VkPipelineRasterizationStateCreateInfo rasterizationInfo{};
        VkPipelineMultisampleStateCreateInfo multisampleInfo{};
        VkPipelineColorBlendAttachmentState colorBlendAttachment{};
        VkPipelineColorBlendStateCreateInfo colorBlendInfo{};
        VkPipelineDepthStencilStateCreateInfo depthStencilInfo{};
        VkPipelineDynamicStateCreateInfo dynamicStateInfo{};
        std::vector<VkDynamicState> dynamicStateEnables;
        std::vector<VkDescriptorSetLayout> setLayouts;
        std::vector<VkPushConstantRange> pushConstantRanges;
        std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
        std::vector<VkVertexInputBindingDescription> bindingsDescription;
        std::vector<VkVertexInputAttributeDescription> attributesDescription;
    };

}


#endif //LITTLEVULKANENGINE_PIPELINE_HPP
