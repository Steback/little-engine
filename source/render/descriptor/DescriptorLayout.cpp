#include "DescriptorLayout.hpp"


namespace lve {

    DescriptorLayout::DescriptorLayout(VkDescriptorSetLayout handle, const std::shared_ptr<Device>& device, std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings)
            : Wrapper<VkDescriptorSetLayout>(handle), device(*device), bindings(std::move(bindings)) {

    }

    DescriptorLayout::~DescriptorLayout() {
        if (handle) {
            vkDestroyDescriptorSetLayout(device, handle, nullptr);
        }
    }

    VkDescriptorSetLayoutBinding DescriptorLayout::getBinding(uint32_t index) {
        if (bindings.contains(index)) {
            return bindings[index];
        } else {
            LVE_LOG_ERROR("[Vulkan] Descriptor layout binding '{}' not present", index);
            return {};
        }
    }

    DescriptorLayoutBuilder::DescriptorLayoutBuilder(std::shared_ptr<Device> device) : device(std::move(device)) {

    }

    DescriptorLayoutBuilder &DescriptorLayoutBuilder::addBinding(uint32_t binding, VkDescriptorType descriptorType, VkShaderStageFlags stageFlags, uint32_t count) {
        if (bindings.contains(binding)) {
            LVE_LOG_ERROR("[Vulkan] Descriptor layout binding '{}' already in use. Will be overwrite");
        }

        VkDescriptorSetLayoutBinding layoutBinding{};
        layoutBinding.binding = binding;
        layoutBinding.descriptorType = descriptorType;
        layoutBinding.descriptorCount = count;
        layoutBinding.stageFlags = stageFlags;
        bindings[binding] = layoutBinding;

        return *this;
    }

    std::unique_ptr<DescriptorLayout> DescriptorLayoutBuilder::build() {
        std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
        for (const auto& [index, binding] : bindings) {
            layoutBindings.push_back(binding);
        }

        VkDescriptorSetLayoutCreateInfo createInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
        createInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());
        createInfo.pBindings = layoutBindings.data();

        VkDescriptorSetLayout handle;
        LVE_VK_CHECK(vkCreateDescriptorSetLayout(*device, &createInfo, nullptr, &handle))

        return std::make_unique<DescriptorLayout>(handle, device, bindings);
    }


}