#ifndef LITTLEVULKANENGINE_DESCRIPTORWRITER_HPP
#define LITTLEVULKANENGINE_DESCRIPTORWRITER_HPP


#include "DescriptorPool.hpp"
#include "DescriptorLayout.hpp"


namespace lve {

    class DescriptorWriter {
    public:
        DescriptorWriter(std::shared_ptr<DescriptorLayout> layout, std::shared_ptr<DescriptorPool> pool);

        DescriptorWriter &writeBuffer(uint32_t binding, VkDescriptorBufferInfo *bufferInfo);

        DescriptorWriter &writeImage(uint32_t binding, VkDescriptorImageInfo *imageInfo);

        bool build(VkDescriptorSet &set);

        void overwrite(VkDescriptorSet &set);

    private:
        std::shared_ptr<DescriptorPool> pool;
        std::shared_ptr<DescriptorLayout> layout;
        std::vector<VkWriteDescriptorSet> writes;
    };

}


#endif //LITTLEVULKANENGINE_DESCRIPTORWRITER_HPP
