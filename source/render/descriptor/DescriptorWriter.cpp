#include "DescriptorWriter.hpp"


namespace lve {

    DescriptorWriter::DescriptorWriter(std::shared_ptr<DescriptorLayout> layout, std::shared_ptr<DescriptorPool> pool)
            : layout(std::move(layout)), pool(std::move(pool)) {

    }

    DescriptorWriter &DescriptorWriter::writeBuffer(uint32_t binding, VkDescriptorBufferInfo *bufferInfo) {
        auto bindingDescription = layout->getBinding(binding);
        VkWriteDescriptorSet write{VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET};
        write.descriptorType = bindingDescription.descriptorType;
        write.dstBinding = binding;
        write.pBufferInfo = bufferInfo;
        write.descriptorCount = 1;

        writes.push_back(write);
        return *this;
    }

    DescriptorWriter &DescriptorWriter::writeImage(uint32_t binding, VkDescriptorImageInfo *imageInfo) {
        auto bindingDescription = layout->getBinding(binding);
        VkWriteDescriptorSet write{VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET};
        write.descriptorType = bindingDescription.descriptorType;
        write.dstBinding = binding;
        write.pImageInfo = imageInfo;
        write.descriptorCount = 1;

        writes.push_back(write);
        return *this;
    }

    bool DescriptorWriter::build(VkDescriptorSet &set) {
        pool->allocateDescriptor({*layout}, set);
        overwrite(set);

        return true;
    }

    void DescriptorWriter::overwrite(VkDescriptorSet &set) {
        for (auto& write : writes) {
            write.dstSet = set;
        }

        vkUpdateDescriptorSets(pool->device, writes.size(), writes.data(), 0, nullptr);
    }

}
