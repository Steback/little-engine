#ifndef LITTLEVULKANENGINE_DESCRIPTORPOOL_HPP
#define LITTLEVULKANENGINE_DESCRIPTORPOOL_HPP


#include <vector>

#include "render/Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class DescriptorPool : public Wrapper<VkDescriptorPool> {
        friend class DescriptorWriter;

    public:
        DescriptorPool(VkDescriptorPool handle, const std::shared_ptr<Device>& device);

        ~DescriptorPool() override;

        void allocateDescriptor(const std::vector<VkDescriptorSetLayout>& layouts, VkDescriptorSet &descriptor) const;

        void freeDescriptors(std::vector<VkDescriptorSet> &descriptors) const;

        void resetPool();

    private:
        VkDevice device;
    };

    struct DescriptorPoolBuilder {
        explicit DescriptorPoolBuilder(std::shared_ptr<Device> device);

        DescriptorPoolBuilder &addPoolSize(VkDescriptorType descriptorType, uint32_t count);

        DescriptorPoolBuilder &setPoolFlags(VkDescriptorPoolCreateFlags flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT);

        DescriptorPoolBuilder &setMaxSets(uint32_t count);

        std::unique_ptr<DescriptorPool> build();

        VkDescriptorPoolCreateFlags flags = 0;
        uint32_t maxSets = 1000;
        std::shared_ptr<Device> device;
        std::vector<VkDescriptorPoolSize> sizes{};
    };

} // lve


#endif //LITTLEVULKANENGINE_DESCRIPTORPOOL_HPP
