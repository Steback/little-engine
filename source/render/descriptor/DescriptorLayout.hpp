#ifndef LITTLEVULKANENGINE_DESCRIPTORLAYOUT_HPP
#define LITTLEVULKANENGINE_DESCRIPTORLAYOUT_HPP


#include <unordered_map>

#include "render/Device.hpp"
#include "utils/Wrapper.hpp"


namespace lve {

    class DescriptorLayout : public Wrapper<VkDescriptorSetLayout> {
        friend class DescriptorWriter;

    public:
        DescriptorLayout(VkDescriptorSetLayout handle, const std::shared_ptr<Device>& device, std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings);

        ~DescriptorLayout() override;

        VkDescriptorSetLayoutBinding getBinding(uint32_t index = 0);

    private:
        VkDevice device;
        std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings;
    };

    class DescriptorLayoutBuilder {
    public:
        explicit DescriptorLayoutBuilder(std::shared_ptr<Device> device);

        DescriptorLayoutBuilder &addBinding(uint32_t binding, VkDescriptorType descriptorType, VkShaderStageFlags stageFlags, uint32_t count = 1);

        std::unique_ptr<DescriptorLayout> build();

    private:
        std::shared_ptr<Device> device;
        std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings{};
    };

}


#endif //LITTLEVULKANENGINE_DESCRIPTORLAYOUT_HPP
