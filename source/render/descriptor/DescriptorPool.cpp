#include "DescriptorPool.hpp"


namespace lve {

    DescriptorPool::DescriptorPool(VkDescriptorPool handle, const std::shared_ptr<Device>& device)
            : Wrapper<VkDescriptorPool>(handle), device(*device) {

    }

    DescriptorPool::~DescriptorPool() {
        if (handle) {
            vkDestroyDescriptorPool(device, handle, nullptr);
        }
    }

    void  DescriptorPool::allocateDescriptor(const std::vector<VkDescriptorSetLayout>& layouts, VkDescriptorSet &descriptor) const {
        VkDescriptorSetAllocateInfo allocateInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO};
        allocateInfo.descriptorPool = handle;
        allocateInfo.pSetLayouts = layouts.data();
        allocateInfo.descriptorSetCount = static_cast<uint32_t>(layouts.size());

        LVE_VK_CHECK(vkAllocateDescriptorSets(device, &allocateInfo, &descriptor))
    }

    void DescriptorPool::freeDescriptors(std::vector<VkDescriptorSet> &descriptors) const {
        vkFreeDescriptorSets(device, handle, static_cast<uint32_t>(descriptors.size()), descriptors.data());
    }

    void DescriptorPool::resetPool() {
        vkResetDescriptorPool(device, handle, 0);
    }

    DescriptorPoolBuilder::DescriptorPoolBuilder(std::shared_ptr<Device> device) : device(std::move(device)) {

    }

    DescriptorPoolBuilder &DescriptorPoolBuilder::addPoolSize(VkDescriptorType descriptorType, uint32_t count) {
        sizes.push_back({descriptorType, count});
        return *this;
    }

    DescriptorPoolBuilder &DescriptorPoolBuilder::setPoolFlags(VkDescriptorPoolCreateFlags inFlags) {
        flags = inFlags;
        return *this;
    }

    DescriptorPoolBuilder &DescriptorPoolBuilder::setMaxSets(uint32_t count) {
        maxSets = count;
        return *this;
    }

    std::unique_ptr<DescriptorPool> DescriptorPoolBuilder::build() {
        VkDescriptorPoolCreateInfo descriptorPoolInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
        descriptorPoolInfo.poolSizeCount = static_cast<uint32_t>(sizes.size());
        descriptorPoolInfo.pPoolSizes = sizes.data();
        descriptorPoolInfo.maxSets = maxSets;
        descriptorPoolInfo.flags = flags;

        VkDescriptorPool handle;
        LVE_VK_CHECK(vkCreateDescriptorPool(*device, &descriptorPoolInfo, nullptr, &handle))

        return std::make_unique<DescriptorPool>(handle, device);
    }


} // lve