#include "Device.hpp"

#include <vector>

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"


namespace lve {

    Device::Device(VkDevice handle, std::shared_ptr<PhysicalDevice> physicalDevice, const std::unordered_map<VkQueueFlagBits, uint32_t>& queues, std::shared_ptr<Surface> surface)
            : Wrapper<VkDevice>(handle), physicalDevice(std::move(physicalDevice)), surface(std::move(surface)) {
        VmaVulkanFunctions functions{};
        functions.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
        functions.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
        functions.vkAllocateMemory = vkAllocateMemory;
        functions.vkFreeMemory = vkFreeMemory;
        functions.vkMapMemory = vkMapMemory;
        functions.vkUnmapMemory = vkUnmapMemory;
        functions.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
        functions.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
        functions.vkBindBufferMemory = vkBindBufferMemory;
        functions.vkBindImageMemory = vkBindImageMemory;
        functions.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
        functions.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
        functions.vkCreateBuffer = vkCreateBuffer;
        functions.vkDestroyBuffer = vkDestroyBuffer;
        functions.vkCreateImage = vkCreateImage;
        functions.vkDestroyImage = vkDestroyImage;
        functions.vkCmdCopyBuffer = vkCmdCopyBuffer;
        functions.vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2;
        functions.vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2;
        functions.vkBindBufferMemory2KHR = vkBindBufferMemory2KHR;
        functions.vkBindImageMemory2KHR = vkBindImageMemory2KHR;
        functions.vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2KHR;
        functions.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
        functions.vkGetDeviceProcAddr = vkGetDeviceProcAddr;

        VmaAllocatorCreateInfo allocatorInfo{};
        allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_3;
        allocatorInfo.device = handle;
        allocatorInfo.physicalDevice = *this->physicalDevice;
        allocatorInfo.instance = *this->physicalDevice->getInstance();
        allocatorInfo.pVulkanFunctions = &functions;

        LVE_VK_CHECK(vmaCreateAllocator(&allocatorInfo, &allocator))

        for (const auto& [type, index] : queues) {
            this->queues[type] = {};
            this->queues[type].index = index;
            vkGetDeviceQueue(handle, index, 0, &this->queues[type].get());
        }
    }

    Device::~Device() {
        if (allocator) vmaDestroyAllocator(allocator);

        if (handle) vkDestroyDevice(handle, nullptr);
    }

    Queue Device::getQueue(VkQueueFlagBits type) {
        if (queues.contains(type)) {
            return queues[type];
        } else {
            return {VK_NULL_HANDLE, (uint32_t)-1,  type};
        }
    }

    void Device::waitIdle() {
        vkDeviceWaitIdle(handle);
    }

    DeviceBuilder::DeviceBuilder(std::shared_ptr<PhysicalDevice> physicalDevice) : physicalDevice(std::move(physicalDevice)) {

    }

    DeviceBuilder &DeviceBuilder::addQueueType(VkQueueFlagBits type) {
        queuesTypes.push_back(type);
        return *this;
    }

    DeviceBuilder &DeviceBuilder::setSurface(std::shared_ptr<Surface> inSurface) {
        surface = std::move(inSurface);
        return *this;
    }

    DeviceBuilder &DeviceBuilder::addExtension(const char *name) {
        extensions.push_back(name);
        return *this;
    }

    std::unique_ptr<Device> DeviceBuilder::build() {
        static const float queuePriority = 1.0f;

        std::unordered_map<VkQueueFlagBits, uint32_t> queues;
        std::vector<VkDeviceQueueCreateInfo> queuesCreateInfos;

        for (const auto& type : queuesTypes) {
            if (const auto index = physicalDevice->getQueueIndex(type, *surface)) {
                if (!queues.contains(type)) {
                    queues[type] = index.value();
                    VkDeviceQueueCreateInfo queueCreateInfo{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO};

                    queueCreateInfo.queueFamilyIndex = index.value();
                    queueCreateInfo.pQueuePriorities = &queuePriority;
                    queueCreateInfo.queueCount = 1;
                    queuesCreateInfos.push_back(queueCreateInfo);
                }
            } else {
                LVE_THROW_EX("[Vulkan] Failed to find valid queue family index: {}", toString(type));
            }
        }

        LVE_LOG_INFO("[Vulkan] Device extensions required: ");
        for (const auto& extension : extensions) {
            LVE_LOG_INFO("\t - {}", extension);
        }

        LVE_LOG_INFO("[Vulkan] Device queues required: ");
        for (const auto& [type, index] : queues) {
            LVE_LOG_INFO("\t - {}: {}", toString(type), index);
        }

        auto features = physicalDevice->getFeatures();
        VkDeviceCreateInfo createInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
        createInfo.pEnabledFeatures = &features;
        createInfo.queueCreateInfoCount = (uint32_t)queuesCreateInfos.size();
        createInfo.pQueueCreateInfos = queuesCreateInfos.empty() ? nullptr : queuesCreateInfos.data();
        createInfo.enabledExtensionCount = extensions.size();
        createInfo.ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data();

        VkDevice handle;
        LVE_VK_CHECK(vkCreateDevice(*physicalDevice, &createInfo, nullptr, &handle));

        return std::make_unique<Device>(handle, physicalDevice, queues, surface);
    }

} // namespace lv