#include "Semaphore.hpp"


namespace lve {

    Semaphore::Semaphore(const std::shared_ptr<Device>& device) : device(*device) {
        VkSemaphoreCreateInfo createInfo{VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
        vkCreateSemaphore(this->device, &createInfo, nullptr, &handle);
    }

    Semaphore::~Semaphore() {
        if (handle) {
            vkDestroySemaphore(device, handle, nullptr);
        }
    }

}
