#include "RenderPass.hpp"


namespace lve {

    RenderPass::RenderPass(VkRenderPass handle, std::shared_ptr<Device> device, const std::shared_ptr<Swapchain>& swapchain)
            : Wrapper<VkRenderPass>(handle), device(std::move(device)) {
        framebuffers.resize(swapchain->getImagesCount());
        for (int i = 0; i < swapchain->getImagesCount(); ++i) {
            std::vector<VkImageView> attachments = { swapchain->getImageView(i), swapchain->getDepthView(i) };
            framebuffers[i] = std::make_unique<Framebuffer>(this->device, handle, swapchain->getExtent(), attachments);
        }
    }

    RenderPass::~RenderPass() {
        if (handle) {
            vkDestroyRenderPass(*device, handle, nullptr);
        }
    }

    void RenderPass::begin(const VkCommandBuffer &cmdBuffer, const std::vector<VkClearValue> &clearValues, uint32_t index) {
        VkRenderPassBeginInfo createInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
        createInfo.renderPass = handle;
        createInfo.framebuffer = *framebuffers[index];
        createInfo.renderArea.offset = {0, 0};
        createInfo.renderArea.extent = framebuffers[index]->getExtent();
        createInfo.clearValueCount = (uint32_t)clearValues.size();
        createInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(cmdBuffer, &createInfo, VK_SUBPASS_CONTENTS_INLINE);
    }

    void RenderPass::end(const VkCommandBuffer &cmdBuffer) {
        vkCmdEndRenderPass(cmdBuffer);
    }

    RenderPassBuilder::RenderPassBuilder(std::shared_ptr<Device> device, std::shared_ptr<Swapchain> swapchain)
            : device(std::move(device)), swapchain(std::move(swapchain)) {

    }

    RenderPassBuilder &RenderPassBuilder::addAttachment(VkAttachmentDescription attachmentDescription) {
        attachmentsDescriptions.push_back(attachmentDescription);
        return *this;
    }

    RenderPassBuilder &RenderPassBuilder::addColorAttachmentReference(VkAttachmentReference reference) {
        colorAttachmentsReferences.push_back(reference);
        return *this;
    }

    RenderPassBuilder &RenderPassBuilder::setDepthAttachmentReference(VkAttachmentReference reference) {
        depthAttachmentReference = reference;
        return *this;
    }

    RenderPassBuilder &RenderPassBuilder::addSubpassDependency(VkSubpassDependency subpassDependency) {
        dependencies.push_back(subpassDependency);
        return *this;
    }

    std::unique_ptr<RenderPass> RenderPassBuilder::build() {
        VkSubpassDescription subpass{};
        subpass.pipelineBindPoint = bindPoint;
        subpass.colorAttachmentCount = (uint32_t)colorAttachmentsReferences.size();
        subpass.pColorAttachments = colorAttachmentsReferences.data();
        subpass.pDepthStencilAttachment = &depthAttachmentReference;

        VkRenderPassCreateInfo createInfo{VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
        createInfo.attachmentCount = (uint32_t)attachmentsDescriptions.size();
        createInfo.pAttachments = attachmentsDescriptions.data();
        createInfo.subpassCount = 1;
        createInfo.pSubpasses = &subpass;
        createInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());
        createInfo.pDependencies = dependencies.data();

        VkRenderPass handle;
        LVE_VK_CHECK(vkCreateRenderPass(*device, &createInfo, nullptr, &handle));

        return std::make_unique<RenderPass>(handle, device, swapchain);
    }

}
