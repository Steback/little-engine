#ifndef LITTLEVULKANENGINE_SURFACE_HPP
#define LITTLEVULKANENGINE_SURFACE_HPP


#include "Window.hpp"
#include "Instance.hpp"


namespace lve {

    class Surface : public Wrapper<VkSurfaceKHR> {
    public:
        Surface(const std::shared_ptr<Instance>& instance, const std::shared_ptr<Window>& window);

        ~Surface() override;

    private:
        VkInstance instance;
    };

}


#endif //LITTLEVULKANENGINE_SURFACE_HPP
