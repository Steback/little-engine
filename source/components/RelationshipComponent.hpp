#ifndef LITTLEVULKANENGINE_RELATIONSHIPCOMPONENT_HPP
#define LITTLEVULKANENGINE_RELATIONSHIPCOMPONENT_HPP


#include <vector>

#include "Component.hpp"
#include "utils/Serialize.hpp"


namespace lve {

    struct RelationshipComponent : public Component {
        LVE_DEFINE_COMPONENT(RelationshipComponent)

        void addChild(Entity id);

        void removeChild(Entity id);

        void setParent(Entity parent = entt::null);

        bool hasChild(Entity id);

        LVE_SERIALIZE_TYPE_INTRUSIVE(RelationshipComponent, parent, children, owner)

        Entity parent{};
        std::vector<Entity> children;
    };

}


#endif //LITTLEVULKANENGINE_RELATIONSHIPCOMPONENT_HPP
