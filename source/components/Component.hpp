#ifndef LITTLEVULKANENGINE_COMPONENT_HPP
#define LITTLEVULKANENGINE_COMPONENT_HPP


#include "scene/Scene.hpp"


#define LVE_DEFINE_COMPONENT(Comp)                                                                                      \
    public:                                                                                                             \
        static inline std::string getName() { return #Comp; }                                                           \
        static inline entt::type_info info() { return entt::type_id<Comp>(); }                                          \
        Comp(entt::entity id, Scene* scene) : Component(id, scene) {  }                                                 \


namespace lve {

    class Component {
    public:
        Component(Entity owner, Scene* scene) : owner(owner), scene(scene) { }

        [[nodiscard]] inline Entity getEntity() const { return owner; }

        template<typename T>
        T* addComponent(const Entity& entity) {
            return scene->addComponent<T>(entity);
        }

        template<typename T>
        T* getComponent(const Entity& entity) {
            return scene->getComponent<T>(entity);
        }

        template<typename T>
        bool hasComponent(const Entity& entity) {
            return scene->hasComponent<T>(entity);
        }

        bool enable{true};

    protected:
        Entity owner;
        Scene* scene;
    };

} // lve


#endif //LITTLEVULKANENGINE_COMPONENT_HPP
