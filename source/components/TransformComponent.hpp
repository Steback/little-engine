#ifndef LITTLEVULKANENGINE_TRANSFORMCOMPONENT_HPP
#define LITTLEVULKANENGINE_TRANSFORMCOMPONENT_HPP


#include <glm/glm.hpp>

#include "Component.hpp"
#include "ui/ImSerialize.hpp"
#include "utils/Serialize.hpp"


namespace lve {

    struct TransformComponent : public Component {
        LVE_DEFINE_COMPONENT(TransformComponent)

        [[nodiscard]] glm::mat4 worldTransform() const;

        [[nodiscard]] glm::mat3 normalMatrix() const;

        LVE_SERIALIZE_TYPE_INTRUSIVE(TransformComponent, translation, rotation, scale, owner)

        glm::vec3 translation{};
        glm::vec3 rotation{};
        glm::vec3 scale{};
    };

    inline void imSerialize(TransformComponent& transformComponent) {
        imSerialize("Position", transformComponent.translation);
        imSerialize("Rotation", transformComponent.rotation);
        imSerialize("Scale", transformComponent.scale);
    }

} // namespace lve


#endif //LITTLEVULKANENGINE_TRANSFORMCOMPONENT_HPP
