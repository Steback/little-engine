#ifndef LITTLEVULKANENGINE_DATACOMPONENT_HPP
#define LITTLEVULKANENGINE_DATACOMPONENT_HPP


#include <string>

#include "Component.hpp"
#include "ui/ImSerialize.hpp"
#include "utils/Serialize.hpp"


namespace lve {

    struct DataComponent : public Component {
        LVE_DEFINE_COMPONENT(DataComponent)

        LVE_SERIALIZE_TYPE_INTRUSIVE(DataComponent, name, flags, owner)

        uint32_t flags{};
        std::string name{};
    };

    inline void imSerialize(DataComponent& component) {
        imSerialize("Name", component.name);
    }

}


#endif //LITTLEVULKANENGINE_DATACOMPONENT_HPP
