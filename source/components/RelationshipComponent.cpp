#include "RelationshipComponent.hpp"


namespace lve {


    void RelationshipComponent::addChild(Entity id) {
        if (id != entt::null) {
            auto it = std::find(children.begin(), children.end(), id);
            if (it == children.end()) {
                children.push_back(id);
            }
        }
    }

    void RelationshipComponent::removeChild(Entity id) {
        if (id != entt::null) {
            auto it = std::find(children.begin(), children.end(), id);
            if (it != children.end()) {
                children.erase(it);
            }
        }
    }

    void RelationshipComponent::setParent(Entity inParent) {
        if (parent != entt::null) {
            if (auto* parentRelationshipComp = scene->getComponent<RelationshipComponent>(parent)) {
                parentRelationshipComp->removeChild(owner);
            }
        }

        parent = inParent;
        if (auto* parentRelationshipComp = scene->getComponent<RelationshipComponent>(parent)) {
            parentRelationshipComp->addChild(owner);
        }
    }

    bool RelationshipComponent::hasChild(Entity id) {
        auto it = std::find(children.begin(), children.end(), id);
        return it != children.end();
    }

}
