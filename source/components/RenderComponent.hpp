#ifndef LITTLEVULKANENGINE_RENDERCOMPONENT_HPP
#define LITTLEVULKANENGINE_RENDERCOMPONENT_HPP


#include <memory>
#include <string>

#include "Component.hpp"
#include "assets/Mesh.hpp"
#include "ui/ImSerialize.hpp"
#include "assets/Material.hpp"
#include "utils/Serialize.hpp"


namespace lve {

    struct RenderComponent : public Component {
        LVE_DEFINE_COMPONENT(RenderComponent)

        LVE_SERIALIZE_TYPE_INTRUSIVE(RenderComponent, meshName, materialName, owner)

        std::string meshName;
        std::string materialName;
        std::shared_ptr<Mesh> mesh{};
        std::shared_ptr<Material> material{};
    };

    inline void imSerialize(RenderComponent& renderComponent) {
        imSerialize("Mesh", renderComponent.meshName);
        imSerialize("Material", renderComponent.materialName);
    }

} // namespace lve


#endif //LITTLEVULKANENGINE_RENDERCOMPONENT_HPP
