#include "CommandsManager.hpp"


namespace lve {

    void CommandsManager::undo() {
        if (commands.empty()) {
            return;
        }

        auto command = std::move(commands.back());
        command->undo();
        commands.pop_back();

        if (undoCommands.size() == maxCommandsCount) {
            undoCommands.erase(undoCommands.begin());
        }

        undoCommands.push_back(std::move(command));
    }

    void CommandsManager::redo() {
        if (undoCommands.empty()) {
            return;
        }

        auto command = std::move(undoCommands.back());
        command->redo();
        undoCommands.pop_back();

        if (commands.size() == maxCommandsCount) {
            commands.erase(commands.begin());
        }

        commands.push_back(std::move(command));
    }

}
