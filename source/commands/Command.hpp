#ifndef LITTLEVULKANENGINE_COMMAND_HPP
#define LITTLEVULKANENGINE_COMMAND_HPP


#include <string>


namespace lve {

    /**
     * @brief Command Interface
     */
    class Command {
    public:
        explicit Command(std::string name) : name(std::move(name)) {  }

        virtual ~Command() = default;

        virtual void exec() = 0;

        virtual void undo() = 0;

        virtual void redo() = 0;

        [[nodiscard]] inline std::string getName() const { return name; }

    protected:
        std::string name;
    };

} // lve


#endif //LITTLEVULKANENGINE_COMMAND_HPP
