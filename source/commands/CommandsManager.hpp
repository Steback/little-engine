#ifndef LITTLEVULKANENGINE_COMMANDSMANAGER_HPP
#define LITTLEVULKANENGINE_COMMANDSMANAGER_HPP


#include <memory>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include "Command.hpp"
#include "utils/Singleton.hpp"


namespace lve {

    class CommandsManager : public Singleton<CommandsManager> {
        friend Singleton<CommandsManager>;

        CommandsManager() = default;

    public:
        template<typename T, typename ...Args>
        void exe(Args&&...args);

        template<typename ...Args>
        void exe(const std::string& name, Args&&...args);

        void undo();

        void redo();

    private:
        uint32_t maxCommandsCount{10};
        std::vector<std::unique_ptr<Command>> commands;
        std::vector<std::unique_ptr<Command>> undoCommands;
    };

    template<typename T, typename... Args>
    void CommandsManager::exe(Args &&... args) {
        if (auto command = std::make_unique<T>(std::forward<Args>(args)...)) {
            if (commands.size() == maxCommandsCount) {
                commands.erase(commands.begin());
            }

            command->exec();
            commands.push_back(std::move(command));
        }
    }

    template<typename... Args>
    void CommandsManager::exe(const std::string &name, Args &&... args) {
        // TODO: Implement Commands
    }

}


#endif //LITTLEVULKANENGINE_COMMANDSMANAGER_HPP
