#include "Logs.hpp"

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"


namespace lve {

    LogsManager::LogsManager(std::filesystem::path root) : path(root / LVE_LOGS_DIR) {
        if (!exists(path)) {
            std::filesystem::create_directory(path);
        }

        for (const auto& it : std::filesystem::directory_iterator(path)) {
            const auto& entry = it.path();
            std::filesystem::remove(entry);
        }
    }

    void LogsManager::addLogger(const std::string &name, const std::string &file) {
        const auto loggerPath = path / (file.empty() ? name + ".log" : file);

        std::vector<spdlog::sink_ptr> sinks;
        sinks.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
        sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(loggerPath.string()));

        auto logger = std::make_shared<spdlog::logger>(name, sinks.begin(), sinks.end());
        logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%^%l%$] %v");

        loggers[name] = logger;
    }

    std::shared_ptr<LogsManager::Logger> LogsManager::getLogger(const std::string &name) {
        if (loggers.contains(name)) {
            return loggers[name];
        } else {
            return nullptr;
        }
    }

}