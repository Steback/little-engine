#ifndef LITTLEVULKANENGINE_LOGS_HPP
#define LITTLEVULKANENGINE_LOGS_HPP


#include <filesystem>

#include <spdlog/spdlog.h>

#include "utils/Utils.hpp"
#include "utils/Singleton.hpp"


#define LVE_LOGS_DIR "logs"
#define LVE_LOGS "runtime"
#define LVE_LOGS_FILE "runtime.log"

/**
* @brief Default log macro
* @param name: Key/Data to Logger. Must be added before to LogsManager
* @param level: Level of log(info, error, warm, trace, debug, critical)
* @param args: Could be string, or string with fmt surfaceFormat and its parameters
*/
#define LVE_LOG(name, level, ...) lve::LogsManager::get()->getLogger(name)->level(__VA_ARGS__)

#define LVE_LOG_INFO(...) LVE_LOG(LVE_LOGS, info, __VA_ARGS__)
#define LVE_LOG_WARN(...) LVE_LOG(LVE_LOGS, warn, __VA_ARGS__)
#define LVE_LOG_ERROR(...) LVE_LOG(LVE_LOGS, error, __VA_ARGS__)
#define LVE_LOG_CRITICAL(...) LVE_LOG(LVE_LOGS, critical, __VA_ARGS__)


namespace lve {

    class LogsManager : public Singleton<LogsManager> {
        friend Singleton<LogsManager>;

        explicit LogsManager(std::filesystem::path root);

    public:
        using Logger = spdlog::logger;

        void addLogger(const std::string& name, const std::string& file = "");

        std::shared_ptr<Logger> getLogger(const std::string& name);

        [[nodiscard]] inline std::filesystem::path getPath() const { return path; }

    private:
        std::filesystem::path path;
        std::unordered_map<std::string, std::shared_ptr<Logger>> loggers;
    };

}


#endif //LITTLEVULKANENGINE_LOGS_HPP
