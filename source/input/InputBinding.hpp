#ifndef LITTLEVULKANENGINE_INPUTBINDING_HPP
#define LITTLEVULKANENGINE_INPUTBINDING_HPP


#include <string>
#include <functional>

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include "utils/Utils.hpp"
#include "ui/ImSerialize.hpp"
#include "utils/Serialize.hpp"


namespace lve {

    enum InputMode {
        Game = 0,
        UI = 1,
        Editor = 2,
        Max
    };

    enum InputType {
        InputTypeNone = 0,
        InputTypeKeyboard = 1,
        InputTypeMouse = 2,
        InputTypeGamepad = 3
    };

    enum State {
        StateStateNone = -1,
        StateRelease = GLFW_RELEASE,
        StatePress = GLFW_PRESS,
        StateRepeat = GLFW_REPEAT
    };

    enum Key {
        KeyUnknown = GLFW_KEY_UNKNOWN,
        KeySpace = GLFW_KEY_SPACE,
        KeyApostrophe = GLFW_KEY_APOSTROPHE,
        KeyComma = GLFW_KEY_COMMA,
        KeyMinus = GLFW_KEY_MINUS,
        KeyPeriod = GLFW_KEY_PERIOD,
        KeySlash = GLFW_KEY_SLASH,
        KeyNum0 = GLFW_KEY_0,
        KeyNum1 = GLFW_KEY_1,
        KeyNum2 = GLFW_KEY_2,
        KeyNum3 = GLFW_KEY_3,
        KeyNum4 = GLFW_KEY_4,
        KeyNum5 = GLFW_KEY_5,
        KeyNum6 = GLFW_KEY_6,
        KeyNum7 = GLFW_KEY_7,
        KeyNum8 = GLFW_KEY_8,
        KeyNum9 = GLFW_KEY_9,
        KeySemicolon = GLFW_KEY_SEMICOLON,
        KeyEqual = GLFW_KEY_EQUAL,
        KeyA = GLFW_KEY_A,
        KeyB = GLFW_KEY_B,
        KeyC = GLFW_KEY_C,
        KeyD = GLFW_KEY_D,
        KeyE = GLFW_KEY_E,
        KeyF = GLFW_KEY_F,
        KeyG = GLFW_KEY_G,
        KeyH = GLFW_KEY_H,
        KeyI = GLFW_KEY_I,
        KeyJ = GLFW_KEY_J,
        KeyK = GLFW_KEY_K,
        KeyL = GLFW_KEY_L,
        KeyM = GLFW_KEY_M,
        KeyN = GLFW_KEY_N,
        KeyO = GLFW_KEY_O,
        KeyP = GLFW_KEY_P,
        KeyQ = GLFW_KEY_Q,
        KeyR = GLFW_KEY_R,
        KeyS = GLFW_KEY_S,
        KeyT = GLFW_KEY_T,
        KeyU = GLFW_KEY_U,
        KeyV = GLFW_KEY_V,
        KeyW = GLFW_KEY_W,
        KeyX = GLFW_KEY_X,
        KeyY = GLFW_KEY_Y,
        KeyZ = GLFW_KEY_Z,
        KeyLeftBracket = GLFW_KEY_LEFT_BRACKET,
        KeyBackslash = GLFW_KEY_BACKSLASH,
        KeyRightBracket = GLFW_KEY_RIGHT_BRACKET,
        KeyGraveAccent = GLFW_KEY_GRAVE_ACCENT,
        KeyEscape = GLFW_KEY_ESCAPE,
        KeyEnter = GLFW_KEY_ENTER,
        KeyTab = GLFW_KEY_TAB,
        KeyBackspace = GLFW_KEY_BACKSPACE,
        KeyInsert = GLFW_KEY_INSERT,
        KeyDelete = GLFW_KEY_DELETE,
        KeyRight = GLFW_KEY_RIGHT,
        KeyLeft = GLFW_KEY_LEFT,
        KeyDown = GLFW_KEY_DOWN,
        KeyUp = GLFW_KEY_UP,
        KeyPageUp = GLFW_KEY_PAGE_UP,
        KeyPageDown = GLFW_KEY_PAGE_DOWN,
        KeyHome = GLFW_KEY_HOME,
        KeyEnd = GLFW_KEY_END,
        KeyCapsLock = GLFW_KEY_CAPS_LOCK,
        KeyScrollLock = GLFW_KEY_SCROLL_LOCK,
        KeyNumLock = GLFW_KEY_NUM_LOCK,
        KeyPrintScreen = GLFW_KEY_PRINT_SCREEN,
        KeyPause = GLFW_KEY_PAUSE,
        KeyF1 = GLFW_KEY_F1,
        KeyF2 = GLFW_KEY_F2,
        KeyF3 = GLFW_KEY_F3,
        KeyF4 = GLFW_KEY_F4,
        KeyF5 = GLFW_KEY_F5,
        KeyF6 = GLFW_KEY_F6,
        KeyF7 = GLFW_KEY_F7,
        KeyF8 = GLFW_KEY_F8,
        KeyF9 = GLFW_KEY_F9,
        KeyF10 = GLFW_KEY_F10,
        KeyF11 = GLFW_KEY_F11,
        KeyF12 = GLFW_KEY_F12,
        KeyF13 = GLFW_KEY_F13,
        KeyF14 = GLFW_KEY_F14,
        KeyF15 = GLFW_KEY_F15,
        KeyF16 = GLFW_KEY_F16,
        KeyF17 = GLFW_KEY_F17,
        KeyF18 = GLFW_KEY_F18,
        KeyF19 = GLFW_KEY_F19,
        KeyF20 = GLFW_KEY_F20,
        KeyF21 = GLFW_KEY_F21,
        KeyF22 = GLFW_KEY_F22,
        KeyF23 = GLFW_KEY_F23,
        KeyF24 = GLFW_KEY_F24,
        KeyF25 = GLFW_KEY_F25,
        KeyKP0 = GLFW_KEY_KP_0,
        KeyKP1 = GLFW_KEY_KP_1,
        KeyKP2 = GLFW_KEY_KP_2,
        KeyKP3 = GLFW_KEY_KP_3,
        KeyKP4 = GLFW_KEY_KP_4,
        KeyKP5 = GLFW_KEY_KP_5,
        KeyKP6 = GLFW_KEY_KP_6,
        KeyKP7 = GLFW_KEY_KP_7,
        KeyKP8 = GLFW_KEY_KP_8,
        KeyKP9 = GLFW_KEY_KP_9,
        KeyKPDecimal = GLFW_KEY_KP_DECIMAL,
        KeyKPDivide = GLFW_KEY_KP_DIVIDE,
        KeyKPMultiply = GLFW_KEY_KP_MULTIPLY,
        KeyKPSubtract = GLFW_KEY_KP_SUBTRACT,
        KeyKPAdd = GLFW_KEY_KP_ADD,
        KeyKPEnter = GLFW_KEY_KP_ENTER,
        KeyKPEqual = GLFW_KEY_KP_EQUAL,
        KeyLeftShip = GLFW_KEY_LEFT_SHIFT,
        KeyLeftControl = GLFW_KEY_LEFT_CONTROL,
        KeyLeftAlt = GLFW_KEY_LEFT_ALT,
        KeyLeftSuper = GLFW_KEY_LEFT_SUPER,
        KeyRightShift = GLFW_KEY_RIGHT_SHIFT,
        KeyRightControl = GLFW_KEY_RIGHT_CONTROL,
        KeyRightAlt = GLFW_KEY_RIGHT_ALT,
        KeyRightSuper = GLFW_KEY_RIGHT_SUPER,
        KeyMenu = GLFW_KEY_MENU,
        KeyMax = GLFW_KEY_LAST
    };

    enum Mod {
        ModeNone = 0,
        ModeShift = GLFW_MOD_SHIFT,
        ModeControl = GLFW_MOD_CONTROL,
        ModeAlt = GLFW_MOD_ALT,
        ModeModSuper = GLFW_MOD_SUPER,
        ModeModCapsLock = GLFW_MOD_CAPS_LOCK,
        ModeModNumLock = GLFW_MOD_NUM_LOCK,
    };

    enum Mouse {
        MouseNone = -1,
        MouseButton1 = GLFW_MOUSE_BUTTON_1,
        MouseButton2 = GLFW_MOUSE_BUTTON_2,
        MouseButton3 = GLFW_MOUSE_BUTTON_3,
        MouseButton4 = GLFW_MOUSE_BUTTON_4,
        MouseButton5 = GLFW_MOUSE_BUTTON_5,
        MouseButton6 = GLFW_MOUSE_BUTTON_6,
        MouseButton7 = GLFW_MOUSE_BUTTON_7,
        MouseButton8 = GLFW_MOUSE_BUTTON_8,
        MouseButtonLeft = GLFW_MOUSE_BUTTON_LEFT,
        MouseButtonRight = GLFW_MOUSE_BUTTON_RIGHT,
        MouseButtonMiddle = GLFW_MOUSE_BUTTON_MIDDLE,
        MouseButtonLast = GLFW_MOUSE_BUTTON_LAST,
        MouseScroll = 8,
        MouseCursor = 9
    };

    inline std::vector<std::string> getInputModes() {
        return {
                "Game",
                "UI",
                "Editor"
        };
    }

    inline std::vector<std::string> getInputTypes() {
        return {
                "None",
                "Keyboard",
                "Mouse",
        };
    }

    inline std::vector<std::string> getInputStates() {
        return {
                "None",
                "Release",
                "Press",
                "Repeat"
        };
    }

    inline std::vector<std::string> getInputKeys() {
        return {
                "Unknown",
                "Space",
                "Apostrophe",
                "Comma",
                "Minus",
                "Period",
                "Slash",
                "Num0",
                "Num1",
                "Num2",
                "Num3",
                "Num4",
                "Num5",
                "Num6",
                "Num7",
                "Num8",
                "Num9",
                "Num9",
                "Semicolon",
                "Equal",
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z",
                "LeftBracket",
                "Backslash",
                "RightBracket",
                "GraveAccent",
                "Escape",
                "Enter",
                "Tab",
                "Backspace",
                "Insert",
                "Delete",
                "Right",
                "Left",
                "Down",
                "Up",
                "PageUp",
                "PageDown",
                "Home",
                "End",
                "CapsLock",
                "ScrollLock",
                "NumLock",
                "PrintScreen",
                "Pause",
                "F1",
                "F2",
                "F3",
                "F4",
                "F5",
                "F6",
                "F7",
                "F8",
                "F9",
                "F10",
                "F11",
                "F12",
                "F13",
                "F14",
                "F15",
                "F16",
                "F17",
                "F18",
                "F19",
                "F20",
                "F21",
                "F22",
                "F23",
                "F24",
                "F25",
                "FP0",
                "FP1",
                "FP2",
                "FP3",
                "FP4",
                "FP5",
                "FP6",
                "FP7",
                "FP8",
                "FP9",
                "KPDecimal",
                "KPDivide",
                "KPMultiply",
                "KPSubtract",
                "KPAdd",
                "KPEnter",
                "KPEqual",
                "LeftShip",
                "LeftControl",
                "LeftAlt",
                "LeftSuper",
                "RightShift",
                "RightControl",
                "RightAlt",
                "RightSuper",
                "Menu",
                "Max",
        };
    }

    inline std::vector<std::string> getInputMouse() {
        return {
            "None",
            "Button1",
            "Button2",
            "Button3",
            "Button4",
            "Button5",
            "Button6",
            "Button7",
            "Button8",
            "ButtonLeft",
            "ButtonRight",
            "ButtonMiddle",
            "ButtonLast",
            "Scroll",
            "Cursor"
        };
    }

    struct Input {
        inline bool operator==(const Input& other) const {
            return type == other.type && input == other.input && action == other.action && mods == other.mods;
        }

        LVE_SERIALIZE_TYPE_INTRUSIVE(Input, type, input, action, mods);

        InputType type{};
        int input{};
        int action{};
        int mods{};
        glm::vec<2, double> values{};
    };

    class InputBinding {
    public:
        InputBinding() = default;

        InputBinding(std::string name, const Input& input, InputMode mode);

        LVE_SERIALIZE_TYPE_INTRUSIVE(InputBinding, name, input, mode)

    public:
        InputMode mode{};
        Input input;
        std::string name;
        std::function<void(const Input&)> callback = [](const Input&){};
    };

    inline void imSerialize(Input& input) {
        int type = input.type;
        imSerialize("Type", getInputTypes(), type);
        input.type = static_cast<InputType>(type);

        std::vector<std::string> inputs;
        switch (input.type) {
            case InputTypeNone:
                inputs = {};
                break;
            case InputTypeKeyboard:
                inputs = getInputKeys();
                break;
            case InputTypeMouse:
                inputs = getInputMouse();
                break;
            case InputTypeGamepad:
                break;
        }

        imSerialize("Input", inputs, input.input, 0);
        imSerialize("Action", getInputStates(), input.action);

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted("Shift");
        ImGui::TableNextColumn();
        ImGui::CheckboxFlags("##Shift", &input.mods, ModeShift);

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted("Ctrl");
        ImGui::TableNextColumn();
        ImGui::CheckboxFlags("##Ctrl", &input.mods, ModeControl);

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted("Alt");
        ImGui::TableNextColumn();
        ImGui::CheckboxFlags("##Alt", &input.mods, ModeAlt);
    }

    inline void imSerialize(InputBinding& binding) {
        if (ImGui::BeginTable("##Binding", 2)) {
            ImGui::TableSetupColumn("", ImGuiTableColumnFlags_WidthFixed, 50.0f);
            ImGui::TableSetupColumn("");

            imSerialize("Name", binding.name);

            int mode = binding.mode;
            imSerialize("Mode", getInputModes(), mode);
            binding.mode = static_cast<InputMode>(mode);

            imSerialize(binding.input);
            ImGui::EndTable();
        }
    }

}

namespace std {

    template <>
    struct hash<lve::Input> {
        size_t operator()(lve::Input const &binding) const {
            size_t seed = 0;
            lve::hashCombine(seed, binding.type, binding.input, binding.action, binding.mods);
            return seed;
        }
    };

}  // namespace std


#endif //LITTLEVULKANENGINE_INPUTBINDING_HPP
