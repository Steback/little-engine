#include "InputSystem.hpp"

#include "logs/Logs.hpp"


namespace lve {

    InputSystem::InputSystem(InputMode inputMode) : inputMode(inputMode) {

    }

    void InputSystem::processInput(const Input &input) {
        if (hashedBindings.contains(input)) {
            hashedBindings[input]->callback(input);
        }
    }

    void InputSystem::addBinding(const std::string &name, const Input &input) {
        if (bindings.contains(name) || hashedBindings.contains(input)) {
            LVE_LOG_WARN("[Input] Binding already exists, will be overwrite: {}", name);
        }

        LVE_LOG_INFO("[Input] Binding added: {}", name);
        bindings[name] = std::make_shared<InputBinding>(name, input, inputMode);
        hashedBindings[input] = bindings[name];
    }

    std::shared_ptr<InputBinding> InputSystem::getBinding(const std::string& name) const {
        if (bindings.contains(name)) {
            return bindings.at(name);
        } else {
            LVE_LOG_ERROR("[Input] Binding doesn't exists: {}", name);
            return nullptr;
        }
    }

    void InputSystem::renameBinding(const std::string &oldName, const std::string &newName) {
        if (bindings.contains(oldName)) {
            auto binding = bindings[oldName];
            bindings.erase(oldName);

            binding->name = newName;
            bindings[newName] = binding;
        } else {
            LVE_LOG_ERROR("[Input] Binding doesn't exists, cannot rename it: {}", oldName);
        }
    }

    void InputSystem::changeBindingInput(const std::string &name, const Input &newInput) {
        if (bindings.contains(name)) {
            auto binding = bindings[name];
            hashedBindings.erase(binding->input);

            binding->input = newInput;
            hashedBindings[newInput] = binding;
        } else {
            LVE_LOG_ERROR("[Input] Binding doesn't exists, cannot change input it: {}", name);
        }
    }

} // namespace lv