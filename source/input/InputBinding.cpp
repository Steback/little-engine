#include "InputBinding.hpp"

#include "ui/ImSerialize.hpp"


namespace lve {

    InputBinding::InputBinding(std::string name, const Input& input, InputMode mode)
            : name(std::move(name)), input(input), mode(mode) {

    }

}
