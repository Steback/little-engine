#ifndef LITTLEVULKANENGINE_INPUTMANAGER_HPP
#define LITTLEVULKANENGINE_INPUTMANAGER_HPP


#include <memory>

#include <GLFW/glfw3.h>

#include "InputSystem.hpp"
#include "render/Window.hpp"
#include "utils/Singleton.hpp"
#include "config/Settings.hpp"


namespace lve {

    class InputSettings;

    class InputManager : public Singleton<InputManager> {
        friend Singleton<InputManager>;

        explicit InputManager(const std::shared_ptr<InputSettings>& settings);

    public:
        void windowFocusCallback(int focused);

        void cursorEnterCallback(int entered);

        void cursorPosCallback(double x, double y);

        void mouseButtonCallback(int button, int action, int mods);

        void scrollCallback(double xoffset, double yoffset);

        void keyCallback(int key, int scancode, int action, int mods);

        void charCallback(unsigned int c);

        inline void setMode(InputMode mode) { currenMode = mode; }

        [[nodiscard]] inline InputMode getMode() const { return currenMode; }

        [[nodiscard]] inline auto getSystem(InputMode mode) const { return systems[mode]; }

    private:
        InputMode currenMode{};
        std::shared_ptr<InputSettings> settings;
        std::vector<std::shared_ptr<InputSystem>> systems;
    };

    class InputSettings : public SettingsInterface {
    public:
        LVE_REGISTRY_SETTINGS_FIELD(Input)

        InputSettings() = default;

        void save(nlohmann::json &archive) override;

        void load(const nlohmann::json &archive) override;

        void draw() override;

        std::vector<InputBinding> bindings;
    };

}


#endif //LITTLEVULKANENGINE_INPUTMANAGER_HPP
