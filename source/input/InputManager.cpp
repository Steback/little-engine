#include "InputManager.hpp"


namespace lve {

    InputManager::InputManager(const std::shared_ptr<InputSettings> &settings) : settings(settings) {
        systems.resize(Max);
        for (int i = 0; i < systems.size(); ++i) {
            systems[i] = std::make_shared<InputSystem>((InputMode)i);
        }

        for (const auto& binding : settings->bindings) {
            systems[binding.mode]->addBinding(binding.name, binding.input);
        }
    }

    void InputManager::windowFocusCallback(int focused) {

    }

    void InputManager::cursorEnterCallback(int entered) {

    }

    void InputManager::cursorPosCallback(double x, double y) {
        systems[currenMode]->processInput({InputTypeMouse, MouseCursor, 0, 0, {x, y}});
    }

    void InputManager::mouseButtonCallback(int button, int action, int mods) {
        systems[currenMode]->processInput({InputTypeMouse, button, action, mods, {}});
    }

    void InputManager::scrollCallback(double xoffset, double yoffset) {
        systems[currenMode]->processInput({InputTypeMouse, MouseScroll, 0, 0, {xoffset, yoffset}});
    }

    void InputManager::keyCallback(int key, int scancode, int action, int mods) {
        systems[currenMode]->processInput({InputTypeKeyboard, key, action, mods, {}});
    }

    void InputManager::charCallback(unsigned int c) {

    }

    void InputSettings::save(nlohmann::json &archive) {
        LVE_SAVE_FIELD_ATTRIBUTE(archive, bindings);
    }

    void InputSettings::load(const nlohmann::json &archive) {
        LVE_LOAD_FIELD_ATTRIBUTE(archive, bindings);
    }

    void InputSettings::draw() {
        imSerialize("Bindings", bindings);
    }

}
