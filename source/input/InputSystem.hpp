#ifndef LITTLEVULKANENGINE_INPUTSYSTEM_HPP
#define LITTLEVULKANENGINE_INPUTSYSTEM_HPP


#include <array>
#include <memory>
#include <unordered_map>

#include "GLFW/glfw3.h"

#include "InputBinding.hpp"
#include "utils/NonCopyable.hpp"


namespace lve {

    class InputSystem {
    public:
        explicit InputSystem(InputMode inputMode);

        void processInput(const Input& input);

        void addBinding(const std::string& name, const Input& input);

        [[nodiscard]] std::shared_ptr<InputBinding> getBinding(const std::string& name) const;

        void renameBinding(const std::string& oldName, const std::string& newName);

        void changeBindingInput(const std::string& name, const Input& newInput);

        [[nodiscard]] inline InputMode getInputMode() const { return inputMode; }

        inline std::unordered_map<std::string, std::shared_ptr<InputBinding>> getBindings() const { return bindings; }

    private:
        InputMode inputMode;
        std::unordered_map<std::string, std::shared_ptr<InputBinding>> bindings;
        std::unordered_map<Input, std::shared_ptr<InputBinding>> hashedBindings;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_INPUTSYSTEM_HPP
