#include "Application.hpp"

#include "logs/Logs.hpp"
#include "ui/Viewport.hpp"
#include "render/Window.hpp"
#include "ui/SceneWidget.hpp"
#include "config/Settings.hpp"
#include "ui/EntityWidget.hpp"
#include "render/Renderer.hpp"
#include "ui/ConsoleWidget.hpp"
#include "ui/SettingsWidget.hpp"
#include "render/SceneRender.hpp"
#include "input/InputManager.hpp"
#include "render/ImGuiRender.hpp"
#include "scene/ScenesManager.hpp"
#include "assets/AssetsManager.hpp"
#include "commands/CommandsManager.hpp"


namespace lve {

    Application::Application(const std::string& name) : name(name) {
        root = std::filesystem::current_path().parent_path();
        data = root / "data";

        LogsManager::setup(root);
        LogsManager::get()->addLogger(LVE_LOGS, LVE_LOGS_FILE);

        LVE_LOG_INFO("[Engine] Root path: {}", root.string());
        LVE_LOG_INFO("[Engine] Data path: {}", data.string());
        LVE_LOG_INFO("[Application] Name: {}", name);

        settings = std::make_shared<Settings>(root / "Settings.json");
        settings->name = name;
    }

    Application::~Application() {
        Renderer::get()->clearLayer();
        consoleWidget.reset();
        Viewport::destroy();
        imGuiRenderSystem.reset();
        renderSystem.reset();
        AssetsManager::destroy();
        ScenesManager::destroy();
        Renderer::destroy();
        window.reset();
        InputManager::destroy();
        settings.reset();
    }

    void Application::run(int argc, char **argv) {
        setup();

        auto currentTime = std::chrono::high_resolution_clock::now();
        while (window->isOpen()) {
            auto newTime = std::chrono::high_resolution_clock::now();
            float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
            currentTime = newTime;

            glfwPollEvents();
            update(frameTime);
            Renderer::get()->render(camera);
        }

        Renderer::get()->waitForDevice();
    }

    void Application::setup() {
        setupSettingsFields();
        if (!exists(settings->getPath())) {
            settings->save();
        }

        settings->load();

        InputManager::setup(settings->getField<InputSettings>());
        setupInputBindings();

        auto windowSettings = settings->getField<WindowSettings>();
        windowSettings->title = name;
        window = std::make_shared<Window>(windowSettings);
        Renderer::setup(window, settings);

        AssetsManager::setup(data);
        ScenesManager::setup(settings->getField<SceneSettings>());

        renderSystem = std::make_shared<SceneRender>();
        Renderer::get()->addLayer(renderSystem);

        imGuiRenderSystem = std::make_shared<ImGuiRender>(settings->getField<ImGuiSettings>());
        Renderer::get()->addLayer(imGuiRenderSystem);

        Viewport::setup();
        consoleWidget = std::make_shared<ConsoleWidget>();
        consoleWidget->addToViewport();
        setupConsoleCommands();

        settingsWidget = std::make_shared<SettingsWidget>(settings);

        sceneWidget = std::make_shared<SceneWidget>();
        sceneWidget->addToViewport();

        entityWidget = std::make_shared<EntityWidget>(sceneWidget);
        entityWidget->addToViewport();
    }

    void Application::update(float deltaTime) {
        float aspect = window->getAspectRatio();
        camera.setPerspectiveProjection(glm::radians(50.f), aspect, 0.1f, 10.f);

        ImGui::NewFrame();

        if (showImGuiDemo) {
            ImGui::ShowDemoWindow(&showImGuiDemo);
        }

        Viewport::get()->update();

        ImGui::Render();

        // TODO: Use Scenes manager singleton inside render systems
        auto scene = ScenesManager::get()->getCurrent();
        renderSystem->update(scene, deltaTime);
        imGuiRenderSystem->update(scene, deltaTime);
    }

    void Application::setupSettingsFields() {
        settings->addField<WindowSettings>();
        settings->addField<RendererSettings>();
        settings->addField<ImGuiSettings>();
        settings->addField<InputSettings>();
        settings->addField<SceneSettings>();
    }

    void Application::setupConsoleCommands() {
        consoleWidget->addCommand("undo", []{ lve::CommandsManager::get()->undo(); });
        consoleWidget->addCommand("redo", []{ lve::CommandsManager::get()->redo(); });
        consoleWidget->addCommand("exit", []{ exit(0); });
        consoleWidget->addCommand("showImGuiDemo", [this]{ showImGuiDemo = true; });

        auto* settingsCommand = consoleWidget->addCommand("settings", [this]{
            if (!settingsWidget->isEnable()) {
                settingsWidget->addToViewport();
            } else {
                settingsWidget->show();
            }
        });

        consoleWidget->addCommand("save", [this]{ settings->save(); }, settingsCommand);
        consoleWidget->addCommand("load", [this]{ settings->load(); }, settingsCommand);

        auto sceneCommand = consoleWidget->addCommand("scene", [this] {
            if (!sceneWidget->isEnable()) {
                sceneWidget->addToViewport();
            } else {
                sceneWidget->show();
            }
        });

        consoleWidget->addCommand("save", []{ if (auto scene = ScenesManager::get()->getCurrent()) scene->save(); }, sceneCommand);
        consoleWidget->addCommand("load", []{ if (auto scene = ScenesManager::get()->getCurrent()) scene->load(); }, sceneCommand);

        consoleWidget->addCommand("entity", [this] {
           if (!entityWidget->isEnable())  {
               entityWidget->addToViewport();
           } else {
               entityWidget->show();
           }
        });
    }

    void Application::setupInputBindings() {
        auto inputSystem = InputManager::get()->getSystem(InputMode::Game);
        inputSystem->addBinding("Open Console", {InputType::InputTypeKeyboard, KeyGraveAccent, StateRelease});
        inputSystem->getBinding("Open Console")->callback = [this] (const Input& input) {
            if (consoleWidget->isOpen()) {
                consoleWidget->hide();
            } else {
                consoleWidget->show();
            }
        };
    }

} // namespace lve