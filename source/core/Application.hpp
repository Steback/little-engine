#ifndef LITTLEVULKANENGINE_APPLICATION_HPP
#define LITTLEVULKANENGINE_APPLICATION_HPP


#include <memory>
#include <filesystem>

#include "render/Camera.hpp"


namespace lve {

    class Window;
    class Settings;
    class SceneRender;
    class ImGuiRender;
    class SceneWidget;
    class EntityWidget;
    class ConsoleWidget;
    class SettingsWidget;

    /**
     * @brief Application interface
     */
    class Application {
    public:
        explicit Application(const std::string& name);

        ~Application();

        /**
         * @brief Function to start and run application
         * @param argc: Num of arguments
         * @param argv: Arguments values pointer
         * @note This functions must called just once and in the main function.
         */
        void run(int argc, char** argv);

        /**
         * @brief Virtual setup function for create the necessary objects and configs.
         * @note Must call Application::setup first.
         */
        virtual void setup();

        /**
         * @brief Virtual update function called every frame.
         * @param deltaTime: Delta time of current frame.
         * @note Must call Application::update first.
         */
        virtual void update(float deltaTime);

        /**
         * @brief Virtual function for setup settings fields for engine and application(if is any).
         * @note Must call Application::setupSettingsFields first.
         */
        virtual void setupSettingsFields();

        /**
         * @brief Virtual function for setup the commands that can be used in Console Widget
         * @note Must call Application::setupConsoleCommands first.
         */
        virtual void setupConsoleCommands();

        /**
         * @brief Virtual function for setup input bindings
         * @note Must call Application::setupInputBindings first.
         */
        virtual void setupInputBindings();

    protected:
        bool showImGuiDemo{false};
        std::string name;
        Camera camera;
        std::shared_ptr<Window> window;
        std::shared_ptr<Settings> settings;
        std::shared_ptr<SceneWidget> sceneWidget;
        std::shared_ptr<SceneRender> renderSystem;
        std::shared_ptr<EntityWidget> entityWidget;
        std::shared_ptr<ConsoleWidget> consoleWidget;
        std::shared_ptr<SettingsWidget> settingsWidget;
        std::shared_ptr<ImGuiRender> imGuiRenderSystem;
        std::filesystem::path root;
        std::filesystem::path data;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_APPLICATION_HPP