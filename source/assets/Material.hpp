#ifndef LITTLEVULKANENGINE_MATERIAL_HPP
#define LITTLEVULKANENGINE_MATERIAL_HPP


#include <glm/glm.hpp>

#include "Texture.hpp"
#include "utils/Serialize.hpp"
#include "render/descriptor/DescriptorLayout.hpp"


#define LVE_MATERIAL_EXT ".mat"


namespace lve {

    inline void to_json(nlohmann::json& archive, const glm::vec4& vector) {
        archive.push_back(vector.x);
        archive.push_back(vector.y);
        archive.push_back(vector.z);
        archive.push_back(vector.w);
    }

    inline void from_json(const nlohmann::json& archive, glm::vec4& vector) {
        vector.x = archive[0];
        vector.y = archive[1];
        vector.z = archive[2];
        vector.w = archive[3];
    }

    struct MaterialFile {
        glm::vec4 baseColor{1.0f};
        std::string baseName;

        LVE_SERIALIZE_TYPE_INTRUSIVE(MaterialFile, baseColor, baseName)
    };

    class Material {
    public:
        Material(std::string baseName, std::shared_ptr<Texture> base, const glm::vec4& baseColor);

        void bind(VkCommandBuffer cmdBuffer, VkPipelineLayout layout);

    public:
        VkDescriptorSet descriptorSet{};
        glm::vec4 baseColor{1.0f};
        std::shared_ptr<Texture> base;
        std::string baseName;
    };

} // lve


#endif //LITTLEVULKANENGINE_MATERIAL_HPP
