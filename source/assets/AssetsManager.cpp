#include "AssetsManager.hpp"

#include "File.hpp"
#include "utils/Utils.hpp"
#include "render/Renderer.hpp"
#include "loader/ObjLoader.hpp"
#include "loader/PNGJPGLoader.hpp"
#include "render/descriptor/DescriptorWriter.hpp"


namespace lve {

    AssetsManager::AssetsManager(std::filesystem::path data)
            : data(std::move(data)) {
        modelLoaders[".obj"] = std::make_shared<ObjLoader>(this);
        texturesLoaders[".png"] = std::make_shared<PNGJPGLoader>(this);
        texturesLoaders[".jpg"] = texturesLoaders[".png"];
        materialLoader = std::make_shared<MaterialLoader>(this);
    }

    AssetsManager::~AssetsManager() {
        materialLoader.reset();
        texturesLoaders.clear();
        modelLoaders.clear();

        materials.clear();
        textures.clear();
        meshes.clear();
    }

    void AssetsManager::setupDescriptorsSets(const std::shared_ptr<DescriptorPool> &pool, const std::shared_ptr<DescriptorLayout> &layout) {
        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        for (auto& [id, material] : materials) {
            imageInfo.sampler = material->base->getSampler();
            imageInfo.imageView = material->base->getImage()->getView();

            DescriptorWriter descriptorWrite(layout, pool);
            descriptorWrite.writeImage(0, &imageInfo);
            descriptorWrite.build(material->descriptorSet);
        }
    }

    File AssetsManager::getFile(const std::filesystem::path &path) {
        auto absolutePath = data / path;
        if (exists(absolutePath)) {
            return File(absolutePath);
        } else {
            LVE_LOG_ERROR("[Assets] File not exists, or path isn't valid: {}", path.string());
            return {};
        }
    }

    std::pair<std::string, std::shared_ptr<Mesh>> AssetsManager::addMesh(const std::string &fileName) {
        File file(data / fileName);
        if (!file.exists()) {
            LVE_LOG_ERROR("[Assets] Failed to load mesh. Path isn't valid or not exists: {}", file.getPath().string());
            return {};
        }

        auto extension = file.getExtension();
        if (!modelLoaders.contains(extension)) {
            LVE_LOG_ERROR("[Assets] Failed to load mesh. Isn't a supported format: {}", file.getPath().string());
            return {};
        }

        std::vector<Vertex> vertices;
        std::vector<uint32_t> indices;
        modelLoaders[extension]->load(file.getPath());
        modelLoaders[extension]->loadMesh(vertices, indices);

        const std::string id = file.getName();
        meshes[id] = std::make_unique<Mesh>(Renderer::get()->getDevice(), vertices, indices);

        return { id, meshes[id] };
    }

    std::shared_ptr<Mesh> AssetsManager::getMesh(const std::string &id) {
        return meshes[id];
    }

    std::pair<std::string, std::shared_ptr<Texture>> AssetsManager::addTexture(const std::string &fileName) {
        File file(data / fileName);
        if (!file.exists()) {
            LVE_LOG_ERROR("[Assets] Failed to load texture. Path isn't valid or not exists: {}", file.getPath().string());
            return {};
        }

        auto extension = file.getExtension();
        if (!texturesLoaders.contains(extension)) {
            LVE_LOG_ERROR("[Assets] Failed to load texture. Isn't a supported format: {}", file.getPath().string());
            return {};
        }

        uint32_t width;
        uint32_t height;
        uint32_t channels;
        const auto* textureData = texturesLoaders[extension]->load(file.getPath(), width, height, channels);

        const std::string id = file.getName();
        textures[id] = std::make_shared<Texture>(Renderer::get()->getDevice(), VkExtent3D{width, height, 1}, textureData, width * height * 4);
        return { id, textures[id] };
    }

    std::shared_ptr<Texture> AssetsManager::getTexture(const std::string &id) {
        return textures[id];
    }

    std::pair<std::string, std::shared_ptr<Material>> AssetsManager::addMaterial(const std::string &fileName) {
        File file(data / fileName);
        if (!file.exists()) {
            LVE_LOG_ERROR("[Assets] Failed to load material. Path isn't valid or not exists: {}", file.getPath().string());
            return {};
        }

        const std::string id = file.getName();
        materials[id] = materialLoader->load(file.getPath());
        return { id, materials[id] };
    }

    std::shared_ptr<Material> AssetsManager::getMaterial(const std::string &id) {
        return materials[id];
    }

} // namespace lv