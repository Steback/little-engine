#include "Mesh.hpp"

#include "render/Command.hpp"
#include "render/buffer/StagingBuffer.hpp"


namespace lve {

    std::vector<VkVertexInputBindingDescription> Vertex::getBindingDescriptions() {
        return {
            { 0, sizeof(Vertex), VK_VERTEX_INPUT_RATE_VERTEX}
        };
    }

    std::vector<VkVertexInputAttributeDescription> Vertex::getAttributeDescriptions() {
        return {
            { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)offsetof(Vertex, position) },
            { 1, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)offsetof(Vertex, color) },
            { 2, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)offsetof(Vertex, normal) },
            { 3, 0, VK_FORMAT_R32G32_SFLOAT, (uint32_t)offsetof(Vertex, uv) },
        };
    }

    bool Vertex::operator==(const Vertex &other) const {
        return position == other.position && color == other.color && normal == other.normal && uv == other.uv;
    }

    Mesh::Mesh(const std::shared_ptr<Device> &device, const std::vector<Vertex> &vertices, const std::vector<uint32_t> &indices) {
        indicesCount = indices.size();
        verticesCount = vertices.size();

        VkDeviceSize vertexBufferSize = sizeof(vertices[0]) * verticesCount;
        StagingBuffer vertexStagingBuffer(device, vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, vertices.data());
        vertexBuffer = std::make_unique<Buffer>(device, vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_ONLY);

        VkDeviceSize indexBufferSize = sizeof(indices[0]) * indicesCount;
        StagingBuffer indexStagingBuffer(device, indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, indices.data());
        indexBuffer = std::make_unique<Buffer>(device, indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_ONLY);

        Queue transferQueue = device->getQueue(VK_QUEUE_TRANSFER_BIT);
        OnceCommandBuffer copyCommandBuffer(device, transferQueue.index, transferQueue);
        vertexStagingBuffer.copyTo(copyCommandBuffer.get(), *vertexBuffer);
        indexStagingBuffer.copyTo(copyCommandBuffer.get(), *indexBuffer);
        copyCommandBuffer.end();
    }

    void Mesh::bind(const VkCommandBuffer &commandBuffer) {
        VkBuffer buffers[] = { *vertexBuffer };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offsets);
        vkCmdBindIndexBuffer(commandBuffer, *indexBuffer, 0, VK_INDEX_TYPE_UINT32);
    }

    void Mesh::draw(const VkCommandBuffer &commanderBuffer) const {
        vkCmdDrawIndexed(commanderBuffer, indicesCount, 1, 0, 0, 0);
    }

} // namespace lv