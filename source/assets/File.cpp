#include "File.hpp"

#include <fstream>

#include "utils/Utils.hpp"


namespace lve {

    File::File(std::filesystem::path path) : path(std::move(path)) {

    }

    void File::setPath(std::filesystem::path newPath) {
        path = std::move(newPath);
    }

    std::vector<char> File::read(bool binary) {
        std::ifstream file{path, binary ? std::ios::ate | std::ios::binary : std::ios::ate };
        if (!file.is_open()) {
            LVE_THROW_EX("failed to open file: {}", path.string());
        }

        size_t fileSize = static_cast<size_t>(file.tellg());
        std::vector<char> buffer(fileSize);

        file.seekg(0);
        file.read(buffer.data(), static_cast<std::streamsize>(fileSize));
        file.close();

        return buffer;
    }

    void File::remove() {
        if (std::filesystem::exists(path)) {
            std::filesystem::remove(path);
        }
    }

    void File::read(nlohmann::json& archive) {
        std::ifstream file(path);
        if (!file.is_open()) {
            LVE_THROW_EX("failed to open file: {}", path.string());
        }

        file >> archive;
        file.close();
    }

    void File::write(const nlohmann::json& archive) {
        std::ofstream file(path);
        file << std::setw(4) << archive;
        file.close();
    }

} // namespace lv