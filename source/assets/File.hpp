#ifndef LITTLEVULKANENGINE_FILE_HPP
#define LITTLEVULKANENGINE_FILE_HPP


#include <vector>
#include <filesystem>

#include <nlohmann/json.hpp>


namespace lve {

    class File {
    public:
        File() = default;

        explicit File(std::filesystem::path path);

        void setPath(std::filesystem::path newPath);

        std::vector<char> read(bool binary = false);

        void read(nlohmann::json& archive);

        void write(const nlohmann::json& archive);

        void remove();

        [[nodiscard]] inline bool exists() const { return std::filesystem::exists(path); }

        [[nodiscard]] inline std::filesystem::path getPath() const { return path; }

        [[nodiscard]] inline std::string getExtension() const { return path.extension().string(); }

        [[nodiscard]] inline std::string getName(bool withExtensions = true) const { return withExtensions ? path.filename().string() : path.stem().string(); }

    private:
        std::filesystem::path path;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_FILE_HPP
