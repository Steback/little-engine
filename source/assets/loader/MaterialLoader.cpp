#include "MaterialLoader.hpp"

#include "assets/AssetsManager.hpp"


namespace lve {

    MaterialLoader::MaterialLoader(AssetsManager *manager) : manager(manager) {

    }

    std::unique_ptr<Material> MaterialLoader::load(const std::filesystem::path &path) {
        File file(path);

        nlohmann::json archive;
        file.read(archive);

        MaterialFile materialData = archive;
        auto base = manager->addTexture("models/Viking Room/" + materialData.baseName);

        return std::make_unique<Material>(materialData.baseName, base.second, materialData.baseColor);
    }

}