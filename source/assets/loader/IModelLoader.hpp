#ifndef LITTLEVULKANENGINE_IMODELLOADER_HPP
#define LITTLEVULKANENGINE_IMODELLOADER_HPP


#include <string>
#include <vector>
#include <filesystem>

#include "assets/Mesh.hpp"


namespace lve {

    class AssetsManager;

    class IModelLoader {
    protected:
        explicit IModelLoader(AssetsManager* manager) : manager(manager) {  }

    public:
        virtual ~IModelLoader() = default;

        virtual void loadMesh(std::vector<Vertex>& vertices, std::vector<uint32_t>& indices) = 0;

        virtual void load(std::filesystem::path inPath) { loaded = false; path = std::move(inPath); }

        [[nodiscard]] inline std::filesystem::path getPath() const { return path; }

        [[nodiscard]] inline bool isLoaded() const { return loaded; }

    protected:
        bool loaded{false};
        AssetsManager* manager;
        std::filesystem::path path;
    };

}


#endif //LITTLEVULKANENGINE_IMODELLOADER_HPP
