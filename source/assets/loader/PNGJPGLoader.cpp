#include "PNGJPGLoader.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <volk.h>

#include "logs/Logs.hpp"


namespace lve {

    PNGJPGLoader::PNGJPGLoader(AssetsManager *manager) : ITextureLoader(manager) {

    }

    const unsigned char* PNGJPGLoader::load(const std::filesystem::path& path, uint32_t &width, uint32_t& height, uint32_t& channels) {
        int texWidth, texHeight, texChannels;
        stbi_uc* pixels = stbi_load(path.string().c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);

        if (!pixels) {
            LVE_LOG_ERROR("[PNGJPGLoader] Failed to load texture: {}", path.string());
            return nullptr;
        }

        width = texWidth;
        height = texHeight;
        channels = texChannels;

        return pixels;
    }

}
