#ifndef LITTLEVULKANENGINE_MATERIALLOADER_HPP
#define LITTLEVULKANENGINE_MATERIALLOADER_HPP


#include <memory>
#include <filesystem>

#include "assets/Material.hpp"


namespace lve {

    class AssetsManager;

    class MaterialLoader {
    public:
        explicit MaterialLoader(AssetsManager* manager);

        std::unique_ptr<Material> load(const std::filesystem::path& path);

    private:
        AssetsManager* manager;
    };

}


#endif //LITTLEVULKANENGINE_MATERIALLOADER_HPP
