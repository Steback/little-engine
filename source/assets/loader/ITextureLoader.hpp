#ifndef LITTLEVULKANENGINE_ITEXTURELOADER_HPP
#define LITTLEVULKANENGINE_ITEXTURELOADER_HPP


#include <filesystem>


namespace lve {

    class AssetsManager;

    class ITextureLoader {
    protected:
        explicit ITextureLoader(AssetsManager* manager) : manager(manager) {  }

    public:
        virtual ~ITextureLoader() = default;

        virtual const unsigned char* load(const std::filesystem::path& path, uint32_t &width, uint32_t& height, uint32_t& channels) = 0;

    protected:
        AssetsManager* manager;
    };

} // lve


#endif //LITTLEVULKANENGINE_ITEXTURELOADER_HPP
