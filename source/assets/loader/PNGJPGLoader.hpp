#ifndef LITTLEVULKANENGINE_PNGJPGLOADER_HPP
#define LITTLEVULKANENGINE_PNGJPGLOADER_HPP


#include "ITextureLoader.hpp"


namespace lve {

    class PNGJPGLoader : public ITextureLoader {
    public:
        explicit PNGJPGLoader(AssetsManager* manager);

        ~PNGJPGLoader() override = default;

        const unsigned char* load(const std::filesystem::path& path, uint32_t &width, uint32_t& height, uint32_t& channels) override;
    };

}


#endif //LITTLEVULKANENGINE_PNGJPGLOADER_HPP
