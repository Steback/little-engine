#ifndef LITTLEVULKANENGINE_OBJLOADER_HPP
#define LITTLEVULKANENGINE_OBJLOADER_HPP


#include <tiny_obj_loader.h>

#include "IModelLoader.hpp"


namespace lve {

    class ObjLoader : public IModelLoader {
    public:
        explicit ObjLoader(AssetsManager* manager);

        ~ObjLoader() override = default;

        void load(std::filesystem::path inPath) override;

        void loadMesh(std::vector<Vertex> &vertices, std::vector<uint32_t> &indices) override;

    private:
        tinyobj::attrib_t attrib;
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;
    };

}


#endif //LITTLEVULKANENGINE_OBJLOADER_HPP
