#include "ObjLoader.hpp"

#include <unordered_map>

#include "utils/Utils.hpp"
#include "assets/Material.hpp"
#include "assets/AssetsManager.hpp"


namespace std {

    template <>
    struct hash<lve::Vertex> {
        size_t operator()(lve::Vertex const &vertex) const {
            size_t seed = 0;
            hash<float> hasher;
            lve::hashCombine(seed, hasher(vertex.position.x), hasher(vertex.position.y), hasher(vertex.position.z));
            lve::hashCombine(seed, hasher(vertex.color.x), hasher(vertex.color.y), hasher(vertex.color.z));
            lve::hashCombine(seed, hasher(vertex.normal.x), hasher(vertex.normal.y), hasher(vertex.normal.z));
            lve::hashCombine(seed, hasher(vertex.uv.x), hasher(vertex.uv.y));
            return seed;
        }
    };

}  // namespace std

namespace lve {

    ObjLoader::ObjLoader(AssetsManager *manager) : IModelLoader(manager) {

    }

    void ObjLoader::load(std::filesystem::path inPath) {
        IModelLoader::load(inPath);

        std::string warn, err;
        if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, path.string().c_str(), path.parent_path().string().c_str())) {
            LVE_LOG_ERROR("[ObjLoader] {}", err);
            return;
        }

        if (!warn.empty()) {
            LVE_LOG_WARN("[ObjLoader] {}", warn);
        }

        for (const auto& materia : materials) {
            MaterialFile materialFile;
            materialFile.baseName = materia.diffuse_texname;
            materialFile.baseColor = {1.0f, 1.0f, 1.0f, 1.0f};

            nlohmann::json archive = materialFile;
            File file(inPath.parent_path() / fmt::format("{}{}", materia.name, LVE_MATERIAL_EXT));
            file.write(archive);
        }

        loaded = true;
    }

    void ObjLoader::loadMesh(std::vector<Vertex> &vertices, std::vector<uint32_t> &indices) {
        if (!loaded) {
            return;
        }

        std::unordered_map<Vertex, uint32_t> uniqueVertices{};
        for (const auto& shape : shapes) {
            for (const auto& index : shape.mesh.indices) {
                Vertex vertex{};

                if (index.vertex_index >= 0) {
                    vertex.position = {
                            attrib.vertices[3 * index.vertex_index + 0],
                            attrib.vertices[3 * index.vertex_index + 1],
                            attrib.vertices[3 * index.vertex_index + 2],
                    };
                }

                vertex.color = {1.0f, 1.0f, 1.0f};

                if (index.normal_index >= 0) {
                    vertex.normal = {
                            attrib.normals[3 * index.normal_index + 0],
                            attrib.normals[3 * index.normal_index + 1],
                            attrib.normals[3 * index.normal_index + 2],
                    };
                }

                if (index.texcoord_index >= 0) {
                    vertex.uv = {
                            attrib.texcoords[2 * index.texcoord_index + 0],
                            1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
                    };
                }

                if (uniqueVertices.count(vertex) == 0) {
                    uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                    vertices.push_back(vertex);
                }

                indices.push_back(uniqueVertices[vertex]);
            }
        }
    }

}
