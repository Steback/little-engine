#include "Texture.hpp"

#include "render/Command.hpp"
#include "render/buffer/StagingBuffer.hpp"


namespace lve {

    Texture::Texture(const std::shared_ptr<Device>& device, VkExtent3D size, const unsigned char *data, VkDeviceSize dataSize)
            : device(*device) {
        StagingBuffer stagingBuffer(device, dataSize, 0, data);

        image = ImageBuilder(device)
                .setType(VK_IMAGE_TYPE_2D)
                .setUsage(VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT)
                .setExtent(size)
                .setFormat(VK_FORMAT_R8G8B8A8_UNORM)
                .setMipLevel(1)
                .setArrayLayer(1)
                .setTiling(VK_IMAGE_TILING_OPTIMAL)
                .setImageLayout(VK_IMAGE_LAYOUT_UNDEFINED)
                .setSamplerCount(VK_SAMPLE_COUNT_1_BIT)
                .setSharingMode(VK_SHARING_MODE_EXCLUSIVE)
                .setAspect(VK_IMAGE_ASPECT_COLOR_BIT)
                .setMemoryUsage(VMA_MEMORY_USAGE_GPU_ONLY)
                .build();

        image->transitionLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

        VkBufferImageCopy bufferImageRegion{};
        bufferImageRegion.bufferOffset = 0;
        bufferImageRegion.bufferRowLength = 0;
        bufferImageRegion.bufferImageHeight = 0;
        bufferImageRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferImageRegion.imageSubresource.mipLevel = 0;
        bufferImageRegion.imageSubresource.baseArrayLayer = 0;
        bufferImageRegion.imageSubresource.layerCount = 1;
        bufferImageRegion.imageOffset = {0, 0, 0};
        bufferImageRegion.imageExtent = {size.width, size.height, 1};

        auto queue = device->getQueue(VK_QUEUE_GRAPHICS_BIT);
        OnceCommandBuffer copyCommandBuffer(device, queue.index, queue);
        vkCmdCopyBufferToImage(copyCommandBuffer, stagingBuffer, *image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageRegion);
        copyCommandBuffer.end();

        image->transitionLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        VkSamplerCreateInfo samplerInfo{VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
        samplerInfo.magFilter = VK_FILTER_LINEAR;
        samplerInfo.minFilter = VK_FILTER_LINEAR;
        samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
        LVE_VK_CHECK(vkCreateSampler(*device, &samplerInfo, nullptr, &sampler))
    }

    Texture::~Texture() {
        if (sampler) {
            vkDestroySampler(device, sampler, nullptr);
        }

        image.reset();
    }

}
