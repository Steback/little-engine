#include "Material.hpp"


namespace lve {

    Material::Material(std::string baseName, std::shared_ptr<Texture> base, const glm::vec4 &baseColor)
            : baseName(std::move(baseName)), base(std::move(base)), baseColor(baseColor) {

    }

    void Material::bind(VkCommandBuffer cmdBuffer, VkPipelineLayout layout) {
        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &descriptorSet, 0, nullptr);
    }

} // lve