#ifndef LITTLEVULKANENGINE_MESH_HPP
#define LITTLEVULKANENGINE_MESH_HPP


#include <memory>
#include <vector>

#include <glm/glm.hpp>

#include "render/Device.hpp"
#include "utils/NonCopyable.hpp"
#include "render/buffer/Buffer.hpp"


namespace lve {

    struct Vertex {
        glm::vec3 position;
        glm::vec3 color;
        glm::vec3 normal;
        glm::vec2 uv;

        bool operator==(const Vertex &other) const;

        static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();

        static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
    };

    class Mesh : public NonCopyable {
    public:
        Mesh(const std::shared_ptr<Device>& device, const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices);

        void bind(const VkCommandBuffer& commandBuffer);

        void draw(const VkCommandBuffer& commanderBuffer) const;

    private:
        uint32_t indicesCount{};
        uint32_t verticesCount{};
        std::unique_ptr<Buffer> indexBuffer;
        std::unique_ptr<Buffer> vertexBuffer;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_MESH_HPP
