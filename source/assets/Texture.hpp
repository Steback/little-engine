#ifndef LITTLEVULKANENGINE_TEXTURE_HPP
#define LITTLEVULKANENGINE_TEXTURE_HPP


#include <memory>

#include "render/Image.hpp"
#include "render/Device.hpp"


namespace lve {

    class Texture {
    public:
        Texture(const std::shared_ptr<Device>& device, VkExtent3D size, const unsigned char* data, VkDeviceSize dataSize);

        ~Texture();

        [[nodiscard]] inline VkSampler getSampler() const { return sampler; }

        [[nodiscard]] inline std::shared_ptr<Image> getImage() const { return image; }

    private:
        VkDevice device{};
        VkSampler sampler{};
        std::shared_ptr<Image> image;
    };

}


#endif //LITTLEVULKANENGINE_TEXTURE_HPP
