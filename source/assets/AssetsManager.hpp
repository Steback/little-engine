#ifndef LITTLEVULKANENGINE_ASSETSMANAGER_HPP
#define LITTLEVULKANENGINE_ASSETSMANAGER_HPP


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "Mesh.hpp"
#include "File.hpp"
#include "Texture.hpp"
#include "Material.hpp"
#include "utils/Singleton.hpp"
#include "loader/IModelLoader.hpp"
#include "loader/ITextureLoader.hpp"
#include "loader/MaterialLoader.hpp"
#include "render/descriptor/DescriptorPool.hpp"
#include "render/descriptor/DescriptorLayout.hpp"


namespace lve {

    class AssetsManager : public Singleton<AssetsManager> {
        friend Singleton<AssetsManager>;

        // TODO: Refactor AssetsManager
        explicit AssetsManager(std::filesystem::path data);

    public:
        ~AssetsManager();

        void setupDescriptorsSets(const std::shared_ptr<DescriptorPool>& pool, const std::shared_ptr<DescriptorLayout>& layout);

        File getFile(const std::filesystem::path& path);

        std::pair<std::string, std::shared_ptr<Mesh>> addMesh(const std::string& fileName);

        std::shared_ptr<Mesh> getMesh(const std::string& id);

        std::pair<std::string, std::shared_ptr<Texture>> addTexture(const std::string& fileName);

        std::shared_ptr<Texture> getTexture(const std::string& id);

        std::pair<std::string, std::shared_ptr<Material>> addMaterial(const std::string& fileName);

        std::shared_ptr<Material> getMaterial(const std::string& id);

        [[nodiscard]] inline uint32_t getMaterialsCount() const { return materials.size(); };

        [[nodiscard]] inline std::filesystem::path getData() const { return data; }

    private:
        std::filesystem::path data;
        std::shared_ptr<MaterialLoader> materialLoader;
        std::unordered_map<std::string, std::shared_ptr<Mesh>> meshes;
        std::unordered_map<std::string, std::shared_ptr<Texture>> textures;
        std::unordered_map<std::string, std::shared_ptr<Material>> materials;
        std::unordered_map<std::string, std::shared_ptr<IModelLoader>> modelLoaders;
        std::unordered_map<std::string, std::shared_ptr<ITextureLoader>> texturesLoaders;
    };

} // namespace lve


#endif //LITTLEVULKANENGINE_ASSETSMANAGER_HPP
