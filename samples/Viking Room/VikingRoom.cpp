#include "VikingRoom.hpp"

#include "scene/ScenesManager.hpp"
#include "components/TransformComponent.hpp"


namespace lve {

    VikingRoom::VikingRoom() : Application("Viking Room") {

    }

    VikingRoom::~VikingRoom() = default;

    void VikingRoom::setup() {
        Application::setup();

        if (auto scene = ScenesManager::get()->getCurrent()) {
            cameraID = scene->getEntity("Camera");
        }
    }

    void VikingRoom::update(float deltaTime) {
        Application::update(deltaTime);

        if (auto scene = ScenesManager::get()->getCurrent()) {
            if (const auto* transform = scene->getComponent<TransformComponent>(cameraID)) {
                camera.setViewYXZ(transform->translation, transform->rotation);
            }
        }
    }

    void VikingRoom::setupSettingsFields() {
        Application::setupSettingsFields();
    }

    void VikingRoom::setupConsoleCommands() {
        Application::setupConsoleCommands();
    }

    void VikingRoom::setupInputBindings() {
        Application::setupInputBindings();
    }

} // namespace lve
