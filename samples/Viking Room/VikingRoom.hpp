#ifndef LITTLEVULKANENGINE_VIKING_ROOM_HPP
#define LITTLEVULKANENGINE_VIKING_ROOM_HPP


#include "scene/Scene.hpp"
#include "core/Application.hpp"


namespace lve {

    class VikingRoom : public Application {
    public:
        VikingRoom();

        ~VikingRoom();

        void setup() override;

        void update(float deltaTime) override;

        void setupSettingsFields() override;

        void setupConsoleCommands() override;

        void setupInputBindings() override;

    private:
        Entity cameraID{};
    };

} // namespace lve


#endif // LITTLEVULKANENGINE_VIKING_ROOM_HPP
