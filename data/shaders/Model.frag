#version 450

layout (location = 0) out vec4 outColor;

layout (location = 0) in vec2 fragUV;
layout (location = 1) in vec3 fragColor;

layout(set = 0, binding = 0) uniform sampler2D texSampler;

void main() {
    outColor = vec4(fragColor * texture(texSampler, fragUV).rgb, 1.0f);
}
